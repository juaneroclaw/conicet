<?php
session_start();

if(!isset($_SESSION["name"])) {
    header("location:index.php");
} else {
 $nick= $_SESSION['name'];}
include('conn.php');

$ncalc = mysqli_fetch_array($con->query("SELECT MAX(idVisor) AS ValorMaximo FROM tblVisor"));
$nvisor = $ncalc['ValorMaximo']+1;
global $degree; 
global $targetPath;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>INECOA | Formato-766</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="author" content="Desarrollado por Ana Lorenzo *-* INECOA-CONICET" />
        <meta name="description" content="Visor ambiental Jujuy - Argentina"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/fonts-googleapis.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
       
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/blue.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="css/loader-ball.css" type="text/css" rel="stylesheet" />
        <link href="css/loader-ball.min.css" type="text/css" rel="stylesheet" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <script type="text/javascript">
            var ayImportar = "Verifique que la planilla Excel seleccionada cumpla con el Formato-766 requerido. <br> Clic en Importar y se alimentará la grilla donde podrá asignar la Capas a cada registro. <br> Cuando se asignan Capas, el registro pasará al final de la grilla del lote importado, haciendo clic sobre el título Capas, la grilla se ordenará por nombre de Capas. Ó bien, usando la barra de navegación podrá ubicarlo. <br> Así con todas las columnas, puede ordenar el lote por cualquier columna visible. <br> Desde la función <b>Buscar</b> podrá filtrar los datos de la grilla por cualquiera de las columnas visibles. <br/> Si es necesario <b>Editar</b> o <b>Eliminar</b> el registro, al final de la línea accede a su edición o eliminación. <br/> Con la función <b>Columnas</b> podrá elegir cúales estarán visibles o no en la grilla. <br/> Con <b>Depurar</b>, TODOS aquellos registros a los que NO se les asigne Capas, serán eliminados. <br/>";
            var ayNva = "ayNva";
        </script> 
        </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
    <script type="text/javascript" src="wz_tooltip/wz_tooltip.js"></script>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="menu.php">
                            <img src="assets/pages/img/logo-big.png" alt="logo" class="logo-default" /> </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
                    <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
                    <div class="hor-menu   hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">
                            <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                            <!--<span add claass active </span>-->
                            <li class="classic-menu-dropdown active" aria-haspopup="true">
                                <a href="import-766.php"> Importar Formato-766
                                    <span class="selected"> </span>
                                </a>
                            </li>

                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="import-exp.php"> Expedientes MA </a> 
                                <!-- <a href="enConstruc.php"> Expedientes MA </a>  -->
                            </li>

                            <!-- MEGA MENU -->
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Consultas
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    
                                    <li class="classic-menu-dropdown" aria-haspopup="true">
                                    <a href="QTaxa.php"> Cantidad de Taxones por Ecoregión</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- MEGA MENU -->
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                   
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="media/users/user-login.png" />
                                    <span class="username username-hide-on-mobile"> <?php echo $nick; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="edit-profile.php">
                                            <i class="icon-user"></i> Editar Perfil </a>
                                    </li>

                                    <li>
                                        <a href="cambiarPassw.php">
                                            <i class="icon-key"></i> Cambiar contraseña </a>
                                    </li>
                                   
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-logout"></i> Salir </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="logout.php" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- END SIDEBAR MENU -->
                        <div class="page-sidebar-wrapper">
                            <!-- BEGIN RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                            <ul class="page-sidebar-menu visible-sm visible-xs  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                                <!-- DOC: This is mobile version of the horizontal menu. The desktop version is defined(duplicated) in the header above -->
                                    
                                                <li class="nav-item start ">
                                                    <a href="menu.php" class="nav-link ">
                                                        <i class="fa fa-dashboard"></i>
                                                        <span class="title">Menú</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item start ">
                                                    <a href="import-766.php" class="nav-link ">
                                                        <i class="fa fa-file-excel-o"></i>
                                                        <span class="title">Importar Formato-766</span>
                                            
                                                    </a>
                                                </li>

                                                <!--<li class="nav-item start ">
                                                    <a href="nva-formato-766.php" class="nav-link ">
                                                        <i class="fa fa-file-excel-o"></i>
                                                        <span class="title">Formato 766</span>
                                            
                                                    </a>
                                                </li>-->

                                               <li class="nav-item  ">
                                            <a href="javascript:;" class="nav-link nav-toggle">
                                                <i class="fa fa-circle-o-notch"></i>
                                                <span class="title">Consultas</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item  ">
                                                   
                                                    <a href="QTaxa.php" class="nav-link ">
                                                        <i class="fa fa-circle-o-notch"></i>
                                                        <span class="title"> # de Taxones por Ecoregión</span>
                                            
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                                
                                        
                                        
                                        <li class="nav-item  ">
                                            <a href="javascript:;" class="nav-link nav-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="title">Configuración</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item  ">
                                                    <a href="edit-profile.php" class="nav-link ">
                                                        <i class="icon-user"></i>
                                                        <span class="title">Editar Perfil</span>
                                                    </a>
                                                </li>
                                                 <li class="nav-item">
                                        <a href="cambiarPassw.php" class="nav-link">
                                            <i class="icon-key"></i> 
                                            <span class="title">Cambiar contraseña</span>
                                             </a>
                                            </li>
                                                <li class="nav-item  ">
                                                    <a href="logout.php" class="nav-link ">
                                                        <i class="icon-logout"></i>
                                                        <span class="title">Salir</span>
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                            </ul>
                            <!-- END RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                        </div>
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                       
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="menu.php">Menú</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="import-766.php">Importar Formato-766</a>
                                 
                                </li>
                                
                            </ul>
                           
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Importar archivos Formato-766 para Visor ambiental
                            
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
            <div class="row">

            <div class="col-md-12">
            <!-- Contenido -->
            <div class="statusMsg"></div>
            <form id="fupForm" name="fupForm" enctype="multipart/form-data" >                 
                <div class="col-md-6">
                    <div class="input-group">
                        <input type="file" class="form-control" id="file" name="file" accept=".xls,.xlsx" required />
                        <span class="input-group-btn">
                        <input type="submit" name="submit" class="btn btn-success submitBtn" value="Importar Registros"/>
                        </span>
                    </div>                              
                </div>
            </form>    

            <!-- Ayudas -->
            <div>      
            <a href="import-766.php" onmouseover="Tip(ayImportar, TEXTALIGN, 'left', WIDTH, 350, HEIGHT, 250, CLICKSTICKY, true, CLICKCLOSE, true, CLOSEBTN, true)" onmouseout="UnTip()"><b> ? </b></a> 
            </div>            

            <!-- Contenido -->
            <div class="col-md-6">
               <a href="#" id="nva_planilla" name="nva_planilla" class="btn btn-success" onclick="nuevo_reg_alert(<?php echo $nvisor; ?>)"><i class='fa fa-plus-circle'></i> Nuevo Registro</a>
            </div>
            </div>

            <div class="col-md-12">
            <div class="col-md-6">
                <a href="import-766.php" onmouseover="Tip('COLUMNAS REQUERIDAS - SIN TÍTULOS <img src=\'images/formato766.png\' width=\'1347\'>image.')" onmouseout="UnTip()"> <font size="2px" color="#3765c9">Formato de columnas en excel requerido</font> </a>
            </div>
            </div>
            <div id="resultados"></div>
            </div>
                         <div class="row">
                            <div class="col-md-12">

                                <div id="view_visor" name="view_visor" style="display:none" >
                              <h1 class="page-title">
                            
                                </h1>

                                <div id="cargar_visor"></div>
                                </div>
                            </div>
                       </div>
<?php include("modal/nuevo_reg.php");?>
<?php include("modal/editar_reg.php");?>
<?php include("modal/eliminar_reg.php");?>
<?php include("modal/depurar_reg.php");?>

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> San Salvador de Jujuy 2020 &copy; Cosecha de datos para VISOR AMBIENTAL
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
        
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
    load_table();
} );
</script>

<script type="text/javascript" >
    var cap1;
    var cap2;
    var cap3;
    var idreg;
    var idcontrol;

    function capa1(e) {
    //cap1 = document.getElementById("capa_1").value;
    //alert($(e).attr("id"));
    cap1 = $(e).val();
    identcap1 = $(e).attr("id");
    var arrStr = identcap1.split(/[_]/);
    idreg = arrStr[2];
    idcontrol = arrStr[3];
    $('#cargar_boton_'+idreg+'_'+idcontrol).css("display","none");
    //alert(arrStr[2]);

    
    console.log(cap1);
    /*$.each(cap1, function(index, value) {
       alert(value);           
     });*/

    $("#"+identcap1.replace(/capa_1/g,"capa_2")).empty();
    $("#"+identcap1.replace(/capa_1/g,"capa_3")).empty();
    $("#"+identcap1.replace(/capa_1/g,"capa_2")).css("display","none");
    $("#"+identcap1.replace(/capa_1/g,"capa_3")).css("display","none");
    cargar_capa2(cap1,identcap1);  
  }
  function capa2(e) {
    //cap2 = document.getElementById("capa_2").value;
    cap2 = $(e).val(); 
    identcap2= $(e).attr("id");
    $("#"+identcap2.replace(/capa_2/g,"capa_3")).empty();
    $("#"+identcap2.replace(/capa_2/g,"capa_3")).css("display","none");
    $('#cargar_boton_'+idreg+'_'+idcontrol).css("display","none");
   
    cargar_capa3(cap2,identcap2);  
  }

  function capa3(e) {

    cap3 = $(e).val();
    console.log(cap3);
    identcap3 = $(e).attr("id"); 
    //alert('LLEGO: ' + idreg );
    $('#cargar_boton_'+idreg+'_'+idcontrol).css("display","");
    $('#cargar_boton_'+idreg+'_'+idcontrol).html('<a class="btn btn-sm green btn-outline filter-cancel" onclick="save_capas_import()"><i class="fa fa-save"></i>  Guardar</a>');
  }

  function cargar_capa2(id,ident){
    var jsonString = JSON.stringify(id);
    //alert(id);

            $.ajax({
                url: "ajax/cargar_capa2_import.php",
                method:"post",
                dataType: 'json',
                //data: {id : jsonString},
                data:{id:id, ident:ident},
                success: function(response){
                    $("#capa2").empty();
                    
                    var identcap = response['identcap'].replace(/capa_1/g,"capa_2");
                    //alert('sapote: ' + identcap);
                    console.log(response);
                    $.each(response, function( index, value ) {
                        //alert(index);
                        if (index!='identcap'){

                        index_=index.replace(/ /g,"_");
                        $("#"+identcap).append('<optgroup id="'+index_+'_cap2" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap2').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });

                        }  
                        
                    });
                 $("#"+identcap).css("display","");
                    
                }   
            });
            }

function cargar_capa3(id,ident){
//alert(id);
            $.ajax({
                url: "ajax/cargar_capa3_import.php",
                method:"post",
                dataType: 'json',
                data:{id:id, ident:ident},
                 success: function(response){
                    var identcap = response['identcap'].replace(/capa_2/g,"capa_3");
                    $("#capa3").empty();
                    console.log(response);
                    $.each(response, function( index, value ) {
                        //alert(index);
                        if (index!='identcap'){

                        index_=index.replace(/ /g,"_");
                        $("#"+identcap).append('<optgroup id="'+index_+'_cap3" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap3').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });

                        }  
                        
                    });
                 $("#"+identcap).css("display","");
                    
                }
            });
            }

  // *-*  ESTA FUNCION GRABA EN tblVisor  *-*  
    function save_capas_import(){
        ids=idreg;
        regs=cap3;
         $.ajax({
                url: "ajax/save_import_reg.php",
                method:"post",
                data:{ids:ids, regs:regs},
                beforeSend: function(objeto){
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#cargar_visor').fadeIn(1000).html(datos);
                    //$('#cargar_boton_'+idreg+'_'+idcontrol).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                    cargar_tabla(<?php  echo $nvisor; ?>);
                }
            });
            }
</script>

 <script type="text/javascript" >
    var cap1_table;
    var cap2_table;
    var cap3_table;
    var idreg_table;
    var idcontrol_table;

    function capa1_table(e) {
    //cap1 = document.getElementById("capa_1").value;
    //alert($(e).attr("id"));
    cap1_table = $(e).val();
    identcap1_table = $(e).attr("id");
    var arrStr_table = identcap1_table.split(/[_]/);
    idreg_table = arrStr_table[3];
    idcontrol_table = arrStr_table[4];
    $('#cargar_boton_table_'+idreg_table+'_'+idcontrol_table).css("display","none");
    //alert('idreg: '+arrStr_table[3]+' idcontrol: '+arrStr_table[4]);

    
    console.log(cap1);
    /*$.each(cap1, function(index, value) {
       alert(value);           
     });*/

    $("#"+identcap1_table.replace(/capa_1_table/g,"capa_2_table")).empty();
    $("#"+identcap1_table.replace(/capa_1_table/g,"capa_3_table")).empty();
    $("#"+identcap1_table.replace(/capa_1_table/g,"capa_2_table")).css("display","none");
    $("#"+identcap1_table.replace(/capa_1_table/g,"capa_3_table")).css("display","none");
    cargar_capa2_table(cap1_table,identcap1_table);  
  }
  
  function capa2_table(e) {
    //cap2 = document.getElementById("capa_2").value;
    cap2_table = $(e).val(); 
    identcap2_table= $(e).attr("id");
    $("#"+identcap2_table.replace(/capa_2_table/g,"capa_3_table")).empty();
    $("#"+identcap2_table.replace(/capa_2_table/g,"capa_3_table")).css("display","none");
    $('#cargar_boton_table_'+idreg_table+'_'+idcontrol_table).css("display","none");
   
    cargar_capa3_table(cap2_table,identcap2_table);  
  }

  function capa3_table(e) {

    cap3_table = $(e).val();
    console.log(cap3_table);
    identcap3_table = $(e).attr("id"); 
    //alert('LLEGO: ' + idreg );
    $('#cargar_boton_table_'+idreg_table+'_'+idcontrol_table).css("display","");
    $('#cargar_boton_table_'+idreg_table+'_'+idcontrol_table).html('<a class="btn btn-sm green btn-outline filter-cancel" onclick="save_capas_import_table()"><i class="fa fa-save"></i>  Guardar</a>');
  }

  function cargar_capa2_table(id,ident){
    var jsonString = JSON.stringify(id);
    //alert(id);

            $.ajax({
                url: "ajax/cargar_capa2_import.php",
                method:"post",
                dataType: 'json',
                //data: {id : jsonString},
                data:{id:id, ident:ident},
                success: function(response){
                    $("#capa2").empty();
                    
                    var identcap = response['identcap'].replace(/capa_1_table/g,"capa_2_table");
                    //alert('sapote: ' + identcap);
                    console.log(response);
                    $.each(response, function( index, value ) {
                        //alert(index);
                        if (index!='identcap'){

                        index_=index.replace(/ /g,"_");
                        $("#"+identcap).append('<optgroup id="'+index_+'_cap2" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap2').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });

                        }  
                        
                    });
                 $("#"+identcap).css("display","");
                    
                }
            });
            }

function cargar_capa3_table(id,ident){
//alert(id);
            $.ajax({
                url: "ajax/cargar_capa3_import.php",
                method:"post",
                dataType: 'json',
                data:{id:id, ident:ident},
                 success: function(response){
                    var identcap = response['identcap'].replace(/capa_2_table/g,"capa_3_table");
                    $("#capa3").empty();
                    console.log(response);
                    $.each(response, function( index, value ) {
                        //alert(index);
                        if (index!='identcap'){

                        index_=index.replace(/ /g,"_");
                        $("#"+identcap).append('<optgroup id="'+index_+'_cap3" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap3').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });

                        }  
                        
                    });
                 $("#"+identcap).css("display","");
                    
                }
            });
            }


// *-* ESTA FUNCION GRABA EN tblVisor
    function save_capas_import_table(){
        ids=idreg_table;
        regs=cap3_table;
         $.ajax({
                url: "ajax/save_import_reg.php",
                method:"post",
                data:{ids:ids, regs:regs},
                beforeSend: function(objeto){
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#cargar_visor').fadeIn(1000).html(datos);
                    //$('#cargar_boton_'+idreg+'_'+idcontrol).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                    load_table();
                }
            });
            }
</script>

<script>
$(document).ready(function(e){
    // Submit form data via Ajax
    $("#fupForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'upload.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#fupForm').css("opacity",".5");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
            },
            success: function(response){ //console.log(response);
                
                $('.statusMsg').html('');
                if(response.status == 1){
                    $('#fupForm')[0].reset();
                    $('#fupForm').css("opacity",".5");
                    $('.submitBtn').attr("disabled","disabled");
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    $('#resultados').fadeIn(1000).html('');
                    $('#cargar_visor').fadeIn(1000).html(response.uri);
                    $("#view_visor").css("display","");
                    synchro_reg(response.uri,<?php  echo $nvisor; ?>);
                }else{
                    if(response.status == 2){
                    $('#fupForm')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-warning">'+response.message+'</p>');
                    $('#resultados').fadeIn(1000).html('');
                    $('#fupForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                    }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $('#resultados').fadeIn(1000).html('');
                    $('#fupForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                    }
                }
            }
        });
    });
});
</script>

<script>
    function synchro_reg(url,idvisor){

            $.ajax({
                url: "ajax/import_reg.php",
                method:"post",
                data:{url:url, idvisor:idvisor},
                beforeSend: function(objeto){
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#cargar_visor').fadeIn(1000).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                 
                    cargar_tabla(<?php  echo $nvisor; ?>);
                }
            });
        }

    function cargar_tabla(id){
            $.ajax({
                url: "ajax/cargar_tabla.php",
                method:"post",
                data:{id:id},
                beforeSend: function(objeto){
                
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#nva_planilla').css("opacity",".3");
                    $('#nva_planilla').attr("disabled","disabled");
                    $('#cargar_visor').fadeIn(1000).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                    $("#view_visor").css("display","");
                    $('html, body').animate({
                    scrollTop: $("#view_visor").offset().top
             }, 1000);
                    //$('#resultados_carga').fadeIn(1000).html('<a role="button" class="btn btn-success submitBtn" onclick="synchro_ml(2);">SINCRONIZAR | MERCADOLIBRE</a>');
                    //load();
                }
            });
        }

    function cargar_visor(id){

            $.ajax({
                url: "ajax/cargar_visor.php",
                method:"post",
                data:{id:id},
                beforeSend: function(objeto){
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success: function(datos){
                    $('#fupForm').css("opacity",".5");
                    $('.submitBtn').attr("disabled","disabled");
                   $('#cargar_visor').fadeIn(1000).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                    $("#view_visor").css("display","");
                    $('html, body').animate({
                    scrollTop: $("#view_visor").offset().top
             }, 1000);
                }
            });
            }

    function load_table(){
            $.ajax({
                url: "ajax/load_table.php",
                method:"post",
          
                beforeSend: function(objeto){
                $("#view_visor").css("display","none");
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#cargar_visor').fadeIn(1000).html(datos);
                    $('#resultados').fadeIn(1000).html('');
                    $('#fupForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                    $('#nva_planilla').css("opacity","");
                    $('#nva_planilla').removeAttr("disabled");
                    $('.statusMsg').html('');
                    $("#view_visor").css("display","");
                    $('html, body').animate({
                    scrollTop: $("#cargar_visor").offset().top
             }, 1000);
                    //$('#resultados_carga').fadeIn(1000).html('<a role="button" class="btn btn-success submitBtn" onclick="synchro_ml(2);">SINCRONIZAR | MERCADOLIBRE</a>');
                    //load();
                }
            });
        }
</script>

<script>
    function editar_alert_import(id,reg){
            $.ajax({
                url: "ajax/editar_alert_import.php",
                method:"post",
                data:{id:id, reg:reg},
                success:function(data){
                    $('#resultados_ajax101').empty();
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }
    function eliminar_alert_import(id){

            $.ajax({
                url: "ajax/eliminar_import_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }
    function eliminar_alert(id,page){

            $.ajax({
                url: "ajax/eliminar_alert.php",
                method:"post",
                data:{id:id, page:page},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }
</script>

<script>
$( "#eliminar_registro" ).submit(function( event ) {
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "ajax/eliminar_reg.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados_ajax100").html("Mensaje: Cargando...");
              },
            success: function(response){
                $("#resultados_ajax100").html(JSON.parse(response).status);
            if(JSON.parse(response).page=='import'){
                cargar_tabla(<?php  echo $nvisor; ?>);
            }
            if(JSON.parse(response).page=='new'){
                cargar_visor(<?php  echo $nvisor; ?>);
            }
            if(JSON.parse(response).page=='data'){
                load_table();
            }
            
        
          }
    });
  event.preventDefault();
})
</script>

<script>
$( "#editar_registro" ).submit(function( event ) {
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "ajax/editar_reg.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados_ajax101").html("Mensaje: Cargando...");
              },
            success: function(response){
                //console.log(JSON.parse(response));
                
            $("#resultados_ajax101").html(JSON.parse(response).status);
            if(JSON.parse(response).page=='import'){
                cargar_tabla(<?php  echo $nvisor; ?>);
            }
            if(JSON.parse(response).page=='new'){
                cargar_visor(<?php  echo $nvisor; ?>);
            }
            if(JSON.parse(response).page=='data'){
                load_table();
            }
          }
    });
  event.preventDefault();
})
</script>

<script>
function nuevo_reg_alert(id){

   if($('#nva_planilla').attr("disabled")!=='disabled') {
      
         $.ajax({
                url: "ajax/nuevo_reg_alert.php",
                method:"post",
                data:{id:id},
                beforeSend: function(objeto){
                $('#resultados_ajax105').empty();
                $('#myModal105').modal("show");
                $('#nuevo_registrov').html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#nuevo_registrov').fadeIn(1000).html(datos); 
                }
            });
            }
   }
    

function eliminar_new_alert(id){

            $.ajax({
                url: "ajax/eliminar_new_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }

    function editar_new_alert(id,reg){

            $.ajax({
                url: "ajax/editar_alert.php",
                method:"post",
                data:{id:id, reg:reg},
                success:function(data){
                    $('#resultados_ajax101').empty();
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }

$("#nuevo_registro").submit(function( event ) {
 var parametros = $(this).serializeArray();
//console.log($(this).serializeArray());
     $.ajax({
            type: "POST",
            url: "ajax/add_nva_visor.php",
            data: parametros,
             beforeSend: function(objeto){
                $('#resultados_ajax105').html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(datos){
                    $('#resultados_ajax105').fadeIn(1000).html(datos);
                    cargar_visor(<?php  echo $nvisor; ?>);
                }
    });
  event.preventDefault();
})
</script>

<script>
    function depurar_table(){

            $.ajax({
                url: "ajax/depurar_table_alert.php",
                method:"post",
                success:function(data){
                    $('#resultados_ajax109').empty();
                    $('#eliminar_registrodep').html(data);
                    $('#myModal109').modal("show");

                }
            });
            }
$("#depurar_registro").submit(function( event ) {
  
 var parametros = $(this).serializeArray();
//console.log($(this).serializeArray());
     $.ajax({
            type: "POST",
            url: "ajax/depurar_table.php",
            data: parametros,
             beforeSend: function(objeto){
                $('#resultados_ajax109').html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(response){
                     $("#resultados_ajax109").html(JSON.parse(response).status);
                    load_table();
                }
    });
  event.preventDefault();
})
</script>

<script>
    function export_excel(id){

            $.ajax({
                url: "ajax/export_excel.php",
                method:"post",
                data:{id:id},
                beforeSend: function(objeto){
                $('#resultados').html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(response){
                    console.log(JSON.parse(response).data);
                    if(JSON.parse(response).aprobado=='OK'){
                        $('.statusMsg').html('');
                        //console.log(JSON.parse(response).data);
                       window.location=JSON.parse(response).link;
                       $('#resultados').html('');
                        load_table();
                        
                    }else{
                        $('html, body').animate({
                    scrollTop: $('.statusMsg').offset().top
             }, 1000);
                        $('.statusMsg').html('<p class="alert alert-warning">No existen datos para exportar.</p>');
                        $('#resultados').html('');
                    }
                }
            });
            }

    function export_csv(id){
            $.ajax({
                url: "ajax/export_csv.php",
                method:"post",
                data:{id:id},
                beforeSend: function(objeto){
                $('#resultados').html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
                success:function(response){
                    console.log(JSON.parse(response));
                    if(JSON.parse(response).aprobado=='OK'){
                        $('.statusMsg').html('');
                        window.location=JSON.parse(response).link;
                        $('#resultados').html('');
                        load_table();
                        
                    }else{
                        $('html, body').animate({
                    scrollTop: $('.statusMsg').offset().top
             }, 1000);
                        $('.statusMsg').html('<p class="alert alert-warning">No existen datos para exportar.</p>');
                        $('#resultados').html('');
                    }
                }
            });
            }
</script>

        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>