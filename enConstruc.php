<?php
session_start();

if(!isset($_SESSION["name"])) {

    header("location:index.php");
} else {
 $nick= $_SESSION['name'];}
include('conn.php');


$ncalc = mysqli_fetch_array($con->query("SELECT MAX(idExp) AS ValorMaximo FROM tblExpediente"));
$nvisor = $ncalc['ValorMaximo']+1;
global $degree; 
global $targetPath;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>INECOA | Expedientes</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="author" content="Desarrollado por Ana Lorenzo *-* INECOA-CONICET" />
        <meta name="description" content="Visor ambiental Jujuy - Argentina"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/fonts-googleapis.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
       
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/blue.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="css/loader-ball.css" type="text/css" rel="stylesheet" />
        <link href="css/loader-ball.min.css" type="text/css" rel="stylesheet" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <script type="text/javascript">
            var ayImportar = "Verifique que la planilla Excel seleccionada cumpla con el Formato para Expedientes requerido. <br> Clic en Importar y se alimentará la grilla donde podrá asignar la Capas a cada registro. <br> Cuando se asignan Capas, el registro pasará al final de la grilla del lote importado, haciendo clic sobre el título Capas, la grilla se ordenará por nombre de Capas. Ó bien, usando la barra de navegación podrá ubicarlo. <br> Así con todas las columnas, puede ordenar el lote por cualquier columna visible. <br> Desde el campo <b>Buscar</b> podrá filtrar los datos de la grilla por cualquiera de las columnas visibles. <br/> Si es necesario <b>Editar</b> o <b>Eliminar</b> el registro, al final de la línea accede a su edición o eliminación. <br/> Con <b>Columnas</b> podrá elegir cúales estarán visibles o no en la grilla. <br/> Con <b>Depurar</b>, TODOS aquellos registros a los que NO se les asigne Capas, serán eliminados. <br/>";
            var ayNva = "ayNva";
        </script> 
        </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
    <script type="text/javascript" src="wz_tooltip/wz_tooltip.js"></script>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="menu.php">
                            <img src="assets/pages/img/logo-big.png" alt="logo" class="logo-default" /> </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
                    <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
                    <div class="hor-menu   hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">
                            <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                            <!--<span add claass active </span>-->
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="import-766.php"> Importar Formato-766
                                </a>
                            </li>

                            <li class="classic-menu-dropdown active" aria-haspopup="true">
<!--                                <a href="import-exp.php"> Expedientes MA </a>  -->
                                <a href="enConstruc.php"> Expedientes MA </a>
                                <span class="selected"> </span>
                            </li>

                            <!-- MEGA MENU -->
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Consultas
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    
                                    <li class="classic-menu-dropdown" aria-haspopup="true">
                                    <a href="QTaxa.php"> Cantidad de Taxones por Ecoregión</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- MEGA MENU -->
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                   
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            
                           
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="media/users/user-login.png" />
                                    <span class="username username-hide-on-mobile"> <?php echo $nick; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="edit-profile.php">
                                            <i class="icon-user"></i> Editar Perfil </a>
                                    </li>

                                    <li>
                                        <a href="cambiarPassw.php">
                                            <i class="icon-key"></i> Cambiar contraseña </a>
                                    </li>
                                   
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-logout"></i> Salir </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="logout.php" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->

<body>
<div align="center">
    <h1>  .     </h1>
    <h1>    .   </h1>
    <h1>  .     </h1>
    <h1>    INECOA - Importación de Expedientes MA  </h1>
    <h1>   En construcción </h1>
</div>
</body>

</html>