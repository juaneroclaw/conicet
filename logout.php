<?php 
require_once("conn.php");

session_start();

unset($_SESSION['name']);

session_destroy();

$con->close();

header("location:index.php");

?>