<?php
session_start();

if(!isset($_SESSION["name"])) {

    header("location:index.php");
} else {
 require_once("conn.php");
 $nick= $_SESSION['name'];
 $email= $_SESSION['email'];}

 $consulta =mysqli_query($con, "SELECT * FROM login WHERE email='$email'");
 $row = mysqli_fetch_array($consulta);

 $name=$row['name'];
 $idInst=$row['idInst'];
   
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>INECOA | Editar Perfil</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="author" content="Desarrollado por Alejandro Plaza" />
        <meta name="description" content="Contactame alejandroepg@gmail.com / +584145446417 "/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/blue.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="css/loader-ball.css" type="text/css" rel="stylesheet" />
        <link href="css/loader-ball.min.css" type="text/css" rel="stylesheet" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="menu.php">
                            <img src="assets/pages/img/logo-big.png" alt="logo" class="logo-default" /> </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
                    <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
                    <div class="hor-menu   hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">
                            <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                            <!--<span add claass active </span>-->
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="import-766.php"> Importar 766
                                    <!--<span class="selected"> </span>-->
                                </a>
                            </li>

                            <!--<li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="nva-formato-766.php"> Formato 766
                                    
                                </a>
                            </li>-->

                            <!-- MEGA MENU -->
                            <li class="classic-menu-dropdown" aria-haspopup="true">
                                <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Consultas
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    
                                    <li class="classic-menu-dropdown" aria-haspopup="true">
                                    <a href="QTaxa.php"> # de Taxones por Ecoregiòn</a>
                                    </li>
                                    
                                   
                                   
                                </ul>
                            </li>
                            <!-- MEGA MENU -->
                            
                            
                            
                            
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                   
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            
                           
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="media/users/user-login.png" />
                                    <span class="username username-hide-on-mobile"> <?php echo $nick; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="edit-profile.php">
                                            <i class="icon-user"></i> Editar Perfil </a>
                                    </li>

                                    <li>
                                        <a href="cambiarPassw.php">
                                            <i class="icon-key"></i> Cambiar contraseña </a>
                                    </li>
                                   
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-logout"></i> Salir </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="logout.php" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- END SIDEBAR MENU -->
                        <div class="page-sidebar-wrapper">
                            <!-- BEGIN RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                            <ul class="page-sidebar-menu visible-sm visible-xs  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                                <!-- DOC: This is mobile version of the horizontal menu. The desktop version is defined(duplicated) in the header above -->
                                    
                                                <li class="nav-item start ">
                                                    <a href="menu.php" class="nav-link ">
                                                        <i class="fa fa-dashboard"></i>
                                                        <span class="title">Menu</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item start ">
                                                    <a href="import-766.php" class="nav-link ">
                                                        <i class="fa fa-file-excel-o"></i>
                                                        <span class="title">Importar 766</span>
                                            
                                                    </a>
                                                </li>

                                                 <!--<li class="nav-item start ">
                                                    <a href="nva-formato-766.php" class="nav-link ">
                                                        <i class="fa fa-file-excel-o"></i>
                                                        <span class="title">Formato 766</span>
                                            
                                                    </a>
                                                </li>-->

                                                <li class="nav-item  ">
                                            <a href="javascript:;" class="nav-link nav-toggle">
                                                <i class="fa fa-circle-o-notch"></i>
                                                <span class="title">Consultas</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item  ">
                                                   
                                                    <a href="QTaxa.php" class="nav-link ">
                                                        <i class="fa fa-circle-o-notch"></i>
                                                        <span class="title"> # de Taxones por Ecoregiòn</span>
                                            
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                                
                                        
                                        
                                        <li class="nav-item  ">
                                            <a href="javascript:;" class="nav-link nav-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="title">Configuracion</span>
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item  ">
                                                    <a href="edit-profile.php" class="nav-link ">
                                                        <i class="icon-user"></i>
                                                        <span class="title">Editar Perfil</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                        <a href="cambiarPassw.php" class="nav-link">
                                            <i class="icon-key"></i> 
                                            <span class="title">Cambiar contraseña</span>
                                             </a>
                                            </li>
                                                <li class="nav-item  ">
                                                    <a href="logout.php" class="nav-link ">
                                                        <i class="icon-logout"></i>
                                                        <span class="title">Salir</span>
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                            </ul>
                            <!-- END RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                        </div>
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                       
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="profile.php">Editar Perfil</a>
                                
                                </li>
                                
                            </ul>
                           
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END PAGE HEADER-->


                        <div class="row">
                            <div class="col-md-6">
                                <div id="resultados"></div>

                                <!-- DATA TAB -->
                                                    <div class="tab-pane" id="data">
                                                        <p><h3><i class="icon-user"></i> Datos de Usuario</h3></p>
                                                       
                                                        <form method="post" id="cambiar_datos" name="cambiar_datos">
                                                            <input type="hidden" value="1" id="id" name="id">
                                                   
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nombre y Apellido</label>
                                                                <input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>"> 
                                                            </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nombre de Usuario</label>
                                                                <input type="text" id="nick" name="nick" class="form-control" value="<?php echo $nick; ?>"> 
                                                            </div>
                                                            </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Correo electronico</label>
                                                                <input type="text" id="email" name="email" class="form-control" value="<?php echo $email; ?>"> 
                                                            </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Institucion</label>
                                                                <select class="form-control" name="instCliente" id="instCliente">
                 <div>

                    <!-- si ingresa una Institución nueva darla de alta en tblInstitucion   -->
                 
                    <option value="0">Seleccione una Institución</option>
                    <?php
                    $sqlSelectDisc = "SELECT * FROM tblInstitucion ORDER BY insAcro ASC";
                    $resultDisc = mysqli_query($con, $sqlSelectDisc);
                            while ($rowDis = mysqli_fetch_array($resultDisc)) {
                            echo '<option name="instCliente" id="instCliente" value="'.$rowDis['idIns'].'">'.$rowDis['insAcro'].'-'.$rowDis['insNom'].'</option>';
                         }
                    ?>
                 </div>
                 </select>
                                                            </div>
                                                            </div>
                                                            </div>
                                                          
                                                       
                                                           
                                                            <div class="margin-top-10" align="right">
                                                                <button type="submit" class="btn green">Guardar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- END CHANGE AVATAR TAB -->
                                
                                

                            </div>
                            
                        </div>

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> San Salvador de Jujuy 2020 &copy; Cosecha de datos para VISOR AMBIENTAL
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script>
        $(document).ready(function(){

            document.getElementById("instCliente").value = "<?php echo $idInst; ?>";
        });
    </script>
    <script>
$( "#cambiar_datos" ).submit(function( event ) {
  //$('#actualizar_datos').attr("disabled", true);
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "ajax/change_data.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#resultados").html('<div id="loader-wrapper"><div id="loader"><div class="ball-pulse">Cargando <div></div><div></div><div></div></div></div>');
              },
            success: function(datos){
            $("#resultados").fadeIn(1000).html(datos);
            
      
          }
    });
  event.preventDefault();
})


    </script>
      
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>