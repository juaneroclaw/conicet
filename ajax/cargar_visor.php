<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_1.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_1.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/


    /*COLOR BACKGROUND SET*/
    table#sample_1.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_1.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>
<?php
function convertDMSToDecimal($latlng) {
    $valid = false;
    $decimal_degrees = 0;
    $degrees = 0; $minutes = 0; $seconds = 0; $direction = 1;

    // Determine if there are extra periods in the input string
    $num_periods = substr_count($latlng, '.');
    if ($num_periods > 1) {
        $temp = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // replace all but last period with delimiter
        $temp = trim(preg_replace('/[a-zA-Z]/','',$temp)); // when counting chunks we only want numbers
        $chunk_count = count(explode(" ",$temp));
        if ($chunk_count > 2) {
            $latlng = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // remove last period
        } else {
            $latlng = str_replace("."," ",$latlng); // remove all periods, not enough chunks left by keeping last one
        }
    }
    
    // Remove unneeded characters
    $latlng = trim($latlng);
    $latlng = str_replace("º"," ",$latlng);
    $latlng = str_replace("°"," ",$latlng);
    $latlng = str_replace("'"," ",$latlng);
    $latlng = str_replace("\""," ",$latlng);
    $latlng = str_replace("  "," ",$latlng);
    $latlng = substr($latlng,0,1) . str_replace('-', ' ', substr($latlng,1)); // remove all but first dash

    if ($latlng != "") {
        // DMS with the direction at the start of the string
        if (preg_match("/^([nsewoNSEWO]?)\s*(\d{1,3})\s+(\d{1,3})\s*(\d*\.?\d*)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[2]);
            $minutes = intval($matches[3]);
            $seconds = floatval($matches[4]);
            if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                $direction = -1;
        }
        // DMS with the direction at the end of the string
        elseif (preg_match("/^(-?\d{1,3})\s+(\d{1,3})\s*(\d*(?:\.\d*)?)\s*([nsewoNSEWO]?)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[1]);
            $minutes = intval($matches[2]);
            $seconds = floatval($matches[3]);
            if (strtoupper($matches[4]) == "S" || strtoupper($matches[4]) == "W" || $degrees < 0) {
                $direction = -1;
                $degrees = abs($degrees);
            }
        }
        if ($valid) {
            // A match was found, do the calculation
            $decimal_degrees = ($degrees + ($minutes / 60) + ($seconds / 3600)) * $direction;
        } else {
            // Decimal degrees with a direction at the start of the string
            if (preg_match("/^([nsewNSEW]?)\s*(\d+(?:\.\d+)?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                    $direction = -1;
                $decimal_degrees = $matches[2] * $direction;
            }
            // Decimal degrees with a direction at the end of the string
            elseif (preg_match("/^(-?\d+(?:\.\d+)?)\s*([nsewNSEW]?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[2]) == "S" || strtoupper($matches[2]) == "W" || $degrees < 0) {
                    $direction = -1;
                    $degrees = abs($degrees);
                }
                $decimal_degrees = $matches[1] * $direction;
            }
        }
    }
    if ($valid) {
        return $decimal_degrees;
    } else {
        return false;
    }
}
require_once("../conn.php");
 $idVis=$_POST['id'];

 

    $response='';
    $response.='
    <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i>Nueva planilla 766 - nva-planilla'.$idVis.' </div>
                                        <div class="tools"> </div>
                                    </div>
                                    
                                    <div class="portlet-body table-both-scroll">
    <table class="table table-striped table-bordered table-hover table-fixed text-nowrap order-column display" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Capa</th>
                                                    <th>Subcapa</th>
                                                    <th>Ecoregión</th>
                                                    <th>Titulo</th>
                                                    <th>Descripción</th>
                                                    <th>Institución</th>
                                                    <th>Fuente</th>
                                                    <th>URL</th>
                                                    <th>Contacto</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                    <th>Mes</th>
                                                    <th>Año</th>
                                                    <th>Geometría</th>
                                                    <th>Altura m.s.</th>
                                                    <th># Taxones</th>
                                                    <th>Excel origen</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
    //$response = array(); 

   
    //$sqlSelectVis="SELECT * FROM tblvisor WHERE idVisor='$idVis'";
    $sqlSelectVis="SELECT * FROM tblVisor t1 JOIN tblDisciplina t2 ON t1.visIdDisc=t2.idDisc JOIN tblSubDisc t3 ON (t1.visIdDisc=t3.idDisc AND t1.visIdSubDisc=t3.idSubDisc) JOIN tblEcoregion t4 ON (t1.visIdDisc=t4.idDisc AND t1.visIdSubDisc=t4.idSubDisc AND t1.visIdEco=t4.IdEco) JOIN tblInstitucion t5 ON t1.idIns=t5.idIns JOIN tblGeometria t6 ON t1.idGeo=t6.idGeo JOIN tblFuente t7 ON t1.visFue=t7.idFue WHERE idVisor='$idVis' AND visUlt='Y'";
    $resultVis = mysqli_query($con, $sqlSelectVis);
    //$count=0;
        while ($rowVis = mysqli_fetch_array($resultVis)) {

        $latitud_data=''.$rowVis['colLatG'].'° '.$rowVis['colLatM'].'\' '.$rowVis['colLatS'].'" '.$rowVis['colLatX'].'';
        $latitud = convertDMSToDecimal($latitud_data);
        $longitud_data=''.$rowVis['colLonG'].'° '.$rowVis['colLonM'].'\' '.$rowVis['colLonS'].'" '.$rowVis['colLonX'].'';
        $longitud = convertDMSToDecimal($longitud_data);
        
        if($rowVis['visDup']=='S'){
            $duplicado="marca2";
        }else{
            $duplicado="";
        }
            $response.='
            <tr class="'.$duplicado.'">
            <td>'.$rowVis['id'].'</td>
            <td>'.$rowVis['nomDisc'].'</td>
            <td>'.$rowVis['nomSubDisc'].'</td>
            <td>'.$rowVis['nomEco'].'</td>
            <td>'.$rowVis['visTit'].'</td>
            <td>'.$rowVis['visDes'].'</td>
            <td>'.$rowVis['insNom'].'</td>
            <td>'.$rowVis['nomFue'].'</td>
            <td>'.$rowVis['visUrl'].'</td>
            <td>'.$rowVis['visCon'].'</td>
            <td>'.$latitud_data.'</td>
            <td>'.$longitud_data.'</td>
            <td>'.$rowVis['visMes'].'</td>
            <td>'.$rowVis['visAni'].'</td>
            <td>'.$rowVis['geoNom'].'</td>
            <td>'.$rowVis['visAlt'].'</td>
            <td>'.$rowVis['visTax'].'</td>
            <td>'.$rowVis['visExc'].'</td>
            <td>
            <div align="center">  <a class="btn btn-sm blue btn-outline filter-cancel" data-toggle="modal" data-target="#myModal101" onclick="editar_new_alert('.$rowVis['id'].','.$idVis.')"><i class="fa fa-edit fa-lg"></i> Editar</a>
            
            
            <a class="btn btn-sm red btn-outline filter-cancel" data-toggle="modal" data-target="#myModal100" onclick="eliminar_new_alert('.$rowVis['id'].')"><i class="fa fa-trash-o fa-lg"></i> Eliminar</a>
          
            </td>
            </tr>';
            //$response[$count]['value']=$rowVis['idSubDisc'];
            //$response[$count]['name']=$rowViss['nomSubDisc'];
            //$count=$count+1;
        }
        $response.=' </tbody>
                                        </table>
                                        </div>
                                     
                                </div>';
    echo $response;		

?>	

 <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons2.js" type="text/javascript"></script>
                                                    