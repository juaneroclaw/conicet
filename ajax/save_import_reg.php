<?php
require_once("../conn.php");

    if (empty($_POST['ids'])) {
         $errors[] = "Entrada ids vacía";
        } else if (empty($_POST['regs'])){
         $errors[] = "Entrada regs vacía";

        }   else if (
            !empty($_POST['ids']) &&
            !empty($_POST['regs'])
            
        ){

        // escaping, additionally removing everything that could be (html/javascript-) code
            
            $ids = mysqli_real_escape_string($con,(strip_tags($_POST["ids"],ENT_QUOTES)));

            $sqlSelectVis="SELECT * FROM tblVisor WHERE id='$ids'";
            $resultVis = mysqli_query($con, $sqlSelectVis);
            $rowVis = mysqli_fetch_array($resultVis);

            $idVisor = $rowVis['idVisor'];
            $visTit = $rowVis['visTit'];
            $visDes = $rowVis['visDes'];
            $idIns = $rowVis['idIns'];
            $visFue = $rowVis['visFue'];
            $visUrl = $rowVis['visUrl'];
            $visCon = $rowVis['visCon'];
            $colLatG = $rowVis['colLatG'];
            $colLatM = $rowVis['colLatM'];
            $colLatS = $rowVis['colLatS'];
            $colLatX = $rowVis['colLatX'];
            $colLonG = $rowVis['colLonG'];
            $colLonM = $rowVis['colLonM'];
            $colLonS = $rowVis['colLonS'];
            $colLonX = $rowVis['colLonX'];
            $visMes = $rowVis['visMes'];
            $visAni = $rowVis['visAni'];
            $idGeo = $rowVis['idGeo'];
            $visAlt = $rowVis['visAlt'];
            $visTax = $rowVis['visTax'];
            $visExc = $rowVis['visExc'];
            $visUlt = $rowVis['visUlt'];
            $visAcro = $rowVis['visAcro'];
            $visNroCol = $rowVis['visNroCol'];
            $visOri = $rowVis['visOri'];
            $visExpor = $rowVis['visExpor'];
            $visDup = $rowVis['visDup'];
            $visEmail = $rowVis['visEmail']; 

            if(is_array($_POST["regs"]))
            {
                //var_dump($_POST["regs"]);
            foreach ($_POST["regs"] as $clave=>$valor)
                {

                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];

                $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) VALUES(null, '$idVisor', '$FindDisc', '$FindSubDisc', '$valor', '$visTit', '$visDes', '$idIns', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$idGeo', '$visAlt', '$visTax', '$visExc', '$visUlt', '$visExpor', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$visDup', '$visEmail')";

        
                    $query_update = mysqli_query($con, $sql);
                 
                }
                $perm_deletd='si';
            }else{

                $perm_deletd='no';

                $valor=$_POST["regs"];
                var_dump($_POST["regs"]);
                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];
              

                $sql="UPDATE tblVisor SET visIdDisc='$FindDisc', visIdSubDisc='$FindSubDisc', visIdEco='$valor' WHERE id='$ids'";

                $query_update = mysqli_query($con, $sql);
            }   

            

            if($perm_deletd=='si'){

                $sql="DELETE FROM tblVisor WHERE id ='".$ids."'";
                $query_update = $con->query($sql);
            }

            if ($query_update){
                $messages[] = "Se ha actualizado el registro satisfactoriamente.";
            } else{
                $errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
            }
            

        } else {
            $errors []= "Error desconocido.";
        }
        
        if (isset($errors)){
            
            ?>
            <div class="alert alert-danger" role="alert">
               
             

                    <strong>Error!</strong> 
                    <?php

                        foreach ($errors as $error) {
                                echo $error;
                            }
                        ?>
            </div>

            
            <?php
            }
            if (isset($messages)){
                
                ?>
                <div class="alert alert-success" role="alert">
                
                    <strong>Bien!</strong> 
                    <?php



                        foreach ($messages as $message) {
                                echo $message;
                            }
                        ?>
            </div>

                
                <?php
            }

?>  