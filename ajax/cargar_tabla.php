<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_1.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_1.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/


    /*COLOR BACKGROUND SET*/
    table#sample_1.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_1.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>
<?php
function convertDMSToDecimal($latlng) {
    $valid = false;
    $decimal_degrees = 0;
    $degrees = 0; $minutes = 0; $seconds = 0; $direction = 1;

    // Determine if there are extra periods in the input string
    $num_periods = substr_count($latlng, '.');
    if ($num_periods > 1) {
        $temp = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // replace all but last period with delimiter
        $temp = trim(preg_replace('/[a-zA-Z]/','',$temp)); // when counting chunks we only want numbers
        $chunk_count = count(explode(" ",$temp));
        if ($chunk_count > 2) {
            $latlng = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // remove last period
        } else {
            $latlng = str_replace("."," ",$latlng); // remove all periods, not enough chunks left by keeping last one
        }
    }
    
    // Remove unneeded characters
    $latlng = trim($latlng);
    $latlng = str_replace("º"," ",$latlng);
    $latlng = str_replace("°"," ",$latlng);
    $latlng = str_replace("'"," ",$latlng);
    $latlng = str_replace("\""," ",$latlng);
    $latlng = str_replace("  "," ",$latlng);
    $latlng = substr($latlng,0,1) . str_replace('-', ' ', substr($latlng,1)); // remove all but first dash

    if ($latlng != "") {
        // DMS with the direction at the start of the string
        if (preg_match("/^([nsewoNSEWO]?)\s*(\d{1,3})\s+(\d{1,3})\s*(\d*\.?\d*)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[2]);
            $minutes = intval($matches[3]);
            $seconds = floatval($matches[4]);
            if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                $direction = -1;
        }
        // DMS with the direction at the end of the string
        elseif (preg_match("/^(-?\d{1,3})\s+(\d{1,3})\s*(\d*(?:\.\d*)?)\s*([nsewoNSEWO]?)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[1]);
            $minutes = intval($matches[2]);
            $seconds = floatval($matches[3]);
            if (strtoupper($matches[4]) == "S" || strtoupper($matches[4]) == "W" || $degrees < 0) {
                $direction = -1;
                $degrees = abs($degrees);
            }
        }
        if ($valid) {
            // A match was found, do the calculation
            $decimal_degrees = ($degrees + ($minutes / 60) + ($seconds / 3600)) * $direction;
        } else {
            // Decimal degrees with a direction at the start of the string
            if (preg_match("/^([nsewNSEW]?)\s*(\d+(?:\.\d+)?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                    $direction = -1;
                $decimal_degrees = $matches[2] * $direction;
            }
            // Decimal degrees with a direction at the end of the string
            elseif (preg_match("/^(-?\d+(?:\.\d+)?)\s*([nsewNSEW]?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[2]) == "S" || strtoupper($matches[2]) == "W" || $degrees < 0) {
                    $direction = -1;
                    $degrees = abs($degrees);
                }
                $decimal_degrees = $matches[1] * $direction;
            }
        }
    }
    if ($valid) {
        return $decimal_degrees;
    } else {
        return false;
    }
}
require_once("../conn.php");
 $idVis=$_POST['id'];

 $sqlExc="SELECT visExc FROM tblVisor WHERE idVisor='$idVis'";
 $resultExc = mysqli_query($con, $sqlExc);
 $rowExc = mysqli_fetch_array($resultExc);
 if(mysqli_fetch_array($resultExc)=='null'){
    $idVis="1";
    $rowExc['visExc']='nueva planilla importada';}

    echo '$idVis : '.$idVis;

    $response='';
    $response.='
    <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i>Plantilla 766 Importada - '.$rowExc['visExc'].'</div>
                                        <div class="tools"> </div>
                                    </div>
                                    
                                    <div class="portlet-body table-both-scroll">
    <table class="table table-striped table-bordered table-hover table-fixed text-nowrap order-column display" id="sample_1">

                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Capas</th>
                                                    <th>Subcapas</th>
                                                    <th>Ecoregión</th>
                                                    <th>Titulo</th>
                                                    <th>Descripción</th>
                                                    <th>Institución</th>
                                                    <th>Fuente</th>
                                                    <th>URL</th>
                                                    <th>Contacto</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                    <th>Mes</th>
                                                    <th>Año</th>
                                                    <th>Geometría</th>
                                                    <th>Altura m.s.</th>
                                                    <th># Taxones</th>
                                                    <th>Excel origen</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>';
    //$response = array(); 
/*BEGIN FOOTER
<tfoot>
<tr>
<th colspan="15" style="text-align:right">Total:&nbsp;&nbsp;</th>
<th></th>
</tr>
</tfoot>
END FOOTER*/
    $sqlSelectDisc="SELECT * FROM tblDisciplina";
    $resultDisc = mysqli_query($con, $sqlSelectDisc);
    $capa_source = '';
    while ($rowDis = mysqli_fetch_array($resultDisc)) {
        $capa_source.='<option type="listbox" value="'.$rowDis['idDisc'].'">'.$rowDis['nomDisc'].'</option>';
            }

    $sqlSelectVis="SELECT * FROM tblVisor WHERE idVisor='$idVis' AND visUlt='S' ORDER BY id ASC";
    $counter=0;
    $resultVis = mysqli_query($con, $sqlSelectVis);
    //$count=0;
        while ($rowVis = mysqli_fetch_array($resultVis)) {
    $counter=$counter+1;
            $insti_id=$rowVis['idIns'];
            $fuen_id=$rowVis['visFue'];
            $geo_id=$rowVis['idGeo'];

            $disc_id=$rowVis['visIdDisc'];
            $subdisc_id=$rowVis['visIdSubDisc'];
            $eco_id=$rowVis['visIdEco'];

            $sqlInsti="SELECT * FROM tblInstitucion WHERE idIns='$insti_id'";
            $resultInsti = mysqli_query($con, $sqlInsti);
            $rowInsti = mysqli_fetch_array($resultInsti);

            $sqlFuen="SELECT * FROM tblFuente WHERE idFue='$fuen_id'";
            $resultFuen = mysqli_query($con, $sqlFuen);
            $rowFuen = mysqli_fetch_array($resultFuen);

            $sqlGeo="SELECT * FROM tblGeometria WHERE idGeo='$geo_id'";
            $resultGeo = mysqli_query($con, $sqlGeo);
            $rowGeo = mysqli_fetch_array($resultGeo);

            $sqldisc="SELECT * FROM tblDisciplina WHERE idDisc='$disc_id'";
            $resultdisc = mysqli_query($con, $sqldisc);
            $rowdisc = mysqli_fetch_array($resultdisc);

            $sqlsubdisc="SELECT * FROM tblSubDisc WHERE idSubDisc='$subdisc_id'";
            $resultsubdisc = mysqli_query($con, $sqlsubdisc);
            $rowsubdisc = mysqli_fetch_array($resultsubdisc);

            $sqleco="SELECT * FROM tblEcoregion WHERE IdEco='$eco_id'";
            $resulteco = mysqli_query($con, $sqleco);
            $roweco = mysqli_fetch_array($resulteco);
                    

        $latitud_data=''.$rowVis['colLatG'].'° '.$rowVis['colLatM'].'\' '.$rowVis['colLatS'].'" '.$rowVis['colLatX'].'';
        $latitud = convertDMSToDecimal($latitud_data);
        $longitud_data=''.$rowVis['colLonG'].'° '.$rowVis['colLonM'].'\' '.$rowVis['colLonS'].'" '.$rowVis['colLonX'].'';
        $longitud = convertDMSToDecimal($longitud_data);

        $control='_'.$rowVis['id'].'_'.round(microtime(true)).'';

        if($rowVis['visDup']=='S'){
            $duplicado="marca2";
        }else{
            $duplicado="";
        }
            $response.='
            <tr class="'.$duplicado.'">
            ';

            if(trim($rowdisc['nomDisc'])!=''){
               $response.='
            <td>'.$rowVis['id'].'</td>
            <td>'.$rowdisc['nomDisc'].'</td>
            <td>'.$rowsubdisc['nomSubDisc'].'</td>
            <td>'.$roweco['nomEco'].'</td>
            '; 
        }else{
            $response.='
            <td>'.$rowVis['id'].'</td>
            <td list >
                <div class="form-group">            
                    <select name="capa_1[]" id="capa_1'.$control.'" size="4" multiple="multiple" style="display:" onchange="return capa1(this);">
                    '.$capa_source.'
                    </select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_2[]" id="capa_2'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa2(this);"></select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_3[]" id="capa_3'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa3(this);"></select>                           
                </div>
                 <div align="center">
                <div id="cargar_boton'.$control.'"></div>
                </div>
            </td>
            ';
        }

            $response.='
            
            <td>'.$rowVis['visTit'].'</td>
            <td>'.$rowVis['visDes'].'</td>
            <td>'.$rowInsti['insNom'].'</td>
            <td>'.$rowFuen['nomFue'].'</td>
            <td>'.$rowVis['visUrl'].'</td>
            <td>'.$rowVis['visCon'].'</td>
            <td>'.$latitud_data.'</td>
            <td>'.$longitud_data.'</td>
            <td>'.$rowVis['visMes'].'</td>
            <td>'.$rowVis['visAni'].'</td>
            <td>'.$rowGeo['geoNom'].'</td>
            <td>'.$rowVis['visAlt'].'</td>
            <td>'.$rowVis['visTax'].'</td>
            <td>'.$rowVis['visExc'].'</td>
            <td>
            <div align="center">
            <a class="btn btn-sm blue btn-outline filter-cancel" data-toggle="modal" data-target="#myModal101" onclick="editar_alert_import('.$rowVis['id'].','.$idVis.')"><i class="fa fa-edit fa-lg"></i> Editar</a>
            </div>
            <br>
            <div align="center">
            <a class="btn btn-sm red btn-outline filter-cancel" data-toggle="modal" data-target="#myModal100" onclick="eliminar_alert_import('.$rowVis['id'].')"><i class="fa fa-trash-o fa-lg"></i> Eliminar</a>
            </div>
            </td>
            </tr>';
            //$response[$count]['value']=$rowVis['idSubDisc'];
            //$response[$count]['name']=$rowViss['nomSubDisc'];
            //$count=$count+1;


        }
        $response.=' </tbody>
                                        </table>
                                        </div>
                                     
                                </div>';
    echo $response;     

?>  

 <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>


<script>
    function editar_alert_import(id,reg){

            $.ajax({
                url: "ajax/editar_alert_import.php",
                method:"post",
                data:{id:id, reg:reg},
                success:function(data){
                    $('#resultados_ajax101').empty();
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }
    function eliminar_alert(id){

            $.ajax({
                url: "ajax/eliminar_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }

</script>   
