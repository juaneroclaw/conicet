<?php
session_start();
require_once("../conn.php");
$id=$_POST['id'];
//$id_visor=$_POST['reg'];
$control=''.$_POST['id'].''.round(microtime(true)).'';

/*$option_fue='';
$option_geo='';
$option_insti='';

$sqlSelectFue = "SELECT * FROM tblFuente";
$resultFue = mysqli_query($con, $sqlSelectFue);
while ($rowFue = mysqli_fetch_array($resultFue)) {
$option_fue.='<option id="fuen" name="fuen" value="'.$rowFue['idFue'].'">'.$rowFue['nomFue'].'</option>';
}

$sqlSelectgeo = "SELECT * FROM tblGeometria";
$resultgeo = mysqli_query($con, $sqlSelectgeo);
while ($rowgeo = mysqli_fetch_array($resultgeo)) {
$option_geo.='<option id="geom" name="geom" value="'.$rowgeo['idGeo'].'">'.$rowgeo['geoNom'].'</option>';
}

$sqlSelectInsti = "SELECT * FROM tblInstitucion ORDER BY insNom ASC";
$resultInsti = mysqli_query($con, $sqlSelectInsti);
while ($rowInsti = mysqli_fetch_array($resultInsti)) {
$option_insti.='<option id="insti" name="insti" value="'.$rowInsti['idIns'].'">'.$rowInsti['insNom'].'</option>';
}*/


$sqlSelectVis="SELECT * FROM tblVisor t1 JOIN tblDisciplina t2 ON t1.visIdDisc=t2.idDisc JOIN tblSubDisc t3 ON (t1.visIdDisc=t3.idDisc AND t1.visIdSubDisc=t3.idSubDisc) JOIN tblEcoregion t4 ON (t1.visIdDisc=t4.idDisc AND t1.visIdSubDisc=t4.idSubDisc AND t1.visIdEco=t4.IdEco) JOIN tblInstitucion t5 ON t1.idIns=t5.idIns JOIN tblGeometria t6 ON t1.idGeo=t6.idGeo JOIN tblFuente t7 ON t1.visFue=t7.idFue WHERE id='$id'";
$resultVis = mysqli_query($con, $sqlSelectVis);
$rowVis = mysqli_fetch_array($resultVis);

$output="";


$output .='
    
<input type="hidden" class="form-control" id="id" name="id" value='.$_POST['id'].'>
<input type="hidden" class="form-control" id="id_visor" name="id_visor" value='.$rowVis['idVisor'].'>
<input type="hidden" class="form-control" id="capa1" name="capa1" value='.$rowVis['nomDisc'].'>
<input type="hidden" class="form-control" id="capa2" name="capa2" value='.$rowVis['nomSubDisc'].'>
<input type="hidden" class="form-control" id="capa3" name="capa3" value='.$rowVis['nomEco'].'>
<input type="hidden" class="form-control" id="email_u" name="email_u" value='.$_SESSION['email'].'>
<input type="hidden" class="form-control" id="visExp" name="visExp" value='.$rowVis['visExp'].'>
<input type="hidden" class="form-control" id="visExc" name="visExc" value='.$rowVis['visExc'].'>
<input type="hidden" class="form-control" id="visUlt" name="visUlt" value='.$rowVis['visUlt'].'>
<input type="hidden" class="form-control" id="visAcro" name="visAcro" value='.$rowVis['visAcro'].'>
<input type="hidden" class="form-control" id="visNroCol" name="visNroCol" value='.$rowVis['visNroCol'].'>
<input type="hidden" class="form-control" id="visOri" name="visOri" value='.$rowVis['visOri'].'>
<input type="hidden" class="form-control" id="visExpor" name="visExpor" value='.$rowVis['visExpor'].'>
<input type="hidden" class="form-control" id="visDup" name="visDup" value='.$rowVis['visDup'].'>

<input type="hidden" class="form-control" id="insti" name="insti" value='.$rowVis['idIns'].'>
<input type="hidden" class="form-control" id="geom" name="geom" value='.$rowVis['idGeo'].'>
<input type="hidden" class="form-control" id="fuen" name="fuen" value='.$rowVis['visFue'].'>
<input type="hidden" class="form-control" id="page" name="page" value="data">

';

if($rowVis['nomDisc']!=''){

$output .='
<input type="hidden" class="form-control" id="capa_1" name="capa_1" value='.$rowVis['visIdDisc'].'>
<input type="hidden" class="form-control" id="capa_2" name="capa_2" value='.$rowVis['visIdSubDisc'].'>
<input type="hidden" class="form-control" id="capa_3" name="capa_3" value='.$rowVis['visIdEco'].'>
<input type="hidden" class="form-control" id="validator" name="validator" value="ok">

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Capa <i class="fa fa-angle-double-right"></i> SubCapa <i class="fa fa-angle-double-right"></i> Ecoregion:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="capa123" name="capa123" class="form-control input-md" value="'.$rowVis['nomDisc'].' » '.$rowVis['nomSubDisc'].' » '.$rowVis['nomEco'].'" readonly> 
         </div> 
    </div>                    
</div>
';

}else{

$output .='
<input type="hidden" class="form-control" id="validator" name="validator" value="fail">

<div class="form-group m-form__group row">
    <div class="col-md-12"> 
        <label><strong>Capa(s):</strong></label>    
            <div class="input-icon input-icon-md right">      
                <select name="capa_1[]" id="capa_1'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
            </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">  
        <label><strong>Subcapa(s):</strong></label>    
            <div class="input-icon input-icon-md right">     
                <select name="capa_2[]" id="capa_2'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
                </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Ecoregion(es):</strong></label>
        <div class="input-icon input-icon-md right"> 
            <select name="capa_3[]" id="capa_3'.$control.'" size="4" multiple="multiple" style="display: none;" ></select>
            </div>
    </div>
</div>
';

}


/* BEGIN SELECT MULTIPLE 
$output .='
<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Fuente:</strong></label>
        <div class="input-icon input-icon-md right">
        <select name="fuen" id="fuen" class="form-control input-md">

                    '.$option_fue.'
            </select>
            
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Institucion:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="insti" id="insti" class="form-control input-md">

                    '.$option_insti.'
            </select> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Geometria:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="geom" id="geom" class="form-control input-md">

                    '.$option_geo.'
            </select>
        </div>
    </div>  
</div>
';
END SELECT MULTIPLE*/

$output .='

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Fuente:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="fuen2" name="fuen2" class="form-control input-md" value="'.$rowVis['nomFue'].'" readonly> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Titulo:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visTit" name="visTit" class="form-control input-md" value="'.$rowVis['visTit'].'"> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Descripcion:</strong></label>
        <div class="input-icon input-icon-md right">
            <textarea id="visDes" name="visDes" class="form-control input-md" rows="2">'.str_replace("<br />", "", nl2br($rowVis['visDes'])).'</textarea>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Institucion:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="insti2" name="insti2" class="form-control input-md" value="'.$rowVis['insNom'].'" readonly> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>URL:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visUrl" name="visUrl" class="form-control input-md" value="'.$rowVis['visUrl'].'" readonly> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Contacto:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visCon" name="visCon" class="form-control input-md" value="'.$rowVis['visCon'].'" readonly> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Latitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">

    <div class="col-md-3">
        <label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="outGra" name="outGra" class="form-control input-md" value="'.$rowVis['colLatG'].'" readonly> 
        </div>
    </div>

    <div class="col-md-3">
        <label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="outMin" name="outMin" class="form-control input-md" value="'.$rowVis['colLatM'].'" readonly> 
        </div>
    </div>   

    <div class="col-md-3">
        <label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="outSeg" name="outSeg" class="form-control input-md" value="'.$rowVis['colLatS'].'" readonly> 
        </div>
    </div>  

    <div class="col-md-3">
        <label>Coordenada:</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="d" name="d" class="form-control input-md" value="'.$rowVis['colLatX'].'" readonly> 
        </div>
    </div>  

</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Longitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">

    <div class="col-md-3">
        <label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="glo" name="glo" class="form-control input-md" value="'.$rowVis['colLonG'].'" readonly> 
        </div>
    </div>

    <div class="col-md-3">
        <label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="mlo" name="mlo" class="form-control input-md" value="'.$rowVis['colLonM'].'" readonly> 
        </div>
    </div>   

    <div class="col-md-3">
        <label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="slo" name="slo" class="form-control input-md" value="'.$rowVis['colLonS'].'" readonly> 
        </div>
    </div>  

    <div class="col-md-3">
        <label>Coordenada:</label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="dlo" name="dlo" class="form-control input-md" value="'.$rowVis['colLonX'].'" readonly> 
        </div>
    </div>  

</div>

<div class="form-group m-form__group row">

    <div class="col-md-6">
        <label><strong>Mes:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visMes" name="visMes" class="form-control input-md" value="'.$rowVis['visMes'].'" readonly> 
        </div>
    </div>

    <div class="col-md-6">
        <label><strong>Año:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visAni" name="visAni" class="form-control input-md" value="'.$rowVis['visAni'].'" readonly> 
        </div>
    </div>   


</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Geometria:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="geom2" name="geom2" class="form-control input-md" value="'.$rowVis['nomFue'].'" readonly> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label><strong>Altura:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visAlt" name="visAlt" class="form-control input-md" value="'.$rowVis['visAlt'].'" readonly> 
        </div>
    </div> 

    <div class="col-md-6">
        <label><strong>Taxones:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visTax" name="visTax" class="form-control input-md" value="'.$rowVis['visTax'].'" readonly> 
        </div>
    </div>  

</div>
    ';
        

echo $output;
            

?>
<script>
    $(document).on('change','#capa_1<?php echo $control; ?>',function(){
        
    cap1 = $('#capa_1<?php echo $control; ?>').val(); 
   // alert(cap1);
    $("#capa_2<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_2<?php echo $control; ?>").css("display","none");
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa2_modal2(cap1);  
})
</script>
<script>
    $(document).on('change','#capa_2<?php echo $control; ?>',function(){
    
    cap2 = $('#capa_2<?php echo $control; ?>').val(); 
    //alert(cap2);
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa3_modal2(cap2);  
})
</script>
<script>
    $(document).on('change','#capa_3<?php echo $control; ?>',function(){
    cap3 = $('#capa_3<?php echo $control; ?>').val(); 
    //alert(cap3);  
})
</script>

<script>
    $(document).ready(function(){
            load_capa1_modal2();
            
        });
    function load_capa1_modal2(page){
        
        $.ajax({
            url:'ajax/cargar_capa1.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#capa_1<?php echo $control; ?>").append(o);
                    $("#capa_1<?php echo $control; ?>").css("display","");
                });
               
            }
        })
    }

  function cargar_capa2_modal2(id){

            $.ajax({
                url: "ajax/cargar_capa2.php",
                method:"post",
                dataType: 'json',
                //data: {id : jsonString},
                data:{id:id},
                success: function(response){
                
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_2<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap2<?php echo $control; ?>" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap2<?php echo $control; ?>').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_2<?php echo $control; ?>").css("display","");
                    
                }
            });
            }

function cargar_capa3_modal2(id){
            $.ajax({
                url: "ajax/cargar_capa3.php",
                method:"post",
                dataType: 'json',
                data:{id:id},
                 success: function(response){
                    
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_3<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap3<?php echo $control; ?>" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap3<?php echo $control; ?>').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_3<?php echo $control; ?>").css("display","");
                    
                }
            });
            }
</script>
<script>
    //document.getElementById("geom").value = "<?php echo $rowVis['idGeo']; ?>";
    //document.getElementById("fuen").value = "<?php echo $rowVis['idFue']; ?>";
    //document.getElementById("insti").value = "<?php echo $rowVis['idIns']; ?>";
</script>


