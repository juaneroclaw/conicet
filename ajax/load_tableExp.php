<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_1.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_1.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/


    /*COLOR BACKGROUND SET*/
    table#sample_1.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_1.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>
<?php
function convertDMSToDecimal($latlng) {
    $valid = false;
    $decimal_degrees = 0;
    $degrees = 0; $minutes = 0; $seconds = 0; $direction = 1;

    // Determine if there are extra periods in the input string
    $num_periods = substr_count($latlng, '.');
    if ($num_periods > 1) {
        $temp = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // replace all but last period with delimiter
        $temp = trim(preg_replace('/[a-zA-Z]/','',$temp)); // when counting chunks we only want numbers
        $chunk_count = count(explode(" ",$temp));
        if ($chunk_count > 2) {
            $latlng = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // remove last period
        } else {
            $latlng = str_replace("."," ",$latlng); // remove all periods, not enough chunks left by keeping last one
        }
    }
    
    // Remove unneeded characters
    $latlng = trim($latlng);
    $latlng = str_replace("º"," ",$latlng);
    $latlng = str_replace("°"," ",$latlng);
    $latlng = str_replace("'"," ",$latlng);
    $latlng = str_replace("\""," ",$latlng);
    $latlng = str_replace("  "," ",$latlng);
    $latlng = substr($latlng,0,1) . str_replace('-', ' ', substr($latlng,1)); // remove all but first dash

    if ($latlng != "") {
        // DMS with the direction at the start of the string
        if (preg_match("/^([nsewoNSEWO]?)\s*(\d{1,3})\s+(\d{1,3})\s*(\d*\.?\d*)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[2]);
            $minutes = intval($matches[3]);
            $seconds = floatval($matches[4]);
            if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                $direction = -1;
        }
        // DMS with the direction at the end of the string
        elseif (preg_match("/^(-?\d{1,3})\s+(\d{1,3})\s*(\d*(?:\.\d*)?)\s*([nsewoNSEWO]?)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[1]);
            $minutes = intval($matches[2]);
            $seconds = floatval($matches[3]);
            if (strtoupper($matches[4]) == "S" || strtoupper($matches[4]) == "W" || $degrees < 0) {
                $direction = -1;
                $degrees = abs($degrees);
            }
        }
        if ($valid) {
            // A match was found, do the calculation
            $decimal_degrees = ($degrees + ($minutes / 60) + ($seconds / 3600)) * $direction;
        } else {
            // Decimal degrees with a direction at the start of the string
            if (preg_match("/^([nsewNSEW]?)\s*(\d+(?:\.\d+)?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                    $direction = -1;
                $decimal_degrees = $matches[2] * $direction;
            }
            // Decimal degrees with a direction at the end of the string
            elseif (preg_match("/^(-?\d+(?:\.\d+)?)\s*([nsewNSEW]?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[2]) == "S" || strtoupper($matches[2]) == "W" || $degrees < 0) {
                    $direction = -1;
                    $degrees = abs($degrees);
                }
                $decimal_degrees = $matches[1] * $direction;
            }
        }
    }
    if ($valid) {
        return $decimal_degrees;
    } else {
        return false;
    }
}
require_once("../conn.php");
 // $idVis=$_POST['idExp'];
 // $idIns=$_POST['idIns'];

    $response='';
    $response.='
    <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i>INECOA Expedientes MA para Visor</div>
                                        <div class="tools"> </div>
                                    </div>
                                    
                                    <div class="portlet-body table-both-scroll">
    <table class="table table-striped table-bordered table-hover table-fixed text-nowrap display" id="sample_1">

                                            <thead>
                                                <tr>
                                                    <th>Institución</th>
                                                    <th>Ubicación</th>
                                                    <th>Nro. Exp.</th>
                                                    <th>Iniciado</th>
                                                    <th>Consul. Amb.</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Dpto.</th>
                                                    <th>Localid.</th>
                                                    <th>Área protegida</th>
                                                    <th>Name_ap</th>
                                                    <th>Catastro</th>
                                                    <th>Lote</th>
                                                    <th>Estado</th>
                                                    <th>Uso suelo</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                    <th>Altura m.s.</th>
                                                    <th>Geometría</th>
                                                    <th>Observador</th>
                                                    <th>Contacto</th>
                                                    <th>Fec. Observac.</th>
                                                    <th>Fuente</th>
                                                    <th>URL</th>
                                                    <th>Contacto</th>
                                                    <th># Taxones</th>
                                                    <th>Cnjt. Esp.</th>
                                                    <th>Grupo</th>
                                                    <th>Familia</th>
                                                    <th>Género</th>
                                                    <th>Especie</th>
                                                    <th>Subespecie</th>
                                                    <th>Nom. Vulgar</th>
                                                    <th>Tipo Reg.</th>
                                                    <th>Sexo</th>
                                                    <th>Clasificación</th>
                                                    <th>Mes</th>
                                                    <th>Año</th>
                                                    <th>Rastros</th>
                                                    <th>Observaciones</th>
                                                    <th>Excel origen</th>
                                                    <th>Exportado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody>';
    //$response = array(); 

//  *-* *-*    EN EXPEDIENTES :  NO VAN CAPAS

/*    $sqlSelectDisc="SELECT * FROM tblDisciplina";
    $resultDisc = mysqli_query($con, $sqlSelectDisc);
    $capa_source = '';
    while ($rowDis = mysqli_fetch_array($resultDisc)) {
        $capa_source.='<option type="listbox" value="'.$rowDis['idDisc'].'">'.$rowDis['nomDisc'].'</option>';
            }
*/
//    $sqlSelectVis="SELECT * FROM tblExpediente t1 JOIN tblInstitucion t5 ON t1.expIdIns=t5.idIns JOIN tblGeometria t6 ON t1.expIdGeo=t6.idGeo JOIN tblFuente t7 ON t1.expFue=t7.idFue WHERE (expUlt='S' OR expUlt='Y') AND expExpor='N' AND (expOri=1 OR expOri=4) ORDER BY insNom ASC";
    
/*    $sqlSelectVis="SELECT 't1.id', `idExp`, `expIdIns`, 't5.insNom AS Institucion', `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`,
 `expDpto`, 't12.Nombre AS Dpto', `expLoc`, 't14.Nombre AS Loca', `expApr`, `expNap`, `expEco`, 't15.nomEco AS Ecoreg', `expCat`, `expLot`, `expEst`, `expIdUsoSue`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `expAlt`, `expIdGeo`, 't6.geoNom AS Geometria', `expVador`, `expCon`, `expFob`, `expFue`, 't7.nomFue AS Fuente', `expUrl`, `expTax`, `expIdGru`, 't2.nomGru AS Grupo', `expCju`, 't3.nomCju AS Conjunto', `expIdFlia`, 't4.nomFlia AS Flia', `expIdGen`, 't8.nomGen AS Genero', `expIdEsp`, 't9.nomEsp AS Especie', `expIdSubEsp`, 't10.nomSubEsp AS Subespecie', `expVul`, `expTipoReg`, `expSex`, `expIdCla`, 't11.claNom AS Clasific', `expMes`, `expAno`, `expRas`, `expObs`, 
    `expExc`, `expOri`, `expUlt`, `expExp`, `expExpor`, `expDup`, `expEmail` 
    FROM tblExpediente t1 JOIN tblInstitucion t5 ON t1.expIdIns=t5.idIns 
    JOIN tblGeometria t6 ON t1.expIdGeo=t6.idGeo 
    JOIN tblFuente t7 ON t1.expFue=t7.idFue 
    JOIN departamento t13 ON t1.expDpto=t13.Nombre 
    JOIN localidad t14 ON t1.expLoc=t14.Nombre 
    JOIN tblEcoregion t15 ON t1.expEco=t15.idEco 
    JOIN tblGrupo t2 ON t1.expIdGru=t2.idGru 
    JOIN tblConjunto t3 ON t1.expCju=t3.idCju 
    JOIN tblFamilia t4 ON t1.expIdFlia=t4.idFlia 
    JOIN tblGenero t8 ON t1.expIdGen=t8.IdGen 
    JOIN tblespecie t9 ON t1.expIdEsp=t9.idEsp 
    JOIN tblsubespecie t10 ON t1.expIdSubEsp=t10.idSubEsp 
    JOIN tblclasificacion t11 ON t1.expIdCla=t11.idCla 
    WHERE (expUlt='S' OR expUlt='Y') AND expExpor='N' AND (expOri=4 OR expOri=5) 
    ORDER BY insNom ASC";
*/
    $sqlSelectVis="SELECT * FROM tblExpediente WHERE (expOri=1 OR expOri=4) ORDER BY expUbi, expNro ASC";

    $counter=0;
    $resultVis = mysqli_query($con, $sqlSelectVis);

        //$count=0;
        while ($rowVis = mysqli_fetch_array($resultVis)) {

             //if(trim($rowVis['nomDisc'])!=''){

            $counter=$counter+1;

            $insti_id = $rowVis['expIdIns'];
            $sqlInsti = "SELECT * FROM tblInstitucion WHERE idIns='$insti_id'";
            $resultInsti = mysqli_query($con, $sqlInsti);
            $rowInsti = mysqli_fetch_array($resultInsti);
            $insti_id = $rowInsti['insNom'];

            $fuen_id=$rowVis['expFue'];
            $sqlFuen="SELECT * FROM tblFuente WHERE idFue='$fuen_id'";
            $resultFuen = mysqli_query($con, $sqlFuen);
            $rowFuen = mysqli_fetch_array($resultFuen);
            $fuen_id=$rowFuen['nomFue'];

            $geo_id=$rowVis['expIdGeo'];
            $sqlGeo="SELECT * FROM tblGeometria WHERE idGeo='$geo_id'";
            $resultGeo = mysqli_query($con, $sqlGeo);
            $rowGeo = mysqli_fetch_array($resultGeo);
            $geo_id=$rowGeo['geoNom'];

      /*      $sqldisc="SELECT * FROM tblDisciplina WHERE idDisc='$disc_id'";
            $resultdisc = mysqli_query($con, $sqldisc);
            $rowdisc = mysqli_fetch_array($resultdisc);

            $sqlsubdisc="SELECT * FROM tblSubDisc WHERE idSubDisc='$subdisc_id'";
            $resultsubdisc = mysqli_query($con, $sqlsubdisc);
            $rowsubdisc = mysqli_fetch_array($resultsubdisc);
*/
            $eco_id=$rowVis['expEco'];
            $sqleco="SELECT * FROM tblEcoregion WHERE idEco='$eco_id'";
            $resulteco = mysqli_query($con, $sqleco);
            $roweco = mysqli_fetch_array($resulteco);
            $eco_id = $roweco['nomEco'];
                    
            $cju_id=$rowVis['expCju'];
            $sqlcju="SELECT * FROM tblConjunto WHERE idCju='$cju_id'";
            $resultcju = mysqli_query($con, $sqlcju);
            $rowcju = mysqli_fetch_array($resultcju);
            $cju_id = $rowcju['nomCju'];
                    
            $gru_id=$rowVis['expIdGru'];
            $sqlgru = "SELECT * FROM tblGrupo WHERE idGru='$gru_id'";
            $resultgru = mysqli_query($con, $sqlgru);
            $rowgru = mysqli_fetch_array($resultgru);
            $gru_id = $rowgru['nomGru'];
                    
            $flia_id=$rowVis['expIdFlia'];
            $sqlflia = "SELECT * FROM tblFamilia WHERE idFlia='$flia_id'";
            $resultflia = mysqli_query($con, $sqlflia);
            $rowflia = mysqli_fetch_array($resultflia);
            $flia_id = $rowflia['nomFlia'];
                    
            $gen_id = $rowVis['expIdGen'];
            $sqlgen = "SELECT * FROM tblGenero WHERE idGen='$gen_id'";
            $resultgen = mysqli_query($con, $sqlgen);
            $rowgen = mysqli_fetch_array($resultgen);
            $gen_id = $rowgen['nomGen'];
                    
            $esp_id = $rowVis['expIdEsp'];
            $sqlesp = "SELECT * FROM tblEspecie WHERE idEsp='$esp_id'";
            $resultesp = mysqli_query($con, $sqlesp);
            $rowesp = mysqli_fetch_array($resultesp);
            $esp_id = $rowesp['nomEsp'];
                        
            $sesp_id = $rowVis['expIdSubEsp'];
            $sqlsesp = "SELECT * FROM tblSubespecie WHERE idSubEsp='$sesp_id'";
            $resultsesp = mysqli_query($con, $sqlsesp);
            $rowsesp = mysqli_fetch_array($resultsesp);
            $sesp_id = $rowsesp['nomSubEsp'];
                        
            $cla_id = $rowVis['expIdCla'];
            $sqlcla = "SELECT * FROM tblClasificacion WHERE idCla ='$cla_id'";
            $resultcla = mysqli_query($con, $sqlcla);
            $rowcla = mysqli_fetch_array($resultcla);
            $cla_id = $rowcla['claNom'];
                        
    
        $latitud_data=''.$rowVis['colLatG'].'° '.$rowVis['colLatM'].'\' '.$rowVis['colLatS'].'" '.$rowVis['colLatX'].'';
        $latitud = convertDMSToDecimal($latitud_data);
        $longitud_data=''.$rowVis['colLonG'].'° '.$rowVis['colLonM'].'\' '.$rowVis['colLonS'].'" '.$rowVis['colLonX'].'';
        $longitud = convertDMSToDecimal($longitud_data);

//        $control='_'.$rowVis['idExp'].'_'.round(microtime(true)).'';

        if($rowVis['expDup']=='S'){
            $duplicado="marca2";
        }else{
            $duplicado="";
        }
            $response.='
            <tr class="'.$duplicado.'">
            ';

/*        if(trim($rowdisc['nomDisc'])!=''){
               $response.='
            <td>'.$rowVis['id'].'</td>
            <td>'.$rowdisc['nomDisc'].'</td>
            <td>'.$rowsubdisc['nomSubDisc'].'</td>
            <td>'.$roweco['nomEco'].'</td>
            '; 
        }else{
            $response.='
            <td>'.$rowVis['id'].'</td>
            <td list >
                <div class="form-group">            
                    <select name="capa_1[]" id="capa_1_table'.$control.'" size="4" multiple="multiple" style="display:" onchange="return capa1_table(this);">
                    '.$capa_source.'
                    </select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_2[]" id="capa_2_table'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa2_table(this);"></select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_3[]" id="capa_3_table'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa3_table(this);"></select>                           
                </div>
                 <div align="center">
                <div id="cargar_boton_table'.$control.'"></div>
                </div>
            </td>
            ';
        }
*/
            $response.='
            <td>'.$rowInsti['insNom'].'</td>
            <td>'.$rowVis['expUbi'].'</td>
            <td>'.$rowVis['expNro'].'</td>
            <td>'.$rowVis['expIni'].'</td>
            <td>'.$rowVis['expTor'].'</td>
            <td>'.$rowVis['expTit'].'</td>
            <td>'.$rowVis['expDes'].'</td>
            <td>'.$rowVis['expDpto'].'</td>
            <td>'.$rowVis['expLoc'].'</td>
            <td>'.$rowVis['expApr'].'</td>
            <td>'.$rowVis['expNap'].'</td>
            <td>'.$roweco['nomEco'].'</td>
            <td>'.$rowVis['expCat'].'</td>
            <td>'.$rowVis['expLot'].'</td>
            <td>'.$rowVis['expEst'].'</td>
            <td>'.$rowVis['expIdUsoSue'].'</td>
            <td>'.$latitud_data.'</td>
            <td>'.$longitud_data.'</td>
            <td>'.$rowVis['expAlt'].'</td>
            <td>'.$rowGeo['geoNom'].'</td>
            <td>'.$rowVis['expVador'].'</td>
            <td>'.$rowVis['expCon'].'</td>
            <td>'.$rowVis['expFob'].'</td>
            <td>'.$rowFuen['nomFue'].'</td>
            <td>'.$rowVis['expUrl'].'</td>
            <td>'.$rowVis['expTax'].'</td>
            
            <td>'.$gru_id.'</td>
            <td>'.$cju_id.'</td>
            <td>'.$flia_id.'</td>
            <td>'.$gen_id.'</td>
            <td>'.$esp_id.'</td>
            <td>'.$sesp_id.'</td>
            <td>'.$rowVis['expVul'].'</td>
            <td>'.$rowVis['expTipoReg'].'</td>
            <td>'.$rowVis['expSex'].'</td>
            <td>'.$cla_id.'</td>
            
            <td>'.$rowVis['expMes'].'</td>
            <td>'.$rowVis['expAno'].'</td>
            <td>'.$rowVis['expRas'].'</td>
            <td>'.$rowVis['expObs'].'</td>
            <td>'.$rowVis['expExc'].'</td>
            <td>'.$rowVis['expExpor'].'</td>

            <td>
            <a class="btn btn-sm blue btn-outline filter-cancel" onclick="edit_data('.$rowVis['idExp'].')"><i class="fa fa-edit fa-lg"></i> Editar</a>
            <a class="btn btn-sm red btn-outline filter-cancel" onclick="delete_data('.$rowVis['idExp'].')"><i class="fa fa-trash-o fa-lg"></i> Eliminar</a>
            </td>
            </tr>';
            //$response[$count]['value']=$rowVis['idSubDisc'];
            //$response[$count]['name']=$rowViss['nomSubDisc'];
            //$count=$count+1;


        //}
        }
        $response.=' </tbody>
                                        </table>
                                        </div>
                                     
                                </div>';
    echo $response;     

?>  

<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttonsExp.js" type="text/javascript"></script>

<script>

    function edit_data(id){
            $.ajax({
                url: "ajax/edit_data_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax101').empty();
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }

    function delete_data(id){
            $.ajax({
                url: "ajax/delete_data_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }
</script>
