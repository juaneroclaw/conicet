<?php
session_start();
require_once("../conn.php");
require '../vendor/autoload.php';
$inputFileName='../example.xlsx';

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($inputFileName);
$reader->setLoadSheetsOnly(["Sheet 1", "Hoja1"]);
$spreadsheet = $reader->load($inputFileName);

$worksheet = $spreadsheet->getActiveSheet();

$response = array();
$response['data']=$_POST['id'];

$counter=0;
$response['aprobado']='FAIL';
foreach($_POST['id'] AS $id){

$SelectId=$id;
$sql="SELECT * FROM tblExpediente t1 JOIN tblInstitucion t5 ON t1.expIdIns=t5.idIns JOIN tblGeometria t6 ON t1.expIdGeo=t6.idGeo 
JOIN tblFuente t7 ON t1.expFue=t7.idFue WHERE (expUlt='S' OR expUlt='Y') AND expExpor='N' AND (expOri=1 OR expOri=4) WHERE t1.id='$SelectId' ORDER BY insNom ASC";
$results = mysqli_query($con, $sql);
$rows = mysqli_fetch_array($results);

if($rows['insNom']!=""){
$counter=$counter+1;
$response['aprobado']='OK';
$latitud_data=''.$rows['colLatG'].'° '.$rows['colLatM'].'\' '.$rows['colLatS'].'" '.$rows['colLatX'].'';
$longitud_data=''.$rows['colLonG'].'° '.$rows['colLonM'].'\' '.$rows['colLonS'].'" '.$rows['colLonX'].'';
$worksheet->getCell('A'.$counter.'')->setValue($rows['nomDisc']);
$worksheet->getCell('B'.$counter.'')->setValue($rows['nomSubDisc']);
$worksheet->getCell('C'.$counter.'')->setValue($rows['nomEco']);
$worksheet->getCell('D'.$counter.'')->setValue($rows['visTit']);
$worksheet->getCell('E'.$counter.'')->setValue($rows['visDes']);
$worksheet->getCell('F'.$counter.'')->setValue($rows['insNom']);
$worksheet->getCell('G'.$counter.'')->setValue($rows['nomFue']);
$worksheet->getCell('H'.$counter.'')->setValue($rows['visUrl']);
$worksheet->getCell('I'.$counter.'')->setValue($rows['visCon']);
$worksheet->getCell('J'.$counter.'')->setValue($latitud_data);
$worksheet->getCell('K'.$counter.'')->setValue($longitud_data);
$worksheet->getCell('L'.$counter.'')->setValue($rows['visMes']);
$worksheet->getCell('M'.$counter.'')->setValue($rows['visAni']);
$worksheet->getCell('N'.$counter.'')->setValue($rows['geoNom']);
$worksheet->getCell('O'.$counter.'')->setValue($rows['visAlt']);
$worksheet->getCell('P'.$counter.'')->setValue($rows['visTax']);
$worksheet->getCell('Q'.$counter.'')->setValue($rows['visExc']);
				
$sql_del="UPDATE tblVisor SET visUlt='N', visExpor='S' WHERE id ='".$rows['id']."'";
$query_update = $con->query($sql_del);
}

}
if($response['aprobado']=='OK'){
$micro_time = round(microtime(true));
$temp="data_export_".$counter."_".$micro_time.".xlsx";
$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
$writer->save("../bajadas/".$temp."");

$response['link']="bajadas/".$temp."";	
}


echo json_encode($response);
?>	

		
		
	