<?php
session_start();
require_once("../conn.php");
$id_visor=$_POST['id'];
$control=''.$_POST['id'].''.round(microtime(true)).'';
$option_fue='';
$option_geo='';
$option_insti='';

$sqlSelectFue = "SELECT * FROM tblFuente";
$resultFue = mysqli_query($con, $sqlSelectFue);
while ($rowFue = mysqli_fetch_array($resultFue)) {
$option_fue.='<option id="fuen" name="fuen" value="'.$rowFue['idFue'].'">'.$rowFue['nomFue'].'</option>';
}

$sqlSelectgeo = "SELECT * FROM tblGeometria";
$resultgeo = mysqli_query($con, $sqlSelectgeo);
while ($rowgeo = mysqli_fetch_array($resultgeo)) {
$option_geo.='<option id="geom" name="geom" value="'.$rowgeo['idGeo'].'">'.$rowgeo['geoNom'].'</option>';
}

$sqlSelectInsti = "SELECT * FROM tblInstitucion ORDER BY insNom ASC";
$resultInsti = mysqli_query($con, $sqlSelectInsti);
while ($rowInsti = mysqli_fetch_array($resultInsti)) {
$option_insti.='<option id="insti" name="insti" value="'.$rowInsti['idIns'].'">'.$rowInsti['insNom'].'</option>';
}

$output="";

$output .='
	
<input type="hidden" class="form-control" id="id" name="id" value='.$_POST['id'].'>
<input type="hidden" class="form-control" id="email_u" name="email_u" value='.$_SESSION['email'].'>
<input type="hidden" class="form-control" id="visExc" name="visExc" value="0">
<input type="hidden" class="form-control" id="visUlt" name="visUlt" value="Y">
<input type="hidden" class="form-control" id="visAcro" name="visAcro" value="">
<input type="hidden" class="form-control" id="visNroCol" name="visNroCol" value="0">
<input type="hidden" class="form-control" id="visOri" name="visOri" value="4">
<input type="hidden" class="form-control" id="visExpor" name="visExpor" value="N">
<input type="hidden" class="form-control" id="visDup" name="visDup" value="">
<input type="hidden" class="form-control" id="page" name="page" value="new">

<input type="hidden" class="form-control" id="validator" name="validator" value="ok">


<div class="form-group m-form__group row">
    <div class="col-md-12"> 
        <label><strong>Capa(s):</strong></label>    
            <div class="input-icon input-icon-md right">      
                <select name="capa_1[]" id="capa_1'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
            </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">  
        <label><strong>Subcapa(s):</strong></label>    
            <div class="input-icon input-icon-md right">     
                <select name="capa_2[]" id="capa_2'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
                </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Ecoregion(es):</strong></label>
        <div class="input-icon input-icon-md right"> 
            <select name="capa_3[]" id="capa_3'.$control.'" size="4" multiple="multiple" style="display: none;" ></select>
            </div>
    </div>
</div>





<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Titulo:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visTit" name="visTit" class="form-control input-md"> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Descripcion:</strong></label>
        <div class="input-icon input-icon-md right">
        	<textarea id="visDes" name="visDes" class="form-control input-md" rows="2"></textarea>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12"> 
        <label><strong>Institucion:</strong></label>    
            <div class="input-icon input-icon-md right">      
                <select name="insti[]" id="insti" size="4" multiple="multiple" style="width: 100%; display: none;"></select>
            </div>
    </div>
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Fuente:</strong></label>
        <div class="input-icon input-icon-md right">
        <select name="fuen" id="fuen" class="form-control input-md">

                    '.$option_fue.'
            </select>
            
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>URL:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visUrl" name="visUrl" class="form-control input-md" > 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Contacto:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visCon" name="visCon" class="form-control input-md" > 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Latitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">

	<div class="col-md-3">
		<label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outGra" name="outGra" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-3">
		<label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outMin" name="outMin" class="form-control input-md" > 
        </div>
    </div>   

    <div class="col-md-3">
		<label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outSeg" name="outSeg" class="form-control input-md" > 
        </div>
    </div>  

    <div class="col-md-3">
		<label>Coordenada:</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="d" name="d" class="form-control input-md" > 
        </div>
    </div>  

</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Longitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">

	<div class="col-md-3">
		<label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="glo" name="glo" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-3">
		<label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="mlo" name="mlo" class="form-control input-md" > 
        </div>
    </div>   

    <div class="col-md-3">
		<label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="slo" name="slo" class="form-control input-md" > 
        </div>
    </div>  

    <div class="col-md-3">
		<label>Coordenada:</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="dlo" name="dlo" class="form-control input-md" > 
        </div>
    </div>  

</div>

<div class="form-group m-form__group row">

	<div class="col-md-6">
		<label><strong>Mes:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visMes" name="visMes" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-6">
		<label><strong>Año:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visAni" name="visAni" class="form-control input-md" > 
        </div>
    </div>   


</div>

<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label><strong>Geometria:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="geom" id="geom" class="form-control input-md">

                    '.$option_geo.'
            </select>
        </div>
    </div>  

    <div class="col-md-6">
        <label><strong>Altura:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visAlt" name="visAlt" class="form-control input-md" > 
        </div>
    </div>

    

</div>
<div class="form-group m-form__group row">
     
	<div class="col-md-6">
		<label><strong>Taxones:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="visTax" name="visTax" class="form-control input-md" > 
        </div>
    </div>  

    <div class="col-md-6">
        <label><strong>Expediente:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="visExp" name="visExp" class="form-control input-md" > 
        </div>
    </div> 


</div>
    ';
		

echo $output;
			

?>	
<script>
    $(document).on('change','#capa_1<?php echo $control; ?>',function(){
        
    cap1 = $('#capa_1<?php echo $control; ?>').val(); 
   // alert(cap1);
    $("#capa_2<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_2<?php echo $control; ?>").css("display","none");
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa2_new(cap1);  
})
</script>
<script>
    $(document).on('change','#capa_2<?php echo $control; ?>',function(){
    
    cap2 = $('#capa_2<?php echo $control; ?>').val(); 
    //alert(cap2);
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa3_new(cap2);  
})
</script>
<script>
    $(document).on('change','#capa_3<?php echo $control; ?>',function(){
    cap3 = $('#capa_3<?php echo $control; ?>').val(); 
    //alert(cap3);  
})
</script>

<script>
    $(document).ready(function(){
            load_capa1_new();
            load_insti_new();
            
        });
     function load_insti_new(page){
        
        $.ajax({
            url:'ajax/cargar_insti.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#insti").append(o);
                    $("#insti").css("display","");
                });
               
            }
        })
    }

    function load_capa1_new(page){
        
        $.ajax({
            url:'ajax/cargar_capa1.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#capa_1<?php echo $control; ?>").append(o);
                    $("#capa_1<?php echo $control; ?>").css("display","");
                });
               
            }
        })
    }

  function cargar_capa2_new(id){

            $.ajax({
                url: "ajax/cargar_capa2.php",
                method:"post",
                dataType: 'json',
                //data: {id : jsonString},
                data:{id:id},
                success: function(response){
                
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_2<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap2" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap2').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_2<?php echo $control; ?>").css("display","");
                    
                }
            });
            }

function cargar_capa3_new(id){
            $.ajax({
                url: "ajax/cargar_capa3.php",
                method:"post",
                dataType: 'json',
                data:{id:id},
                 success: function(response){
                    
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_3<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap3" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap3').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_3<?php echo $control; ?>").css("display","");
                    
                }
            });
            }
</script>