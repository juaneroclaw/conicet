<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php
session_start();
require_once("../conn.php");
require '../vendor/autoload.php';
$inputFileType = 'xlsx';
$inputFileName = '../'.$_POST['url'].'';
$url_ex = $_POST['url'];
$email_u=$_SESSION['email'];
$nvisor = $_POST['idvisor'];

/** Load $inputFileName to a Spreadsheet Object  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($inputFileName);
$reader->setLoadSheetsOnly(["Sheet 1", "Hoja1"]);
//$reader->setReadFilter($filterSubset);
$spreadsheet = $reader->load($inputFileName);

$arreglo[] = $spreadsheet->getActiveSheet()->toArray();
$nfila = count($arreglo[0]);
$nfila_one = $nfila-1;
$ntotal = ($nfila-3);
$ncolumna = count($arreglo[0][2]);
$arreglo_data=array();


for($i=0; $i<=$nfila_one; $i++){

    if($arreglo[0][$i][0]!=''){

        $input = $arreglo[0][$i][2];
        $output = preg_split("/(:|-|;)/",$input);
        $contar = count($output);
        $contar2 = 0;
        $check = 'off';
        
        if($contar>1){
            $duplicado='S';
            $contar2=0;
        }else{
            $duplicado='N';
        }

        foreach ($output as $line) {
            
            $contar2=$contar2+1;

            if($duplicado=='S' && $contar2==1){
                $duplicado='N';
                $check='on';
            }
            
            if($check=='on' && $duplicado=='N' && $contar2>1){
                $duplicado='S';
                $check='off';
            }
             
            if($arreglo[0][$i][12]=='' || $arreglo[0][$i][12]=="'-"){
                $arreglo[0][$i][12]=0;           
            }

            $tit=$arreglo[0][$i][0];
            $desc=$arreglo[0][$i][1];
            $insti=$line;
            $fuen=$arreglo[0][$i][3];
            $url=$arreglo[0][$i][4];
            $contac=$arreglo[0][$i][5];
            $lat=$arreglo[0][$i][6];
            $lon=$arreglo[0][$i][7];
            $mes=$arreglo[0][$i][8];
            $ani=$arreglo[0][$i][9];
            $geo=$arreglo[0][$i][10];
            $alt=$arreglo[0][$i][11];
            $tax=$arreglo[0][$i][12];

            
            $deg_lat='';
            $min_lat='';
            $sec_lat='';
            $cor_lat=' ';  
            if(trim($lat)!==''){

            $dec_lat = str_replace(",", ".", $lat);

            $varstemp_lat = explode(".",$dec_lat);
            if(round($varstemp_lat[1])==0){
                $dec_lat = $dec_lat/10000;
            }else{
                $dec_lat = $dec_lat;
            }
            $vars_lat = explode(".",$dec_lat);
            $deg_lat = $vars_lat[0];
            $tempma_lat = "0.".$vars_lat[1];
            $tempma_lat = $tempma_lat * 3600;
            $min_lat = floor($tempma_lat / 60);
            $sec_lat = $tempma_lat - ($min_lat*60);
            if($vars_lat[0]>=0 && $vars_lat[0]<=90){
                $cor_lat='N';
            }
            if($vars_lat[0]<=0 && $vars_lat[0]>=-90){
                $cor_lat='S';
            }

            }
            

            $deg_lon='';
            $min_lon='';
            $sec_lon='';
            $cor_lon=' ';
            if(trim($lon)!==''){

            $dec_lon = str_replace(",", ".", $lon);
            $varstemp_lon = explode(".",$dec_lon);
            if(round($varstemp_lon[1])==0){
                $dec_lon = $dec_lon/10000;
            }else{
                $dec_lon = $dec_lon;
            }
            $vars_lon = explode(".",$dec_lon);
            $deg_lon = $vars_lon[0];
            $tempma_lon = "0.".$vars_lon[1];
            $tempma_lon = $tempma_lon * 3600;
            $min_lon = floor($tempma_lon / 60);
            $sec_lon = $tempma_lon - ($min_lon*60);
            if($vars_lon[0]>=0 && $vars_lon[0]<=180){
                $cor_lon='E';
            }
            if($vars_lon[0]<=0 && $vars_lon[0]>=-180){
                $cor_lon='O';
            }

            }

            
            if($deg_lat==''){
                $deg_lat=0;
            }

            if($min_lat==''){
                $min_lat=0;
            }

            if($sec_lat==''){
                $sec_lat=0;
            }

            if($deg_lon==''){
                $deg_lon=0;
            }
            
            if($min_lon==''){
                $min_lon=0;
            }
            
            if($sec_lon==''){
                $sec_lon=0;
            }

            if(trim($insti)!=''){
                $insti_nom=trim($insti);
                $sqlInsti="SELECT * FROM tblInstitucion WHERE insNom='$insti_nom'";
                $resultInsti = mysqli_query($con, $sqlInsti);
                $rowInsti = mysqli_num_rows($resultInsti);
                $rowInsti_array = mysqli_fetch_array($resultInsti);

                $sqlInsti_id="SELECT * FROM tblInstitucion";
                $resultInsti_id = mysqli_query($con, $sqlInsti_id);
                $rowInsti_id = mysqli_num_rows($resultInsti_id)+1;

                if($rowInsti==0){

                $sqlInsti_ADD="INSERT INTO tblInstitucion(`idIns`, `insAcro`, `insExAcro`, `insNom`, `insCon`, `insUrl`, `insCiu`, `insProv`, `insPai`) VALUES ('$rowInsti_id','NO DISPONIBLE',null,'$insti_nom',null,null,null,null,'NO DISPONIBLE')";
                $resultInsti_ADD = mysqli_query($con, $sqlInsti_ADD);
                    //var_dump($resultInsti_ADD);
                $insti_id = $rowInsti_id;

                }else{
                    
                $insti_id=$rowInsti_array['idIns'];
                //echo 'id existente:'.$insti_id.'';
                            //echo $insti_id;
                }
            }else{
            	$insti_id=1;
            }

            if(trim($fuen)!=''){

            $fuen_nom=trim($fuen);
            $sqlFuen="SELECT * FROM tblFuente WHERE nomFue='$fuen_nom'";
            $resultFuen = mysqli_query($con, $sqlFuen);
            $rowFuen = mysqli_num_rows($resultFuen);
            $rowFuen_array = mysqli_fetch_array($resultFuen);

            $sqlFuen_id="SELECT * FROM tblFuente";
            $resultFuen_id = mysqli_query($con, $sqlFuen_id);
            $rowFuen_id = mysqli_num_rows($resultFuen_id)+1;

            if($rowFuen==0){
                
            $sqlFuen_ADD="INSERT INTO tblFuente(`idFue`, `nomFue`) VALUES ('$rowFuen_id','$fuen_nom')";
            $resultFuen_ADD = mysqli_query($con, $sqlFuen_ADD);
            //var_dump($resultFuen_ADD);
            $fuen_id = $rowFuen_id;
                
            }else{
                
            $fuen_id=$rowFuen_array['idFue'];
            //echo $fuen_id;
                
            }

            }else{
            	$fuen_id=1;
            }

            if(trim($geo)!=''){

            $geo_nom=trim($geo);
            $sqlGeo="SELECT * FROM tblGeometria WHERE geoNom='$geo_nom'";
            $resultGeo = mysqli_query($con, $sqlGeo);
            $rowGeo = mysqli_num_rows($resultGeo);
            $rowGeo_array = mysqli_fetch_array($resultGeo);

            $sqlGeo_id="SELECT * FROM tblGeometria";
            $resultGeo_id = mysqli_query($con, $sqlGeo_id);
            $rowGeo_id = mysqli_num_rows($resultGeo_id)+1;

            if($rowGeo==0){
                
            $sqlGeo_ADD="INSERT INTO tblGeometria(`idGeo`, `geoNom`) VALUES ('$rowGeo_id','$geo_nom')";
            $resultGeo_ADD = mysqli_query($con, $sqlGeo_ADD);
            //var_dump($resultGeo_ADD);
            $geo_id = $rowGeo_id;

            }else{
                
            $geo_id=$rowGeo_array['idGeo'];
            //echo $geo_id;

            }
            }else{
            	$geo_id=1;
            }

            $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, 
            `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, 
            `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) 
            VALUES(null, '$nvisor', '999', '999', '999', '$tit', '$desc', '$insti_id', '$fuen_id', '$url', '$contac', 
            '$deg_lat', '$min_lat', '$sec_lat', '$cor_lat', '$deg_lon', '$min_lon', '$sec_lon', '$cor_lon', 
            '$mes', '$ani', '$geo_id', '$alt', '$tax', '$url_ex', 'S', '766', '0', '0', '1', 'N', '$duplicado', '$email_u')";
            $query_update = mysqli_query($con, $sql);
            //var_dump($query_update);     
            }
        }
    }


mysqli_close($con);
?>	
<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script>
    function editar_alert(id){

            $.ajax({
                url: "ajax/editar_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }
</script>	
		
	