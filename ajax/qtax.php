<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_4.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_4.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/


    /*COLOR BACKGROUND SET*/
    table#sample_4.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_4.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>
<?php
require_once("../conn.php");

$data_table='';
$data_table.='
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box green">
<div class="portlet-title">
<div class="caption">
<i class="fa fa-globe"></i>INECOA 766 </div>
<div class="tools"> </div>
</div>
<div class="portlet-body table-both-scroll">';
/* BEGIN TABLA */
$data_table.= '
<table class="table table-striped table-bordered table-hover table-fixed text-nowrap" id="sample_4">
<thead>
<tr >';

/*BEGIN ENCABEZADO PERSONALIZADO*/
$data_table.= '
<th class="th-sm">Institucion</th>
<th class="th-sm">Capas</th>
<th class="th-sm">Subcapas</th>
<th class="th-sm">Ecoregion</th>
<th class="th-sm">#Taxones</th>


';
/*END ENCABEZADO PERSONALIZADO*/
$data_table.='
</tr>
</thead>
<tfoot>
<tr>
<th colspan="4" style="text-align:right">Total:&nbsp;&nbsp;</th>
<th></th>
</tr>
</tfoot>
<tbody>';

$sqlSelectVis="SELECT DISTINCT D.nomDisc AS Disc, S.nomSubDisc AS SubDisc, E.nomEco AS Eco, i.insNom AS nomInst, 
SUM(v.visTax) AS QTax  
FROM tblVisor v 
inner join tblInstitucion i on i.idIns=v.idIns 
inner join tblDisciplina D on D.idDisc=v.visIdDisc 
inner join tblSubDisc S on S.idSubDisc=v.visIdSubDisc 
inner join tblEcoregion E on E.IdEco=v.visIdEco 
GROUP BY i.insNom, D.nomDisc, S.nomSubDisc, E.nomEco
";
    $resultVis = mysqli_query($con, $sqlSelectVis);

    while ($rowVis = mysqli_fetch_array($resultVis)) {
  if($rowVis['Disc']!=''){


$data_table.= '
        <tr>
        <td>'.$rowVis['nomInst'].'</td>
        <td>'.$rowVis['Disc'].'</td>
        <td>'.$rowVis['SubDisc'].'</td>
        <td>'.$rowVis['Eco'].'</td>
        ';
        if($rowVis['QTax']==''){
            $rowVis['QTax']=0;
             $data_table.= '
             <td>'.intval($rowVis['QTax']).'</td></tr>
                   ';
        }else{
            $data_table.= '
             <td>'.intval($rowVis['QTax']).'</td></tr>
                   ';
        }

        }
          } 
        
$data_table.='
</tbody>
</table>
</div>
</div>';
/* END TABLA */

echo $data_table;

mysqli_close($con);
?>	
<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<!--<script>
    $(document).ready(function() {
    $('#sample_4_wrapper').DataTable( {
   
    } );
} );
</script>-->
<script>
    $(document).ready(function() {
        
    //$("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
} );
</script>
		