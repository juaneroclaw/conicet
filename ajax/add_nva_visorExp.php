<?php
require_once("../conn.php");

    if (empty($_POST['id'])) {
         $errors[] = "Entrada id vacía";
        } else if (empty($_POST['expTit'])){
         $errors[] = "Entrada Título vacía";
        } else if (empty($_POST['expDes'])){
         $errors[] = "Entrada Descripción vacía";
        } else if (empty($_POST['insti'])){
         $errors[] = "Entrada Institución vacía";
        } else if (empty($_POST['fuen'])){
         $errors[] = "Entrada Fuente vacía";
        } else if (empty($_POST['email_u'])){
         $errors[] = "Entrada email vacía";
        } else if (empty($_POST['expEco'])){
            $errors[] = "Entrada Ecoregión vacía";

        }   else if (

            !empty($_POST['id']) &&
            !empty($_POST['expTit']) &&
            !empty($_POST['expDes']) &&
            !empty($_POST['insti']) &&
            !empty($_POST['fuen']) &&
            !empty($_POST['email_u']) &&
            !empty($_POST['expEco'])
        ){

        // escaping, additionally removing everything that could be (html/javascript-) code

(`idExp`, `expIdIns`, `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`, `expDpto`, `expLoc`, `expApr`, `expNap`, `expEco`, `expCat`, `expLot`, `expEst`, `expSue`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `expAlt`, `expIdGeo`, `expVador`, `expCon`, `expFob`, `expFue`, `expUrl`, `expTax`, `expIdGru`, `expCju`, `expIdFlia`, `expIdGen`, `expIdEsp`, `expIdSubEsp`, `expVul`, `expTipo`, `expSex`, `expIdCla`, `expMes`, `expAno`, `expRas`, `expObs`, `expExc`, `expOri`, `expUlt`, `expExp`, `expExpor`, `expDup`)
VALUES(null, '$id', '$inst', '$capa3', '$visTit', '$visDes', '$inst', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', 'visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$visDup', '$visEmail')";

            $id = mysqli_real_escape_string($con,(strip_tags($_POST["id"],ENT_QUOTES)));
            //$capa1 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_1"],ENT_QUOTES)));
            //$capa2 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_2"],ENT_QUOTES)));
            //$capa3 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_3"],ENT_QUOTES)));
            $visTit = mysqli_real_escape_string($con,(strip_tags($_POST["expTit"],ENT_QUOTES)));
            $visDes = mysqli_real_escape_string($con,(strip_tags($_POST["expDes"],ENT_QUOTES)));
            $inst = mysqli_real_escape_string($con,(strip_tags($_POST["insti"],ENT_QUOTES)));
            $visFue = mysqli_real_escape_string($con,(strip_tags($_POST["fuen"],ENT_QUOTES)));
            /*$visGeo = mysqli_real_escape_string($con,(strip_tags($_POST["geo_"],ENT_QUOTES)));*/

            $capa3 = mysqli_real_escape_string($con,(strip_tags($_POST["expEco"],ENT_QUOTES)));
            $visUrl = mysqli_real_escape_string($con,(strip_tags($_POST["expUrl"],ENT_QUOTES)));
            $visCon = mysqli_real_escape_string($con,(strip_tags($_POST["expCon"],ENT_QUOTES)));
            
            if (empty($_POST['outGra'])){
             $colLatG = 0;
            } else{
            $colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            } 
            if (empty($_POST['outMin'])){
             $colLatM = 0;
            } else{
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            } 
            if (empty($_POST['outSeg'])){
             $colLatS = 0;
            } else{
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            }
            if (empty($_POST['d'])){
             $colLatX = ' ';
            } else{
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            }
            if (empty($_POST['glo'])){
             $colLonG = 0;
            } else{
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            }
            if (empty($_POST['mlo'])){
             $colLonM = 0;
            } else{
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['slo'])){
             $colLonS = 0;
            } else{
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            } 
            if (empty($_POST['dlo'])){
             $colLonX = ' ';
            } else{
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['geom'])){
             $Geo = 1;
            } else{
            $Geo = mysqli_real_escape_string($con,(strip_tags($_POST["geom"],ENT_QUOTES)));
            }
            if (empty($_POST['expAlt'])){
             $Alt = 0;
            } else{
            $Alt = mysqli_real_escape_string($con,(strip_tags($_POST["expAlt"],ENT_QUOTES)));
            }
            if (empty($_POST['expTax'])){
             $Tax = 0;
            } else{
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["expTax"],ENT_QUOTES)));
            }
            if (empty($_POST['expExp'])){
             $visExp = $_POST['expUbi'].'-'.$_POST['expNro'];
             } else{
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["expExp"],ENT_QUOTES)));
            }
            /*$colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));*/
            $visMes = mysqli_real_escape_string($con,(strip_tags($_POST["expMes"],ENT_QUOTES)));
            $visAni= mysqli_real_escape_string($con,(strip_tags($_POST["expAno"],ENT_QUOTES)));
            
            $Alt = mysqli_real_escape_string($con,(strip_tags($_POST["expAlt"],ENT_QUOTES)));
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["expTax"],ENT_QUOTES)));
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["expExp"],ENT_QUOTES)));
            $visEmail = mysqli_real_escape_string($con,(strip_tags($_POST["email_u"],ENT_QUOTES)));

            $visExc = 'nva-planilla'.$id.'';
            $visUlt = 'Y';
            $visAcro = '';
            $visNroCol = 0;
            $visOri = '4';
            $visExpor = 'N';
            $visDup = '';

            if(is_array($_POST["capa_3"]))    // dnde guardo expEco - ecoregión
            {
            /*foreach ($_POST["capa_3"] as $clave=>$valor)
                {
                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];

                $contar = count($_POST["insti"]);
                $contar2 = 0;
                $check = 'off';
                if($contar>1){
                    $duplicado='S';
                    $contar2=0;
                }else{
                    $duplicado='N';
                }  */

                foreach ($_POST["capa_3"] as $clave2=>$valor2)
                    {
                        $contar2 = count($_POST["expEco"]);
                        $contar2=$contar2+1;

                        if($duplicado=='S' && $contar2==1){
                            $duplicado='N';
                            $check='on';
                        }
                        if($check=='on' && $duplicado=='N' && $contar2>1){
                            $duplicado='S';
                            $check='off';
                        }

                    $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) VALUES(null, '$id', '$FindDisc', '$FindSubDisc', '$valor', '$visTit', '$visDes', '$valor2', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', '$visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$duplicado', '$visEmail')";
                    $query_update = mysqli_query($con, $sql);

                    }
                }
            }

            // INSERT INTO tblExpediente
            $sqlExp="INSERT INTO tblExpediente(
            (`idExp`, `expIdIns`, `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`, `expDpto`, `expLoc`, `expApr`, `expNap`, `expEco`, `expCat`, `expLot`, `expEst`, `expSue`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `expAlt`, `expIdGeo`, `expVador`, `expCon`, `expFob`, `expFue`, `expUrl`, `expTax`, `expIdGru`, `expCju`, `expIdFlia`, `expIdGen`, `expIdEsp`, `expIdSubEsp`, `expVul`, `expTipo`, `expSex`, `expIdCla`, `expMes`, `expAno`, `expRas`, `expObs`, `expExc`, `expOri`, `expUlt`, `expExp`, `expExpor`, `expDup`)
            VALUES(null, '$id', '$inst', '$capa3', '$visTit', '$visDes', '$inst', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', 'visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$visDup', '$visEmail')";
            $query_update = mysqli_query($con, $sqlExp);
   
            if ($query_update){
                $messages[] = "Se ha agregado el registro satisfactoriamente.";
            } else{
                $errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
            }
            
        } else {
            $errors []= "Error desconocido.";
        }
        
        if (isset($errors)){
            
            ?>
            <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> 
                    <?php
                        foreach ($errors as $error) {
                                echo $error;
                            }
                        ?>
            </div>

            <?php
            }
            if (isset($messages)){
            ?>
                <div class="alert alert-success" role="alert">
                    <strong>Bien!</strong> 
                    <?php
                        foreach ($messages as $message) {
                                echo $message;
                            }
                    ?>
            </div>

            <?php
            }
?>  