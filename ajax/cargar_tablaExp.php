<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_1.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_1.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/


    /*COLOR BACKGROUND SET*/
    table#sample_1.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_1.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>
<?php
function convertDMSToDecimal($latlng) {
    $valid = false;
    $decimal_degrees = 0;
    $degrees = 0; $minutes = 0; $seconds = 0; $direction = 1;

    // Determine if there are extra periods in the input string
    $num_periods = substr_count($latlng, '.');
    if ($num_periods > 1) {
        $temp = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // replace all but last period with delimiter
        $temp = trim(preg_replace('/[a-zA-Z]/','',$temp)); // when counting chunks we only want numbers
        $chunk_count = count(explode(" ",$temp));
        if ($chunk_count > 2) {
            $latlng = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // remove last period
        } else {
            $latlng = str_replace("."," ",$latlng); // remove all periods, not enough chunks left by keeping last one
        }
    }
    
    // Remove unneeded characters
    $latlng = trim($latlng);
    $latlng = str_replace("º"," ",$latlng);
    $latlng = str_replace("°"," ",$latlng);
    $latlng = str_replace("'"," ",$latlng);
    $latlng = str_replace("\""," ",$latlng);
    $latlng = str_replace("  "," ",$latlng);
    $latlng = substr($latlng,0,1) . str_replace('-', ' ', substr($latlng,1)); // remove all but first dash

    if ($latlng != "") {
        // DMS with the direction at the start of the string
        if (preg_match("/^([nsewoNSEWO]?)\s*(\d{1,3})\s+(\d{1,3})\s*(\d*\.?\d*)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[2]);
            $minutes = intval($matches[3]);
            $seconds = floatval($matches[4]);
            if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                $direction = -1;
        }
        // DMS with the direction at the end of the string
        elseif (preg_match("/^(-?\d{1,3})\s+(\d{1,3})\s*(\d*(?:\.\d*)?)\s*([nsewoNSEWO]?)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[1]);
            $minutes = intval($matches[2]);
            $seconds = floatval($matches[3]);
            if (strtoupper($matches[4]) == "S" || strtoupper($matches[4]) == "W" || $degrees < 0) {
                $direction = -1;
                $degrees = abs($degrees);
            }
        }
        if ($valid) {
            // A match was found, do the calculation
            $decimal_degrees = ($degrees + ($minutes / 60) + ($seconds / 3600)) * $direction;
        } else {
            // Decimal degrees with a direction at the start of the string
            if (preg_match("/^([nsewNSEW]?)\s*(\d+(?:\.\d+)?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                    $direction = -1;
                $decimal_degrees = $matches[2] * $direction;
            }
            // Decimal degrees with a direction at the end of the string
            elseif (preg_match("/^(-?\d+(?:\.\d+)?)\s*([nsewNSEW]?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[2]) == "S" || strtoupper($matches[2]) == "W" || $degrees < 0) {
                    $direction = -1;
                    $degrees = abs($degrees);
                }
                $decimal_degrees = $matches[1] * $direction;
            }
        }
    }
    if ($valid) {
        return $decimal_degrees;
    } else {
        return false;
    }
}
require_once("../conn.php");
//$idVis=$_POST['idExp'];
//$idIns=$_POST['idIns'];
$idVis=$_POST['id'];

 $sqlExc="SELECT expExc FROM tblExpediente WHERE idExp='$idVis'";
 $resultExc = mysqli_query($con, $sqlExc);
 $rowExc = mysqli_fetch_array($resultExc);

    $response='';
    $response.='
    <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i>Planilla Expdientes Importada - '.$rowExc['expExc'].'</div>
                                            $idVis
                                        <div class="tools"> </div>
                                    </div>
                                    
                                    <div class="portlet-body table-both-scroll">
    <table class="table table-striped table-bordered table-hover table-fixed text-nowrap order-column display" id="sample_1">

                                            <thead>
                                                <tr>
                                                    <th>Institución</th>
                                                    <th>Ubicación</th>
                                                    <th>Nro.Exp.</th>
                                                    <th>Iniciado</th>
                                                    <th>Consultor Amb.</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Departamento</th>
                                                    <th>Localidad</th>
                                                    <th>Área protegida</th>
                                                    <th>name_ap</th>
                                                    <th>Ecoregión</th>
                                                    <th>Catastro</th>
                                                    <th>Lote</th>
                                                    <th>Estado</th>
                                                    <th>Suelo</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                    <th>Altura m.s.</th>
                                                    <th>Geometría</th>
                                                    <th>Observador</th>
                                                    <th>Contacto</th>
                                                    <th>Fecha Observación</th>
                                                    <th>Fuente</th>
                                                    <th>URL</th>
                                                    <th>Cantidad Taxones</th>

                                                    <th>Grupo</th>
                                                    <th>Cnj. Esp.</th>
                                                    <th>Familia</th>
                                                    <th>Género</th>
                                                    <th>Especie</th>
                                                    <th>SubEsp.</th>
                                                    <th>Nom. Vulgar</th>
                                                    <th>Tipo</th>
                                                    <th>Sexo</th>
                                                    <th>Clasificación</th>

                                                    <th>Mes</th>
                                                    <th>Año</th>

                                                    <th>Rastros</th>
                                                    <th>Observaciones</th>
                                                    <th>Excel origen</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>';
    //$response = array(); 
/*BEGIN FOOTER
<tfoot>
<tr>
<th colspan="15" style="text-align:right">Total:&nbsp;&nbsp;</th>
<th></th>
</tr>
</tfoot>
END FOOTER*/
    $sqlSelectVis="SELECT * FROM tblExpediente WHERE idExp='$idVis' AND expUlt='S' ORDER BY idExp ASC";
    $counter=0;
    $resultVis = mysqli_query($con, $sqlSelectVis);
    //$count=0;
        while ($rowVis = mysqli_fetch_array($resultVis)) {
    $counter=$counter+1;
            $insti_id=$rowVis['expIdIns'];
            $fuen_id=$rowVis['expFue'];
            $geo_id=$rowVis['expIdGeo'];

            $gru_id=$rowVis['expIdGru'];
            $cju_id=$rowVis['expIdCju'];
            $flia_id=$rowVis['expIdFlia'];
            $gen_id=$rowVis['expIdGen'];
            $esp_id=$rowVis['expIdEsp'];
            $sesp_id=$rowVis['expIdSubEsp'];
            $cla_id=$rowVis['expIdCla'];
            $eco_id=$rowVis['expIdEco'];

            $sqlInsti="SELECT * FROM tblInstitucion WHERE idIns='$insti_id'";
            $resultInsti = mysqli_query($con, $sqlInsti);
            $rowInsti = mysqli_fetch_array($resultInsti);

            $sqlFuen="SELECT * FROM tblFuente WHERE idFue='$fuen_id'";
            $resultFuen = mysqli_query($con, $sqlFuen);
            $rowFuen = mysqli_fetch_array($resultFuen);

            $sqlGeo="SELECT * FROM tblGeometria WHERE idGeo='$geo_id'";
            $resultGeo = mysqli_query($con, $sqlGeo);
            $rowGeo = mysqli_fetch_array($resultGeo);

            $sqleco="SELECT * FROM tblEcoregion WHERE IdEco='$eco_id'";
            $resulteco = mysqli_query($con, $sqleco);
            $roweco = mysqli_fetch_array($resulteco);
                    
            $sqlGru="SELECT * FROM tblGrupo WHERE idGru='$gru_id'";
            $resultGru = mysqli_query($con, $sqlGru);
            $rowGru = mysqli_fetch_array($resultGru);

            $sqlCju="SELECT * FROM tblConjunto WHERE idCju='$cju_id'";
            $resultCju = mysqli_query($con, $sqlCju);
            $rowCju = mysqli_fetch_array($resultCju);

            $sqlFlia="SELECT * FROM tblFamilia WHERE idFlia='$flia_id'";
            $resultFlia = mysqli_query($con, $sqlFlia);
            $rowFlia = mysqli_fetch_array($resultFlia);

            $sqlGen="SELECT * FROM tblGenero WHERE idGen='$gen_id'";
            $resultGen = mysqli_query($con, $sqlGen);
            $rowGen = mysqli_fetch_array($resultGen);

            $sqlEsp="SELECT * FROM tblEspecie WHERE idEsp='$esp_id'";
            $resultEsp = mysqli_query($con, $sqlEsp);
            $rowEsp = mysqli_fetch_array($resultEsp);

            $sqlsEsp="SELECT * FROM tblSubEspecie WHERE idSubEsp='$sesp_id'";
            $resultsEsp = mysqli_query($con, $sqlsEsp);
            $rowsEsp = mysqli_fetch_array($resultsEsp);

            $sqlCla="SELECT * FROM tblClasificacion WHERE idCla='$cla_id'";
            $resultCla = mysqli_query($con, $sqlCla);
            $rowCla = mysqli_fetch_array($resultCla);

        $latitud_data=''.$rowVis['colLatG'].'° '.$rowVis['colLatM'].'\' '.$rowVis['colLatS'].'" '.$rowVis['colLatX'].'';
        $latitud = convertDMSToDecimal($latitud_data);
        $longitud_data=''.$rowVis['colLonG'].'° '.$rowVis['colLonM'].'\' '.$rowVis['colLonS'].'" '.$rowVis['colLonX'].'';
        $longitud = convertDMSToDecimal($longitud_data);

        $control='_'.$rowVis['id'].'_'.round(microtime(true)).'';

        if($rowVis['expDup']=='S'){
            $duplicado="marca2";
        }else{
            $duplicado="";
        }
            $response.='
            <tr class="'.$duplicado.'">';

    /*    if(trim($rowInsti['insNom'])!=''){
            $response.='
            <td>'.$rowVis['idExp'].'</td>
            '; 
        } else{
            $response.='
            <td>'.$rowVis['idExp'].'</td>
            <td list >
                <div class="form-group">            
                    <select name="capa_1[]" id="capa_1'.$control.'" size="4" multiple="multiple" style="display:" onchange="return capa1(this);">
                    '.$capa_source.'
                    </select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_2[]" id="capa_2'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa2(this);"></select>                           
                </div>
            </td>

            <td list >
                <div class="form-group">            
                    <select name="capa_3[]" id="capa_3'.$control.'" size="4" multiple="multiple" style="display: none;" onchange="return capa3(this);"></select>                           
                </div>
                 <div align="center">
                <div id="cargar_boton'.$control.'"></div>
                </div>
            </td>
            ';
        } */

            $response.='
            <td>'.$rowInsti['insNom'].'</td>
            <td>'.$rowVis['expUbi'].'</td>
            <td>'.$rowVis['expNro'].'</td>
            <td>'.$rowVis['expIni'].'</td>
            <td>'.$rowVis['expTor'].'</td>
            <td>'.$rowVis['expTit'].'</td>
            <td>'.$rowVis['expDes'].'</td>
            <td>'.$rowVis['expDpto'].'</td>
            <td>'.$rowVis['expLoc'].'</td>
            <td>'.$rowVis['expApr'].'</td>
            <td>'.$rowVis['expNap'].'</td>
            <td>'.$roweco['nomEco'].'</td>
            <td>'.$rowVis['expCat'].'</td>
            <td>'.$rowVis['expLot'].'</td>
            <td>'.$rowVis['expEst'].'</td>
            <td>'.$rowVis['expSue'].'</td>
            <td>'.$latitud_data.'</td>
            <td>'.$longitud_data.'</td>
            <td>'.$rowVis['expAlt'].'</td>
            <td>'.$rowGeo['geoNom'].'</td>
            <td>'.$rowVis['expVador'].'</td>
            <td>'.$rowVis['expCon'].'</td>
            <td>'.$rowVis['expFob'].'</td>
            <td>'.$rowFuen['nomFue'].'</td>
            <td>'.$rowVis['expUrl'].'</td>
            <td>'.$rowVis['expTax'].'</td>
            
            <td>'.$rowGru['nomGru'].'</td>
            <td>'.$rowCju['nomCju'].'</td>
            <td>'.$rowFlia['nomFlia'].'</td>
            <td>'.$rowGen['nomGen'].'</td>
            <td>'.$rowEsp['nomEsp'].'</td>
            <td>'.$rowsEsp['nomSubEsp'].'</td>
            <td>'.$rowVis['nomVul'].'</td>
            <td>'.$rowVis['nomTipo'].'</td>
            <td>'.$rowVis['nomSex'].'</td>
            <td>'.$rowCla['nomCla'].'</td>
            
            <td>'.$rowVis['expMes'].'</td>
            <td>'.$rowVis['expAno'].'</td>
            <td>'.$rowVis['expRas'].'</td>
            <td>'.$rowVis['expObs'].'</td>
            <td>'.$rowVis['expExc'].'</td>
            <td>
            <div align="center">
            <a class="btn btn-sm blue btn-outline filter-cancel" data-toggle="modal" data-target="#myModal101" onclick="editar_alert_import('.$rowVis['idExp'].','.$idVis.')"><i class="fa fa-edit fa-lg"></i> Editar</a>
            </div>
            <br>
            <div align="center">
            <a class="btn btn-sm red btn-outline filter-cancel" data-toggle="modal" data-target="#myModal100" onclick="eliminar_alert_import('.$rowVis['idExp'].')"><i class="fa fa-trash-o fa-lg"></i> Eliminar</a>
            </div>
            </td>
            </tr>';
            //$response[$count]['value']=$rowVis['idSubDisc'];
            //$response[$count]['name']=$rowViss['nomSubDisc'];
            //$count=$count+1;
        }
        $response.=' </tbody>
                                        </table>
                                        </div>
                                     
                                </div>';
    echo $response;     

?>  

<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttonsExp.js" type="text/javascript"></script>


<script>
    function editar_alert_import(id,reg){

            $.ajax({
                url: "ajax/editar_alert_importExp.php",
                method:"post",
                data:{id:id, reg:reg},
                success:function(data){
                    $('#resultados_ajax101').empty();
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }
    function eliminar_alert(id){

            $.ajax({
                url: "ajax/eliminar_alertExp.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#resultados_ajax100').empty();
                    $('#eliminar_registrov').html(data);
                    $('#myModal100').modal("show");
                }
            });
            }

</script>   
