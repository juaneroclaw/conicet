<?php
session_start();
require_once("../conn.php");
$id_visor=$_POST['id'];
$control=''.$_POST['id'].''.round(microtime(true)).'';
$option_fue='';
$option_geo='';
$option_insti='';

$option_eco='';
$option_dpto=''.$_POST['id'].''.round(microtime(true)).'';
$option_loca=''.$_POST['id'].''.round(microtime(true)).'';
$option_gru='';
$option_cju='';
$option_flia='';
$option_gen='';
$option_esp='';
$option_sesp='';
$option_cla='';

$sqlSelectInsti = "SELECT * FROM tblInstitucion ORDER BY insNom ASC";
$resultInsti = mysqli_query($con, $sqlSelectInsti);
while ($rowInsti = mysqli_fetch_array($resultInsti)) {
$option_insti.='<option id="insti" name="insti" value="'.$rowInsti['idIns'].'">'.$rowInsti['insNom'].'</option>';
}

$sqlSelectFue = "SELECT * FROM tblFuente";
$resultFue = mysqli_query($con, $sqlSelectFue);
while ($rowFue = mysqli_fetch_array($resultFue)) {
$option_fue.='<option id="fuen" name="fuen" value="'.$rowFue['idFue'].'">'.$rowFue['nomFue'].'</option>';
}

$sqlSelectgeo = "SELECT * FROM tblGeometria";
$resultgeo = mysqli_query($con, $sqlSelectgeo);
while ($rowgeo = mysqli_fetch_array($resultgeo)) {
$option_geo.='<option id="geom" name="geom" value="'.$rowgeo['idGeo'].'">'.$rowgeo['geoNom'].'</option>';
}

/*$sqleco="SELECT * FROM tblEcoregion";
$resulteco = mysqli_query($con, $sqleco);
while ($roweco = mysqli_fetch_array($resulteco)) {
    $option_eco.='<option id="eco" name="eco" value="'.$roweco['idEco'].'">'.$roweco['nomEco'].'</option>';
}

$sqlSelectDpto = "SELECT * FROM departamento WHERE idProvincia=9 ORDER BY Nombre";
$resultDpto = mysqli_query($con, $sqlSelectDpto);
while ($rowDpto = mysqli_fetch_array($resultDpto)) {
$option_dpto.='<option id="dpto" name="dpto" value="'.$rowDpto['ID'].'">'.$rowDpto['Nombre'].'</option>';
$control2=$rowDpto['ID'];
}

$sqlSelectLoca = "SELECT * FROM localidad ORDER BY Nombre";
$resultLoca = mysqli_query($con, $sqlSelectLoca);
while ($rowLoca = mysqli_fetch_array($resultLoca)) {
$option_loca.='<option id="loca" name="loca" value="'.$rowLoca['ID'].'">'.$rowLoca['Nombre'].'</option>';
}
*/

$sqlSelectgru = "SELECT * FROM tblGrupo";
$resultgru = mysqli_query($con, $sqlSelectgru);
while ($rowgru = mysqli_fetch_array($resultgru)) {
$option_gru.='<option id="gru" name="gru" value="'.$rowgru['idGru'].'">'.$rowgru['nomGru'].'</option>';
}

$sqlSelectcju = "SELECT * FROM tblConjunto";
$resultcju = mysqli_query($con, $sqlSelectcju);
while ($rowcju = mysqli_fetch_array($resultcju)) {
$option_cju.='<option id="cju" name="cju" value="'.$rowcju['idCju'].'">'.$rowcju['nomCju'].'</option>';
}

$sqlSelectFlia = "SELECT * FROM tblFamilia";
$resultFlia = mysqli_query($con, $sqlSelectFlia);
while ($rowFlia = mysqli_fetch_array($resultFlia)) {
$option_flia.='<option id="flia" name="flia" value="'.$rowFlia['idFlia'].'">'.$rowFlia['nomFlia'].'</option>';
}

$sqlSelectGen = "SELECT * FROM tblGenero";
$resultGen = mysqli_query($con, $sqlSelectGen);
while ($rowGen = mysqli_fetch_array($resultGen)) {
$option_gen.='<option id="gen" name="gen" value="'.$rowGen['IdGen'].'">'.$rowGen['nomGen'].'</option>';
}

$sqlSelectEsp = "SELECT * FROM tblEspecie";
$resultEsp = mysqli_query($con, $sqlSelectEsp);
while ($rowEsp = mysqli_fetch_array($resultEsp)) {
$option_esp.='<option id="esp" name="esp" value="'.$rowEsp['idEsp'].'">'.$rowEsp['nomEsp'].'</option>';
}

$sqlSelectsEsp = "SELECT * FROM tblSubEspecie";
$resultsEsp = mysqli_query($con, $sqlSelectsEsp);
while ($rowsEsp = mysqli_fetch_array($resultsEsp)) {
$option_sesp.='<option id="sesp" name="sesp" value="'.$rowsEsp['idSubEsp'].'">'.$rowsEsp['nomSubEsp'].'</option>';
}

$sqlSelectsCla = "SELECT * FROM tblClasificacion";
$resultsCla = mysqli_query($con, $sqlSelectsCla);
while ($rowsCla = mysqli_fetch_array($resultsCla)) {
$option_cla.='<option id="cla" name="cla" value="'.$rowsCla['idCla'].'">'.$rowsCla['claNom'].'</option>';
}

$output="";

$output .='
	
<input type="hidden" class="form-control" id="id" name="id" value='.$_POST['id'].'>
<input type="hidden" class="form-control" id="email_u" name="email_u" value='.$_SESSION['email'].'>
<input type="hidden" class="form-control" id="visExc" name="expExc" value="0">
<input type="hidden" class="form-control" id="visUlt" name="expUlt" value="Y">
<input type="hidden" class="form-control" id="visAcro" name="expAcro" value="">
<input type="hidden" class="form-control" id="visOri" name="expOri" value="4">
<input type="hidden" class="form-control" id="visExpor" name="expExpor" value="N">
<input type="hidden" class="form-control" id="visDup" name="expDup" value="">
<input type="hidden" class="form-control" id="page" name="page" value="new">

<input type="hidden" class="form-control" id="validator" name="validator" value="ok">

<div class="form-group m-form__group row">
    <div class="col-md-12"> 
        <label><strong>Capa(s):</strong></label>    
            <div class="input-icon input-icon-md right">      
                <select name="capa_1[]" id="capa_1'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
            </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">  
        <label><strong>Subcapa(s):</strong></label>    
            <div class="input-icon input-icon-md right">     
                <select name="capa_2[]" id="capa_2'.$control.'" size="4" multiple="multiple" style="display: none;"></select>
                </div>
    </div>
</div>
                                               
<div class="form-group m-form__group row">
    <div class="col-md-12">
        <label><strong>Ecoregion(es):</strong></label>
        <div class="input-icon input-icon-md right"> 
            <select name="capa_3[]" id="capa_3'.$control.'" size="4" multiple="multiple" style="display: none;" ></select>
            </div>
    </div>
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12"> 
        <label><strong>Institución:</strong></label>    
            <div class="input-icon input-icon-md right">      
                <select name="insti[]" id="insti" size="4" multiple="multiple" style="width: 100%; display: none;"></select>
            </div>
    </div>
</div>

<div class="form-group m-form__group row">
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Ubicación:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expUbi" name="expUbi" class="form-control input-md"> 
        </div>
    </div>                    
</div>
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Nro.Exp.:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expNro" name="expNro" class="form-control input-md"> 
        </div>
    </div>                    
</div>
</div>

<div class="form-group m-form__group row">
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Fec.Iniciado:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expIni" name="expIni" class="form-control input-md"> 
        </div>
    </div>                    
</div>
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Cons.ambiental:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expTor" name="expTor" class="form-control input-md"> 
        </div>
    </div>                    
</div>
</div>

<div class="form-group m-form__group row">
<div class="col-md-12"> 
    <label><strong>Dpto:</strong></label>    

    <div class="input-icon input-icon-md right">      
        <select name="capa_Dpto[]" id="capa_Dpto'.$option_dpto.'" size="4" style="display: none;"></select>
    </div>
</div>
</div>
<div class="form-group m-form__group row">
<div class="col-md-12"> 
    <label><strong>Localidad:</strong></label>    
    <div class="input-icon input-icon-md right">      
        <select name="capa_Loca[]" id="capa_Loca'.$option_loca.'" size="4" style="display: none;"></select>
    </div>
</div>
</div>


<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Título:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expTit" name="expTit" class="form-control input-md"> 
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Descripción:</strong></label>
        <div class="input-icon input-icon-md right">
        	<textarea id="expDes" name="expDes" class="form-control input-md" rows="2"></textarea>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Área Protegida:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expApr" name="expApr" class="form-control input-md"> 
        </div>
    </div>                    
</div>
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>name_ap:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expNap" name="expNap" class="form-control input-md"> 
        </div>
    </div>                    
</div>
</div>

<div class="form-group m-form__group row">
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Catastro:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expCat" name="expCat" class="form-control input-md"> 
        </div>
    </div>                    
</div>
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Lote:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expLot" name="expLot" class="form-control input-md"> 
        </div>
    </div>                    
</div>
</div>

<div class="form-group m-form__group row">
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Estado:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expEst" name="expEst" class="form-control input-md"> 
        </div>
    </div>                    
</div>
<div class="col-md-6">
	<div class="col-md-12">
		<label><strong>Uso del Suelo:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expSue" name="expSue" class="form-control input-md"> 
        </div>
    </div>                    
</div>
</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Latitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">

	<div class="col-md-3">
		<label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outGra" name="outGra" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-3">
		<label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outMin" name="outMin" class="form-control input-md" > 
        </div>
    </div>   

    <div class="col-md-3">
		<label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="outSeg" name="outSeg" class="form-control input-md" > 
        </div>
    </div>  

    <div class="col-md-3">
		<label>Coordenada:</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="d" name="d" class="form-control input-md" > 
        </div>
    </div>  

</div>

<div class="form-group m-form__group row">
	<div class="col-md-12">
		<label><strong>Longitud:</strong></label>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-3">
		<label>Grado(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="glo" name="glo" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-3">
		<label>Minuto(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="mlo" name="mlo" class="form-control input-md" > 
        </div>
    </div>   

    <div class="col-md-3">
		<label>Segundo(s):</label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="slo" name="slo" class="form-control input-md" > 
        </div>
    </div>  
    <div class="col-md-3">
        <label>Coordenada:</label>    
        <div class="input-icon input-icon-md right">
            <input type="text" id="dlo" name="dlo" class="form-control input-md" > 
        </div>
    </div>
</div>

<div class="form-group m-form__group row">
    <div class="col-md-3">
        <label><strong>Altura:</strong></label>
        <div class="input-icon input-icon-md right">
            <input type="text" id="expAlt" name="expAlt" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-6">
        <label><strong>Geometría:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="geom" id="geom" class="form-control input-md">

                    '.$option_geo.'
            </select>
        </div>
    </div>  
</div>

<div class="form-group m-form__group row">
            <div class="col-md-6">
                <label><strong>Observador:</strong></label>
                <div class="input-icon input-icon-md right">
                    <input type="text" id="expVador" name="expVador" class="form-control input-md" > 
                </div>
            </div>                        

            <div class="col-md-6">
                <label><strong>Contacto:</strong></label>
                <div class="input-icon input-icon-md right">
                    <input type="text" id="expCon" name="expCon" class="form-control input-md" > 
                </div>
            </div>                    
</div>

<div class="form-group m-form__group row">
            <div class="col-md-3">
                <label><strong>Fec.Observac.:</strong></label>
                <div class="input-icon input-icon-md right">
                    <input type="text" id="expFob" name="expFob" class="form-control input-md" > 
                </div>
            </div>                    

    <div class="col-md-6">
        <label><strong>Fuente:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="fuen" id="fuen" class="form-control input-md">

                    '.$option_fue.'
            </select>
        </div>
    </div>                    
</div>  

<div class="form-group m-form__group row">

        <div class="col-md-6">
                <label><strong>URL:</strong></label>
                <div class="input-icon input-icon-md right">
                    <input type="text" id="expUrl" name="expUrl" class="form-control input-md" > 
                </div>
        </div>

      	<div class="col-md-6">
    		<label><strong>Taxones:</strong></label>
            <div class="input-icon input-icon-md right">
            	<input type="text" id="expTax" name="expTax" class="form-control input-md" > 
            </div>
        </div>  
</div>

<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label><strong>Grupo:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="gru" id="gru" class="form-control input-md">

                    '.$option_gru.'
            </select>
        </div>
    </div>                    
    <div class="col-md-6">
        <label><strong>Cju.Especies:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="cju" id="cju" class="form-control input-md">

                    '.$option_cju.'
            </select>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label><strong>Familia:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="flia" id="flia" class="form-control input-md">

                    '.$option_flia.'
            </select>
        </div>
    </div>                    
    <div class="col-md-6">
        <label><strong>Género:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="gen" id="gen" class="form-control input-md">

                    '.$option_gen.'
            </select>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label><strong>Especie:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="esp" id="esp" class="form-control input-md">

                    '.$option_esp.'
            </select>
        </div>
    </div>                    
    <div class="col-md-6">
        <label><strong>SubEspecie:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="sesp" id="sesp" class="form-control input-md">

                    '.$option_sesp.'
            </select>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-6">
		<label><strong>Nom.Vulgar:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expVul" name="expVul" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-6">
		<label><strong>Tipo:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expTip" name="expTip" class="form-control input-md" > 
        </div>
    </div>   
</div>

<div class="form-group m-form__group row">
	<div class="col-md-6">
		<label><strong>Sexo:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expSex" name="expSex" class="forcontrol input-md" > 
        </div>
    </div>

    <div class="col-md-6">
        <label><strong>Clasificación:</strong></label>
        <div class="input-icon input-icon-md right">
            <select name="cla" id="cla" class="form-control input-md">

                    '.$option_cla.'
            </select>
        </div>
    </div>                    
</div>

<div class="form-group m-form__group row">
	<div class="col-md-3">
		<label><strong>Mes:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expMes" name="expMes" class="form-control input-md" > 
        </div>
    </div>

    <div class="col-md-3">
		<label><strong>Año:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expAno" name="expAno" class="form-control input-md" > 
        </div>
    </div>   

    <div class="col-md-6">
		<label><strong>Rastros:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expRas" name="expRas" class="form-control input-md" > 
        </div>
    </div>
</div>

<div class="form-group m-form__group row">
    <div class="col-md-12">
		<label><strong>Observaciones:</strong></label>
        <div class="input-icon input-icon-md right">
        	<input type="text" id="expObs" name="expObs" class="form-control input-md" > 
        </div>
    </div>   
</div>
    ';
		
echo $output;

?>	
<script>
    $(document).on('change','#capa_1<?php echo $control; ?>',function(){
        
    cap1 = $('#capa_1<?php echo $control; ?>').val(); 
   // alert(cap1);
    $("#capa_2<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_2<?php echo $control; ?>").css("display","none");
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa2_new(cap1);  
})
</script>
<script>
    $(document).on('change','#capa_2<?php echo $control; ?>',function(){
    
    cap2 = $('#capa_2<?php echo $control; ?>').val(); 
    //alert(cap2);
    $("#capa_3<?php echo $control; ?>").empty();
    $("#capa_3<?php echo $control; ?>").css("display","none");
    cargar_capa3_new(cap2);  
})
</script>
<script>
    $(document).on('change','#capa_3<?php echo $control; ?>',function(){
    cap3 = $('#capa_3<?php echo $control; ?>').val(); 
    //alert(cap3);  
})
</script>

<script>
    $(document).ready(function(){
            load_capa1_new();
            load_insti_new();
            
        });
     function load_insti_new(page){
        
        $.ajax({
            url:'ajax/cargar_insti.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#insti").append(o);
                    $("#insti").css("display","");
                });
               
            }
        })
    }

    function load_capa1_new(page){
        
        $.ajax({
            url:'ajax/cargar_capa1.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#capa_1<?php echo $control; ?>").append(o);
                    $("#capa_1<?php echo $control; ?>").css("display","");
                });
               
            }
        })
    }

  function cargar_capa2_new(id){

            $.ajax({
                url: "ajax/cargar_capa2.php",
                method:"post",
                dataType: 'json',
                //data: {id : jsonString},
                data:{id:id},
                success: function(response){
                
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_2<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap2" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap2').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_2<?php echo $control; ?>").css("display","");
                    
                }
            });
            }

function cargar_capa3_new(id){
            $.ajax({
                url: "ajax/cargar_capa3.php",
                method:"post",
                dataType: 'json',
                data:{id:id},
                 success: function(response){
                    
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_3<?php echo $control; ?>").append('<optgroup id="'+index_+'_cap3" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_cap3').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                        
                    });
                 $("#capa_3<?php echo $control; ?>").css("display","");
                    
                }
            });
            }
</script>

<!-- scripts para Departamento y Localidad -->
<script>
    $(document).on('change','#capa_Dpto<?php echo $option_dpto; ?>',function(){
    
    capDpto = $('#capa_Dpto<?php echo $option_dpto; ?>').val(); 
    //alert(cap2);
    $("#capa_Loca<?php echo $option_dpto; ?>").empty();
    $("#capa_Loca<?php echo $option_dpto; ?>").css("display","none");
    cargar_loca_new(capDpto);  
})
</script>
<script>
    $(document).on('change','#capa_Loca<?php echo $option_loca; ?>',function(){
    capLoca = $('#capa_Loca<?php echo $option_loca; ?>').val(); 
    //alert(cap3);  
})
</script>

<script>
    $(document).ready(function(){
            load_dpto_new();
            cargar_loca_new();
        });

        function load_dpto_new(page){
        $.ajax({
            url:'ajax/cargar_dpto.php',
            dataType: 'json',
            success:function(response){
                $.each(response, function( index, value ) {
                    var o = new Option(value.name, value.value);
                    $(o).html(value.name);
                   
                    $("#capa_Dpto<?php echo $option_dpto; ?>").append(o);
                    $("#capa_Dpto<?php echo $option_dpto; ?>").css("display","");
                });
               
            }
        })
    }

    function cargar_loca_new(id){
            $.ajax({
                url: "ajax/cargar_loca.php",
                method:"post",
                dataType: 'json',
                data:{id:id},
                 success: function(response){
                    
                    console.log(response);
                    $.each(response, function( index, value ) {
                        index_=index.replace(/ /g,"_");
                        $("#capa_Loca<?php echo $option_loca; ?>").append('<optgroup id="'+index_+'_capLoca" label="'+index+'"></optgroup>');
                        $.each(response[index], function( index2, value2 ) {
                         
                            $('#'+index_+'_capLoca').append('<option value="'+value2.value+'">'+value2.name+'</option>');
                        });
                        
                    });
                 $("#capa_Loca<?php echo $option_loca; ?>").css("display","");
                    
                }
            });
            }

</script>