<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php
session_start();
require_once("../conn.php");
require '../vendor/autoload.php';
$inputFileType = 'xlsx';
$inputFileName = '../'.$_POST['url'].'';
$url_ex = $_POST['url'];
$email_u=$_SESSION['email'];
$nvisor = $_POST['idvisor'];
$insti_id = $_POST['idIns'];

// var para idExp
//$evisor = $_POST['idsExp'];

// $idIns=$_POST['idIns'];

/** Load $inputFileName to a Spreadsheet Object  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($inputFileName);
$reader->setLoadSheetsOnly(["Sheet 1", "Hoja1"]);
//$reader->setReadFilter($filterSubset);
$spreadsheet = $reader->load($inputFileName);

$arreglo[] = $spreadsheet->getActiveSheet()->toArray();
$nfila = count($arreglo[0]);
$nfila_one = $nfila-1;
$ntotal = ($nfila-3);
$ncolumna = count($arreglo[0][2]);
$arreglo_data=array();


for($i=0; $i<=$nfila_one; $i++){

    if($arreglo[0][$i][0]!=''){

        $input = $arreglo[0][$i][2];
        $output = preg_split("/(:|-|;)/",$input);
        $contar = count($output);
        $contar2 = 0;
        $check = 'off';
        
        if($contar>1){
            $duplicado='S';
            $contar2=0;
        }else{
            $duplicado='N';
        }

        foreach ($output as $line) {
            
            $contar2=$contar2+1;

            if($duplicado=='S' && $contar2==1){
                $duplicado='N';
                $check='on';
            }
            
            if($check=='on' && $duplicado=='N' && $contar2>1){
                $duplicado='S';
                $check='off';
            }
             
            if($arreglo[0][$i][38]=='' || $arreglo[0][$i][38]=="'-"){
                $arreglo[0][$i][38]=0;           
            }

            $insti=$line;
            $ubi=$arreglo[0][$i][0];
            $caj=$arreglo[0][$i][1];
            $ini=$arreglo[0][$i][2];
            $tor=$arreglo[0][$i][3];
            $tit=$arreglo[0][$i][4];
            $desc=$arreglo[0][$i][5];
            $dpto=$arreglo[0][$i][6];
            $loc=$arreglo[0][$i][7];
            $apr=$arreglo[0][$i][8];
            $nap=$arreglo[0][$i][9];
            $eco=$arreglo[0][$i][10];
            $cat=$arreglo[0][$i][11];
            $lot=$arreglo[0][$i][12];
            $est=$arreglo[0][$i][13];
            $sue=$arreglo[0][$i][14];

            $lat=$arreglo[0][$i][15];
            $lon=$arreglo[0][$i][16];

            $deg_lat='';
            $min_lat='';
            $sec_lat='';
            $cor_lat=' ';  
            if(trim($lat)!==''){

            $dec_lat = str_replace(",", ".", $lat);

            $varstemp_lat = explode(".",$dec_lat);
            if(round($varstemp_lat[1])==0){
                $dec_lat = $dec_lat/10000;
            }else{
                $dec_lat = $dec_lat;
            }
            $vars_lat = explode(".",$dec_lat);
            $deg_lat = $vars_lat[0];
            $tempma_lat = "0.".$vars_lat[1];
            $tempma_lat = $tempma_lat * 3600;
            $min_lat = floor($tempma_lat / 60);
            $sec_lat = $tempma_lat - ($min_lat*60);
            if($vars_lat[0]>=0 && $vars_lat[0]<=90){
                $cor_lat='N';
            }
            if($vars_lat[0]<=0 && $vars_lat[0]>=-90){
                $cor_lat='S';
            }

            }
            

            $deg_lon='';
            $min_lon='';
            $sec_lon='';
            $cor_lon=' ';
            if(trim($lon)!==''){

            $dec_lon = str_replace(",", ".", $lon);
            $varstemp_lon = explode(".",$dec_lon);
            if(round($varstemp_lon[1])==0){
                $dec_lon = $dec_lon/10000;
            }else{
                $dec_lon = $dec_lon;
            }
            $vars_lon = explode(".",$dec_lon);
            $deg_lon = $vars_lon[0];
            $tempma_lon = "0.".$vars_lon[1];
            $tempma_lon = $tempma_lon * 3600;
            $min_lon = floor($tempma_lon / 60);
            $sec_lon = $tempma_lon - ($min_lon*60);
            if($vars_lon[0]>=0 && $vars_lon[0]<=180){
                $cor_lon='E';
            }
            if($vars_lon[0]<=0 && $vars_lon[0]>=-180){
                $cor_lon='O';
            }

            }

            
            if($deg_lat==''){
                $deg_lat=0;
            }

            if($min_lat==''){
                $min_lat=0;
            }

            if($sec_lat==''){
                $sec_lat=0;
            }

            if($deg_lon==''){
                $deg_lon=0;
            }
            
            if($min_lon==''){
                $min_lon=0;
            }
            
            if($sec_lon==''){
                $sec_lon=0;
            }

            $alt=$arreglo[0][$i][17];

            $geo=$arreglo[0][$i][18];

            $vador=$arreglo[0][$i][19];
            $contac=$arreglo[0][$i][20];
            $fob=$arreglo[0][$i][21];
            $fuen=$arreglo[0][$i][22];
            $url=$arreglo[0][$i][23];
            $mes=$arreglo[0][$i][35];
            $ani=$arreglo[0][$i][36];
            $tax=$arreglo[0][$i][24];

            $gru=$arreglo[0][$i][25];
            $cju=$arreglo[0][$i][26];
            $flia=$arreglo[0][$i][27];
            $gen=$arreglo[0][$i][28];
            $esp=$arreglo[0][$i][29];
            $sesp=$arreglo[0][$i][30];
   
            $vul=$arreglo[0][$i][31];
            $tipo=$arreglo[0][$i][32];
            $sex=$arreglo[0][$i][33];

            $cla=$arreglo[0][$i][34];

            $mes=$arreglo[0][$i][35];
            $ano=$arreglo[0][$i][36];
            $ras=$arreglo[0][$i][37];
            $obs=$arreglo[0][$i][38];

  /*          if(trim($insti)!=''){
                $insti_nom=trim($insti);
                //$sqlInsti="SELECT * FROM tblInstitucion WHERE insNom='$insti_nom'";
                // BUSCO POR idIns
                $sqlInsti="SELECT * FROM tblInstitucion WHERE idIns='$idIns'";
                $resultInsti = mysqli_query($con, $sqlInsti);
                $rowInsti = mysqli_num_rows($resultInsti);
                $rowInsti_array = mysqli_fetch_array($resultInsti);

                $sqlInsti_id="SELECT * FROM tblInstitucion";
                $resultInsti_id = mysqli_query($con, $sqlInsti_id);
                $rowInsti_id = mysqli_num_rows($resultInsti_id)+1;

                if($rowInsti==0){

                $sqlInsti_ADD="INSERT INTO tblInstitucion(`idIns`, `insAcro`, `insExAcro`, `insNom`, `insCon`, `insUrl`, `insCiu`, `insProv`, `insPai`) VALUES ('$rowInsti_id','NO DISPONIBLE',null,'$insti_nom',null,null,null,null,'NO DISPONIBLE')";
                $resultInsti_ADD = mysqli_query($con, $sqlInsti_ADD);
                    //var_dump($resultInsti_ADD);
                $insti_id = $rowInsti_id;

                }else{
                    
                $insti_id=$rowInsti_array['idIns'];
                //echo 'id existente:'.$insti_id.'';
                            //echo $insti_id;
                }
            }else{
            	$insti_id=1;
            }
*/
            if(trim($dpto)!=''){

                $dpto_nom=trim($dpto);
                $sqlDpto="SELECT * FROM departamento WHERE Nombre='$dpto_nom'";
                $resultDpto = mysqli_query($con, $sqlDpto);
                $rowDpto = mysqli_num_rows($resultDpto);
                $rowDpto_array = mysqli_fetch_array($resultDpto);
    
                $sqlDpto_id="SELECT * FROM departamento";
                $resultDpto_id = mysqli_query($con, $sqlDpto_id);
                $rowDpto_id = mysqli_num_rows($resultDpto_id)+1;
    
                if($rowDpto==0){
                    
                $sqlDpto_ADD="INSERT INTO departamento (`ID`, 'idProvincia', `Nombre`) VALUES ('$rowDpto_id', 9, '$dpto_nom'.' ADD dsd IMPORT-EXP')";
                $resultDpto_ADD = mysqli_query($con, $sqlDpto_ADD);
                //var_dump($resultFuen_ADD);
                $dpto_id = $rowDpto_id;
                    
                }else{
                    
                $dpto_id=$rowDpto_array['ID'];
                //echo $fuen_id;
                    
                }
    
                }else{
                    $dpto_id=1;
                }
    
                if(trim($loc)!=''){

                    $loc_nom=trim($loc);
                    $sqlloc="SELECT * FROM localidad WHERE Nombre='$loc_nom'";
                    $resultloc = mysqli_query($con, $sqlloc);
                    $rowloc = mysqli_num_rows($resultloc);
                    $rowloc_array = mysqli_fetch_array($resultloc);
        
                    $sqlloc_id="SELECT * FROM localidad";
                    $resultloc_id = mysqli_query($con, $sqlloc_id);
                    $rowloc_id = mysqli_num_rows($resultloc_id)+1;
        
                    if($rowloc==0){
                        
                    $sqlloc_ADD="INSERT INTO localidad (`ID`, 'idDepartamento', `Nombre`) VALUES ('$rowloc_id', '$dpto_id', '$loc_nom'.' ADD dsd IMPORT-EXP')";
                    $resultloc_ADD = mysqli_query($con, $sqlloc_ADD);
                    //var_dump($resultFuen_ADD);
                    $loc_id = $rowloc_id;
                        
                    }else{
                        
                    $loc_id=$rowloc_array['ID'];
                    //echo $fuen_id;
                        
                    }
        
                    }else{
                        $loc_id=1;
                    }

                    
            if(trim($eco)!=''){

                $eco_nom=trim($eco);
                $sqlEco="SELECT * FROM tblEcoregion WHERE nomEco='$eco_nom'";
                $resultEco = mysqli_query($con, $sqlEco);
                $rowEco = mysqli_num_rows($resultEco);
                $rowEco_array = mysqli_fetch_array($resultEco);
    
                $sqlEco_id="SELECT * FROM tblEcoregion";
                $resultEco_id = mysqli_query($con, $sqlEco_id);
                $rowEco_id = mysqli_num_rows($resultEco_id)+1;
    
                if($rowEco==0){
                    
                $sqlEco_ADD="INSERT INTO tblEcoregion (`idEco`, `nomEco`) VALUES ('$rowEco_id','$eco_nom')";
                $resultEco_ADD = mysqli_query($con, $sqlEco_ADD);
                //var_dump($resultFuen_ADD);
                $eco_id = $rowEco_id;
                    
                }else{
                    
                $eco_id=$rowEco_array['idEco'];
                //echo $fuen_id;
                    
                }
    
                }else{
                    $eco_id=1;
                }
    

            if(trim($fuen)!=''){

            $fuen_nom=trim($fuen);
            $sqlFuen="SELECT * FROM tblFuente WHERE nomFue='$fuen_nom'";
            $resultFuen = mysqli_query($con, $sqlFuen);
            $rowFuen = mysqli_num_rows($resultFuen);
            $rowFuen_array = mysqli_fetch_array($resultFuen);

            $sqlFuen_id="SELECT * FROM tblFuente";
            $resultFuen_id = mysqli_query($con, $sqlFuen_id);
            $rowFuen_id = mysqli_num_rows($resultFuen_id)+1;

            if($rowFuen==0){
                
            $sqlFuen_ADD="INSERT INTO tblFuente(`idFue`, `nomFue`) VALUES ('$rowFuen_id','$fuen_nom')";
            $resultFuen_ADD = mysqli_query($con, $sqlFuen_ADD);
            //var_dump($resultFuen_ADD);
            $fuen_id = $rowFuen_id;
                
            }else{
                
            $fuen_id=$rowFuen_array['idFue'];
            //echo $fuen_id;
                
            }

            }else{
            	$fuen_id=1;
            }

            if(trim($geo)!=''){

            $geo_nom=trim($geo);
            $sqlGeo="SELECT * FROM tblGeometria WHERE geoNom='$geo_nom'";
            $resultGeo = mysqli_query($con, $sqlGeo);
            $rowGeo = mysqli_num_rows($resultGeo);
            $rowGeo_array = mysqli_fetch_array($resultGeo);

            $sqlGeo_id="SELECT * FROM tblGeometria";
            $resultGeo_id = mysqli_query($con, $sqlGeo_id);
            $rowGeo_id = mysqli_num_rows($resultGeo_id)+1;

            if($rowGeo==0){
                
            $sqlGeo_ADD="INSERT INTO tblGeometria(`idGeo`, `geoNom`) VALUES ('$rowGeo_id','$geo_nom')";
            $resultGeo_ADD = mysqli_query($con, $sqlGeo_ADD);
            //var_dump($resultGeo_ADD);
            $geo_id = $rowGeo_id;

            }else{
                
            $geo_id=$rowGeo_array['idGeo'];
            //echo $geo_id;

            }
            }else{
            	$geo_id=1;
            }


            if(trim($gru)!=''){

                $gru_nom=trim($gru);
                $sqlgru="SELECT * FROM tblGrupo WHERE nomgru='$gru_nom'";
                $resultgru = mysqli_query($con, $sqlgru);
                $rowgru = mysqli_num_rows($resultgru);
                $rowgru_array = mysqli_fetch_array($resultgru);
    
                $sqlgru_id="SELECT * FROM tblGrupo";
                $resultgru_id = mysqli_query($con, $sqlgru_id);
                $rowgru_id = mysqli_num_rows($resultgru_id)+1;
    
                if($rowgru==0){
                    
                $sqlgru_ADD="INSERT INTO tblGrupo (`idGru`, `nomGru`) VALUES ('$rowgru_id','$gru_nom')";
                $resultgru_ADD = mysqli_query($con, $sqlgru_ADD);
                //var_dump($resultFuen_ADD);
                $gru_id = $rowgru_id;
                    
                }else{
                    
                $gru_id=$rowgru_array['idGru'];
                //echo $fuen_id;
                    
                }
    
                }else{
                    $gru_id=1;
                }
    

                if(trim($cju)!=''){

                    $cju_nom=trim($cju);
                    $sqlcju="SELECT * FROM tblConjunto WHERE nomCju='$cju_nom'";
                    $resultcju = mysqli_query($con, $sqlcju);
                    $rowcju = mysqli_num_rows($resultcju);
                    $rowcju_array = mysqli_fetch_array($resultcju);
        
                    $sqlcju_id="SELECT * FROM tblConjunto";
                    $resultcju_id = mysqli_query($con, $sqlcju_id);
                    $rowcju_id = mysqli_num_rows($resultcju_id)+1;
        
                    if($rowcju==0){
                        
                    $sqlcju_ADD="INSERT INTO tblConjunto (`idCju`, `nomCju`) VALUES ('$rowcju_id','$cju_nom')";
                    $resultcju_ADD = mysqli_query($con, $sqlcju_ADD);
                    //var_dump($resultFuen_ADD);
                    $cju_id = $rowcju_id;
                        
                    }else{
                        
                    $cju_id=$rowcju_array['idCju'];
                    //echo $fuen_id;
                        
                    }
        
                    }else{
                        $cju_id=1;
                    }
        
                    if(trim($flia)!=''){

                        $flia_nom=trim($flia);
                        $sqlflia="SELECT * FROM tblFamilia WHERE nomFlia='$flia_nom'";
                        $resultflia = mysqli_query($con, $sqlflia);
                        $rowflia = mysqli_num_rows($resultflia);
                        $rowflia_array = mysqli_fetch_array($resultflia);
            
                        $sqlflia_id="SELECT * FROM tblFamilia";
                        $resultflia_id = mysqli_query($con, $sqlflia_id);
                        $rowflia_id = mysqli_num_rows($resultflia_id)+1;
            
                        if($rowflia==0){
                            
                        $sqlflia_ADD="INSERT INTO tblFamilia (`idCla`, `idSubCla`, `idOrd`, `idSubOrd`,`idFlia`, `nomFlia`) VALUES ('1','1','1','1','$rowflia_id','$flia_nom')";
                        $resultflia_ADD = mysqli_query($con, $sqlflia_ADD);
                        //var_dump($resultFuen_ADD);
                        $flia_id = $rowflia_id;
                            
                        }else{
                            
                        $flia_id=$rowflia_array['idFlia'];
                        //echo $fuen_id;
                            
                        }
            
                        }else{
                            $flia_id=1;
                        }
            
                        if(trim($gen)!=''){

                            $gen_nom=trim($gen);
                            $sqlgen="SELECT * FROM tblGenero WHERE nomGen='$gen_nom'";
                            $resultgen = mysqli_query($con, $sqlgen);
                            $rowgen = mysqli_num_rows($resultgen);
                            $rowgen_array = mysqli_fetch_array($resultgen);
                
                            $sqlgen_id="SELECT * FROM tblGenero";
                            $resultgen_id = mysqli_query($con, $sqlgen_id);
                            $rowgen_id = mysqli_num_rows($resultgen_id)+1;
                
                            if($rowgen==0){
                                
                            $sqlgen_ADD="INSERT INTO tblGenero (`idCla`, `idSubCla`, `idOrd`, `idSubOrd`, `idFlia`, `IdGen`, `nomGen`) VALUES ('1','1','1','1','$rowflia_id','$rowgen_id','$gen_nom')";
                            $resultgen_ADD = mysqli_query($con, $sqlgen_ADD);
                            //var_dump($resultFuen_ADD);
                            $gen_id = $rowgen_id;
                                
                            }else{
                                
                            $gen_id=$rowgen_array['idGen'];
                            //echo $fuen_id;
                                
                            }
                
                            }else{
                                $gen_id=1;
                            }
                
                            if(trim($esp)!=''){

                                $esp_nom=trim($esp);
                                $sqlesp="SELECT * FROM tblEspecie WHERE nomEsp='$esp_nom'";
                                $resultesp = mysqli_query($con, $sqlesp);
                                $rowesp = mysqli_num_rows($resultesp);
                                $rowesp_array = mysqli_fetch_array($resultesp);
                    
                                $sqlesp_id="SELECT * FROM tblEspecie";
                                $resultesp_id = mysqli_query($con, $sqlesp_id);
                                $rowesp_id = mysqli_num_rows($resultesp_id)+1;
                    
                                if($rowesp==0){
                                    
                                $sqlesp_ADD="INSERT INTO tblEspecie (`idCla`, `idSubCla`, `idOrd`, `idSubOrd`, `idFlia`, 'idGen', `idEsp`, `nomEsp`) VALUES ('1','1','1','1','$rowflia_id','$rowgen_id','$rowesp_id','$esp_nom')";
                                $resultesp_ADD = mysqli_query($con, $sqlesp_ADD);
                                //var_dump($resultFuen_ADD);
                                $esp_id = $rowesp_id;
                                    
                                }else{
                                    
                                $esp_id=$rowesp_array['idEsp'];
                                //echo $fuen_id;
                                    
                                }
                    
                                }else{
                                    $esp_id=1;
                                }
                    
                                if(trim($sesp)!=''){

                                    $sesp_nom=trim($sesp);
                                    $sqlsesp="SELECT * FROM tblSubespecie WHERE nomSubEsp='$sesp_nom'";
                                    $resultsesp = mysqli_query($con, $sqlsesp);
                                    $rowsesp = mysqli_num_rows($resultsesp);
                                    $rowsesp_array = mysqli_fetch_array($resultsesp);
                        
                                    $sqlsesp_id="SELECT * FROM tblSubespecie";
                                    $resultsesp_id = mysqli_query($con, $sqlsesp_id);
                                    $rowsesp_id = mysqli_num_rows($resultsesp_id)+1;
                        
                                    if($rowsesp==0){
                                        
                                    $sqlsesp_ADD="INSERT INTO tblSubespecie (`idCla`, `idSubCla`, `idOrd`, `idSubOrd`, `idFlia`, 'idGen', `idEsp`, `idSubEsp`, `nomSubEsp`) VALUES ('1','1','1','1','$rowflia_id','$rowgen_id','$rowesp_id','$rowsesp_id','$sesp_nom')";
                                    $resultsesp_ADD = mysqli_query($con, $sqlsesp_ADD);
                                    //var_dump($resultFuen_ADD);
                                    $sesp_id = $rowsesp_id;
                                        
                                    }else{
                                        
                                    $sesp_id=$rowsesp_array['idSubEsp'];
                                    //echo $fuen_id;
                                        
                                    }
                        
                                    }else{
                                        $sesp_id=1;
                                    }
                        
                                    if(trim($cla)!=''){

                                        $cla_nom=trim($cla);
                                        $sqlcla="SELECT * FROM tblClasificacion WHERE claNom='$cla_nom'";
                                        $resultcla = mysqli_query($con, $sqlcla);
                                        $rowcla = mysqli_num_rows($resultcla);
                                        $rowcla_array = mysqli_fetch_array($resultcla);
                            
                                        $sqlcla_id="SELECT * FROM tblClasificacion";
                                        $resultcla_id = mysqli_query($con, $sqlcla_id);
                                        $rowcla_id = mysqli_num_rows($resultcla_id)+1;
                            
                                        if($rowcla==0){
                                            
                                        $sqlcla_ADD="INSERT INTO tblClasificacion (`idCla`, `claNom`) VALUES ('$rowcla_id','$cla_nom')";
                                        $resultcla_ADD = mysqli_query($con, $sqlcla_ADD);
                                        //var_dump($resultFuen_ADD);
                                        $cla_id = $rowcla_id;
                                            
                                        }else{
                                            
                                        $cla_id=$rowcla_array['idCla'];
                                        //echo $fuen_id;
                                            
                                        }
                            
                                        }else{
                                            $cla_id=1;
                                        }
                            

            // tomo idExp de tblExpediente
            $ncalc = mysqli_fetch_array($con->query("SELECT MAX(idExp) AS ValorMaximo FROM tblExpediente"));
            $evisor = $ncalc['ValorMaximo']+1;
            
            // ***********************************************************************************
            echo "<br>.<br>.<br>.<br>.<br>.<br>"."INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`,
            `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, 
            `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) 
            VALUES(null, '$nvisor', '999', '999', '999', '$tit', '$desc', '$insti_id', '$fuen_id', '$url', 
            '$contac', '$deg_lat', '$min_lat', '$sec_lat', '$cor_lat', '$deg_lon', '$min_lon', '$sec_lon', '$cor_lon', '$mes', '$ano', '$geo_id', '$alt',
            '$tax', '$url_ex', 'S', '$ubi'.'-'.'$caj', '0', '$ubi'.'-'.'$caj', '4', 'N', '$duplicado', '$email_u')";
            // ***********************************************************************************

            $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`,
             `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, 
             `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) 
             VALUES(null, '$nvisor', '999', '999', '999', '$tit', '$desc', '$insti_id', '$fuen_id', '$url', 
             '$contac', '$deg_lat', '$min_lat', '$sec_lat', '$cor_lat', '$deg_lon', '$min_lon', '$sec_lon', '$cor_lon', '$mes', '$ano', '$geo_id', '$alt',
             '$tax', '$url_ex', 'S', '$ubi'.'-'.'$caj', '0', '$ubi'.'-'.'$caj', '4', 'N', '$duplicado', '$email_u')";
            $query_update = mysqli_query($con, $sql);
            //var_dump($query_update);     

            // ***********************************************************************************
            echo "<br>.<br>.<br>.<br>.<br>.<br>"."INSERT INTO `tblExpediente`('id', `idExp`, `expIdIns`, `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`, `expDpto`, `expLoc`, `expApr`, 
            `expNap`, `expEco`, `expCat`, `expLot`, `expEst`, `expSue`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, 
            `expAlt`, `expIdGeo`, `expVador`, `expCon`, `expFob`, `expFue`, `expUrl`, `expTax`, `expIdGru`, `expCju`, `expIdFlia`, `expIdGen`, `expIdEsp`, 
            `expIdSubEsp`, `expVul`, `expTipo`, `expSex`, `expIdCla`, `expMes`, `expAno`, `expRas`, `expObs`, `expExc`, `expOri`, `expUlt`, `expExp`, 
            `expExpor`, `expDup`, `expEmail`) 
            VALUES (null, '$evisor', '$insti_id', '$ubi', '$Nro', ' $ini', '$tor', '$tit', '$desc', '$dpto_id', '$loc_id', '$apr', 
            '$nap', '$eco_id', '$cat', '$lot', '$est', '$sue', '$deg_lat', '$min_lat', '$sec_lat', '$cor_lat', '$deg_lon', '$min_lon', '$sec_lon', '$cor_lon',   
            '$alt', '$geo_id', '$vador', '$contac',  '$fob', '$fuen_id', '$url', '$tax', '$gru_id', '$cju_id', '$flia_id', '$gen_id', '$esp_id',
            '$sesp_id', '$vul', '$tipo', '$sex', '$cla_id', '$mes', '$ano', '$ras', '$obs', '$expExc', 
            '4', 'S', 'N', '$duplicado', '$email_u')";
            // ***********************************************************************************

            // grabo en tblExpediente
            $sqlExp="INSERT INTO `tblExpediente`('id', `idExp`, `expIdIns`, `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`, `expDpto`, `expLoc`, `expApr`, 
            `expNap`, `expEco`, `expCat`, `expLot`, `expEst`, `expSue`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, 
            `expAlt`, `expIdGeo`, `expVador`, `expCon`, `expFob`, `expFue`, `expUrl`, `expTax`, `expIdGru`, `expCju`, `expIdFlia`, `expIdGen`, `expIdEsp`, 
            `expIdSubEsp`, `expVul`, `expTipo`, `expSex`, `expIdCla`, `expMes`, `expAno`, `expRas`, `expObs`, `expExc`, `expOri`, `expUlt`, `expExp`, 
            `expExpor`, `expDup`, `expEmail`) 
            VALUES (null, '$evisor', '$insti_id', '$ubi', '$Nro', ' $ini', '$tor', '$tit', '$desc', '$dpto_id', '$loc_id', '$apr', 
            '$nap', '$eco_id', '$cat', '$lot', '$est', '$sue', '$deg_lat', '$min_lat', '$sec_lat', '$cor_lat', '$deg_lon', '$min_lon', '$sec_lon', '$cor_lon',   
            '$alt', '$geo_id', '$vador', '$contac',  '$fob', '$fuen_id', '$url', '$tax', '$gru_id', '$cju_id', '$flia_id', '$gen_id', '$esp_id',
            '$sesp_id', '$vul', '$tipo', '$sex', '$cla_id', '$mes', '$ano', '$ras', '$obs', '$expExc', 
            '4', 'S', 'N', '$duplicado', '$email_u')";

            $query_update = mysqli_query($con, $sqlExp);
            }
        }
    }


mysqli_close($con);
?>	
<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script>
    function editar_alert(id){

            $.ajax({
                url: "ajax/editar_alert.php",
                method:"post",
                data:{id:id},
                success:function(data){
                    $('#editar_registrov').html(data);
                    $('#myModal101').modal("show");
                }
            });
            }
</script>	
		
	