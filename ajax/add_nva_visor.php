<?php
require_once("../conn.php");


    if (empty($_POST['id'])) {
         $errors[] = "Entrada id vacía";
        } else if (empty($_POST['capa_1'])){
         $errors[] = "Entrada Capas vacía";
        } else if (empty($_POST['capa_2'])){
         $errors[] = "Entrada Subcapas vacía";
        } else if (empty($_POST['capa_3'])){
         $errors[] = "Entrada Ecoregión vacía";
        } else if (empty($_POST['visTit'])){
         $errors[] = "Entrada Título vacía";
        } else if (empty($_POST['visDes'])){
         $errors[] = "Entrada Descripción vacía";
        } else if (empty($_POST['insti'])){
         $errors[] = "Entrada Institución vacía";
        } else if (empty($_POST['fuen'])){
         $errors[] = "Entrada Fuente vacía";
        } else if (empty($_POST['email_u'])){
         $errors[] = "Entrada email vacía";
      
       
        
        }   else if (

            !empty($_POST['id']) &&
            !empty($_POST['capa_1']) &&
            !empty($_POST['capa_2']) &&
            !empty($_POST['capa_3']) &&
            !empty($_POST['visTit']) &&
            !empty($_POST['visDes']) &&
            !empty($_POST['insti']) &&
            !empty($_POST['fuen']) &&
            !empty($_POST['email_u']) 
            
        ){

      

        // escaping, additionally removing everything that could be (html/javascript-) code
            
            $id = mysqli_real_escape_string($con,(strip_tags($_POST["id"],ENT_QUOTES)));
            //$capa1 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_1"],ENT_QUOTES)));
            //$capa2 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_2"],ENT_QUOTES)));
            //$capa3 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_3"],ENT_QUOTES)));
            $visTit = mysqli_real_escape_string($con,(strip_tags($_POST["visTit"],ENT_QUOTES)));
            $visDes = mysqli_real_escape_string($con,(strip_tags($_POST["visDes"],ENT_QUOTES)));
            //$inst = mysqli_real_escape_string($con,(strip_tags($_POST["insti"],ENT_QUOTES)));
            $visFue = mysqli_real_escape_string($con,(strip_tags($_POST["fuen"],ENT_QUOTES)));
            /*$visGeo = mysqli_real_escape_string($con,(strip_tags($_POST["geo_"],ENT_QUOTES)));*/
            
            $visUrl = mysqli_real_escape_string($con,(strip_tags($_POST["visUrl"],ENT_QUOTES)));
            $visCon = mysqli_real_escape_string($con,(strip_tags($_POST["visCon"],ENT_QUOTES)));
            
            if (empty($_POST['outGra'])){
             $colLatG = 0;
            } else{
            $colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            } 
            if (empty($_POST['outMin'])){
             $colLatM = 0;
            } else{
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            } 
            if (empty($_POST['outSeg'])){
             $colLatS = 0;
            } else{
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            }
            if (empty($_POST['d'])){
             $colLatX = ' ';
            } else{
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            }
            if (empty($_POST['glo'])){
             $colLonG = 0;
            } else{
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            }
            if (empty($_POST['mlo'])){
             $colLonM = 0;
            } else{
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['slo'])){
             $colLonS = 0;
            } else{
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            } 
            if (empty($_POST['dlo'])){
             $colLonX = ' ';
            } else{
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['geom'])){
             $Geo = 1;
            } else{
            $Geo = mysqli_real_escape_string($con,(strip_tags($_POST["geom"],ENT_QUOTES)));
            }
            if (empty($_POST['visAlt'])){
             $Alt = 0;
            } else{
            $Alt = mysqli_real_escape_string($con,(strip_tags($_POST["visAlt"],ENT_QUOTES)));
            }
            if (empty($_POST['visTax'])){
             $Tax = 0;
            } else{
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["visTax"],ENT_QUOTES)));
            }
            if (empty($_POST['visExp'])){
             $visExp = 766;
             } else{
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["visExp"],ENT_QUOTES)));
            }
            /*$colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));*/
            $visMes = mysqli_real_escape_string($con,(strip_tags($_POST["visMes"],ENT_QUOTES)));
            $visAni= mysqli_real_escape_string($con,(strip_tags($_POST["visAni"],ENT_QUOTES)));
            
            /*$Alt = mysqli_real_escape_string($con,(strip_tags($_POST["visAlt"],ENT_QUOTES)));
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["visTax"],ENT_QUOTES)));
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["visExp"],ENT_QUOTES)));*/
            $visEmail = mysqli_real_escape_string($con,(strip_tags($_POST["email_u"],ENT_QUOTES)));

            $visExc = 'nva-planilla'.$id.'';
            $visUlt = 'Y';
            $visAcro = '';
            $visNroCol = 0;
            $visOri = '4';
            $visExpor = 'N';
            $visDup = '';

            if(is_array($_POST["capa_3"]))
            {

            foreach ($_POST["capa_3"] as $clave=>$valor)
                {

                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];

                $contar = count($_POST["insti"]);
                $contar2 = 0;
                $check = 'off';
                if($contar>1){
                    $duplicado='S';
                    $contar2=0;
                }else{
                    $duplicado='N';
                }

                foreach ($_POST["insti"] as $clave2=>$valor2)
                    {

                        $contar2=$contar2+1;

                        if($duplicado=='S' && $contar2==1){
                            $duplicado='N';
                            $check='on';
                        }
                        if($check=='on' && $duplicado=='N' && $contar2>1){
                            $duplicado='S';
                            $check='off';
                        }

                    $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) VALUES(null, '$id', '$FindDisc', '$FindSubDisc', '$valor', '$visTit', '$visDes', '$valor2', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', '$visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$duplicado', '$visEmail')";
                    $query_update = mysqli_query($con, $sql);

                    }


                }
            }

            //$sql="INSERT INTO tblvisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) VALUES(null, '$id', '$capa1', '$capa2', '$capa3', '$visTit', '$visDes', '$inst', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', 'visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$visDup', '$visEmail')";
          
            //$query_update = mysqli_query($con, $sql);
       
   
            if ($query_update){
                $messages[] = "Se ha agregado el registro satisfactoriamente.";
            } else{
                $errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
            }
            

        } else {
            $errors []= "Error desconocido.";
        }
        
        if (isset($errors)){
            
            ?>
            <div class="alert alert-danger" role="alert">
               
             

                    <strong>Error!</strong> 
                    <?php
                        foreach ($errors as $error) {
                                echo $error;
                            }
                        ?>
            </div>

            
            <?php
            }
            if (isset($messages)){
                
                ?>
                <div class="alert alert-success" role="alert">
                
                    <strong>Bien!</strong> 
                    <?php
                        foreach ($messages as $message) {
                                echo $message;
                            }
                        ?>
            </div>

                
                <?php
            }

?>  