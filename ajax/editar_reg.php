<?php
require_once("../conn.php");
$response = array();
$response['page']=$_POST['page'];

    if (empty($_POST['id'])) {
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada id vacía</div>";
        } else if (empty($_POST['capa_1'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Capas vacía</div>";
        } else if (empty($_POST['capa_2'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Subcapas vacía</div>";
        } else if (empty($_POST['capa_3'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Ecoregion vacía</div>";
        } else if (empty($_POST['visTit'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Titulo vacía</div>";
        } else if (empty($_POST['visDes'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Descripcion vacía</div>";
        } else if (empty($_POST['insti'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Institucion vacía</div>";
        } else if (empty($_POST['fuen'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada Fuente vacía</div>";
        } else if (empty($_POST['email_u'])){
         $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Entrada email vacía</div>";
      
       
        
        }   else if (

            !empty($_POST['id']) &&
            !empty($_POST['capa_1']) &&
            !empty($_POST['capa_2']) &&
            !empty($_POST['capa_3']) &&
            !empty($_POST['visTit']) &&
            !empty($_POST['visDes']) &&
            !empty($_POST['insti']) &&
            !empty($_POST['fuen']) &&
            !empty($_POST['email_u']) 
            
        ){

      

        // escaping, additionally removing everything that could be (html/javascript-) code
            
            $id = mysqli_real_escape_string($con,(strip_tags($_POST["id"],ENT_QUOTES)));
            //$capa1 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_1"],ENT_QUOTES)));
            //$capa2 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_2"],ENT_QUOTES)));
            //$capa3 = mysqli_real_escape_string($con,(strip_tags($_POST["capa_3"],ENT_QUOTES)));
            $visTit = mysqli_real_escape_string($con,(strip_tags($_POST["visTit"],ENT_QUOTES)));
            $visDes = mysqli_real_escape_string($con,(strip_tags($_POST["visDes"],ENT_QUOTES)));
            $insti = mysqli_real_escape_string($con,(strip_tags($_POST["insti"],ENT_QUOTES)));
            $visFue = mysqli_real_escape_string($con,(strip_tags($_POST["fuen"],ENT_QUOTES)));
            /*$visGeo = mysqli_real_escape_string($con,(strip_tags($_POST["geo_"],ENT_QUOTES)));*/
            
            $visUrl = mysqli_real_escape_string($con,(strip_tags($_POST["visUrl"],ENT_QUOTES)));
            $visCon = mysqli_real_escape_string($con,(strip_tags($_POST["visCon"],ENT_QUOTES)));
            
            if (empty($_POST['outGra'])){
             $colLatG = 0;
            } else{
            $colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            } 
            if (empty($_POST['outMin'])){
             $colLatM = 0;
            } else{
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            } 
            if (empty($_POST['outSeg'])){
             $colLatS = 0;
            } else{
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            }
            if (empty($_POST['d'])){
             $colLatX = 0;
            } else{
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            }
            if (empty($_POST['glo'])){
             $colLonG = 0;
            } else{
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            }
            if (empty($_POST['mlo'])){
             $colLonM = 0;
            } else{
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['slo'])){
             $colLonS = 0;
            } else{
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            } 
            if (empty($_POST['dlo'])){
             $colLonX = 0;
            } else{
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));
            } 
            if (empty($_POST['geom'])){
             $Geo = 1;
            } else{
            $Geo = mysqli_real_escape_string($con,(strip_tags($_POST["geom"],ENT_QUOTES)));
            }
            if (empty($_POST['visAlt'])){
             $Alt = 0;
            } else{
            $Alt = mysqli_real_escape_string($con,(strip_tags($_POST["visAlt"],ENT_QUOTES)));
            }
            if (empty($_POST['visTax'])){
             $Tax = 0;
            } else{
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["visTax"],ENT_QUOTES)));
            }
            if (empty($_POST['visExp'])){
             $visExp = 766;
             } else{
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["visExp"],ENT_QUOTES)));
            }
            /*$colLatG = mysqli_real_escape_string($con,(strip_tags($_POST["outGra"],ENT_QUOTES)));
            $colLatM = mysqli_real_escape_string($con,(strip_tags($_POST["outMin"],ENT_QUOTES)));
            $colLatS = mysqli_real_escape_string($con,(strip_tags($_POST["outSeg"],ENT_QUOTES)));
            $colLatX = mysqli_real_escape_string($con,(strip_tags($_POST["d"],ENT_QUOTES)));
            $colLonG = mysqli_real_escape_string($con,(strip_tags($_POST["glo"],ENT_QUOTES)));
            $colLonM = mysqli_real_escape_string($con,(strip_tags($_POST["mlo"],ENT_QUOTES)));
            $colLonS = mysqli_real_escape_string($con,(strip_tags($_POST["slo"],ENT_QUOTES)));
            $colLonX = mysqli_real_escape_string($con,(strip_tags($_POST["dlo"],ENT_QUOTES)));*/
            $visMes = mysqli_real_escape_string($con,(strip_tags($_POST["visMes"],ENT_QUOTES)));
            $visAni= mysqli_real_escape_string($con,(strip_tags($_POST["visAni"],ENT_QUOTES)));
            
            /*$Alt = mysqli_real_escape_string($con,(strip_tags($_POST["visAlt"],ENT_QUOTES)));
            $Tax = mysqli_real_escape_string($con,(strip_tags($_POST["visTax"],ENT_QUOTES)));
            $visExp = mysqli_real_escape_string($con,(strip_tags($_POST["visExp"],ENT_QUOTES)));*/
            $visEmail = mysqli_real_escape_string($con,(strip_tags($_POST["email_u"],ENT_QUOTES)));
            $id_visor = mysqli_real_escape_string($con,(strip_tags($_POST["id_visor"],ENT_QUOTES)));
            
            if($_POST["visExc"]==''){
                $visExc = '0';
            }else{
                $visExc = $_POST["visExc"];
            }
            
            
            $visUlt = mysqli_real_escape_string($con,(strip_tags($_POST["visUlt"],ENT_QUOTES)));
            $visAcro = mysqli_real_escape_string($con,(strip_tags($_POST["visAcro"],ENT_QUOTES)));
            $visNroCol = mysqli_real_escape_string($con,(strip_tags($_POST["visNroCol"],ENT_QUOTES)));
            $visOri = mysqli_real_escape_string($con,(strip_tags($_POST["visOri"],ENT_QUOTES)));
            $visExpor = mysqli_real_escape_string($con,(strip_tags($_POST["visExpor"],ENT_QUOTES)));
            $visDup = mysqli_real_escape_string($con,(strip_tags($_POST["visDup"],ENT_QUOTES)));
            /*$visUlt = 'Y';
            $visAcro = '';
            $visNroCol = 0;
            $visOri = '4';
            $visExpor = 'N';
            $visDup = '';*/

            $validator=$_POST["validator"];

            if($validator!='ok'){

                

                if(is_array($_POST["capa_3"]))
                {
                    $perm_deletd='si';
                    
                    foreach ($_POST["capa_3"] as $clave=>$valor)
                    {

                    $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                    $resultEco = mysqli_query($con, $sqlSelectEco);
                    $rowEco = mysqli_fetch_array($resultEco);
                    $FindDisc = $rowEco['idDisc'];
                    $FindSubDisc = $rowEco['idSubDisc'];

                    $sql="INSERT INTO tblVisor(`id`, `idVisor`, `visIdDisc`, `visIdSubDisc`, `visIdEco`, `visTit`, `visDes`, `idIns`, `visFue`, `visUrl`, `visCon`, `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, `visMes`, `visAni`, `idGeo`, `visAlt`, `visTax`, `visExc`, `visUlt`, `visExp`, `visAcro`, `visNroCol`, `visOri`, `visExpor`, `visDup`, `visEmail`) VALUES(null, '$id_visor', '$FindDisc', '$FindSubDisc', '$valor', '$visTit', '$visDes', '$insti', '$visFue', '$visUrl', '$visCon', '$colLatG', '$colLatM', '$colLatS', '$colLatX', '$colLonG', '$colLonM', '$colLonS', '$colLonX', '$visMes', '$visAni', '$Geo', '$Alt', '$Tax', '$visExc', '$visUlt', '$visExp', '$visAcro', '$visNroCol', '$visOri', '$visExpor', '$visDup', '$visEmail')";

            
                        $query_update = mysqli_query($con, $sql);
                }
            }else{

                $perm_deletd='no';

                $valor=$_POST["capa_3"];
                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];
              

                $sql="UPDATE tblVisor SET visIdDisc='$FindDisc', visIdSubDisc='$FindSubDisc', visIdEco='$valor', visTit='$visTit', visDes='$visDes', idIns='$insti', visFue='$visFue', visUrl='$visUrl', visCon='$visCon', colLatG='$colLatG', colLatM='$colLatM', colLatS='$colLatS', colLatX='$colLatX', colLonG='$colLonG', colLonM='$colLonM', colLonS='$colLonS', colLonX='$colLonX', visMes='$visMes', visAni='$visAni', idGeo='$Geo', visAlt='$Alt', visTax='$Tax', visExc='$visExc', visUlt='$visUlt', visExp='$visExp', visAcro='$visAcro', visNroCol='$visNroCol', visOri='$visOri', visExpor='$visExpor', visDup='$visDup', visEmail='$visEmail' WHERE id='$id'";

                $query_update = mysqli_query($con, $sql);

            }   

            }else{

                $perm_deletd='no';

                $valor=$_POST["capa_3"];
                $sqlSelectEco="SELECT * FROM tblEcoregion WHERE idEco='$valor'";
                $resultEco = mysqli_query($con, $sqlSelectEco);
                $rowEco = mysqli_fetch_array($resultEco);
                $FindDisc = $rowEco['idDisc'];
                $FindSubDisc = $rowEco['idSubDisc'];
              

                $sql="UPDATE tblVisor SET visIdDisc='$FindDisc', visIdSubDisc='$FindSubDisc', visIdEco='$valor', visTit='$visTit', visDes='$visDes', idIns='$insti', visFue='$visFue', visUrl='$visUrl', visCon='$visCon', colLatG='$colLatG', colLatM='$colLatM', colLatS='$colLatS', colLatX='$colLatX', colLonG='$colLonG', colLonM='$colLonM', colLonS='$colLonS', colLonX='$colLonX', visMes='$visMes', visAni='$visAni', idGeo='$Geo', visAlt='$Alt', visTax='$Tax', visExc='$visExc', visUlt='$visUlt', visExp='$visExp', visAcro='$visAcro', visNroCol='$visNroCol', visOri='$visOri', visExpor='$visExpor', visDup='$visDup', visEmail='$visEmail' WHERE id='$id'";

                $query_update = mysqli_query($con, $sql);

            }

            if($perm_deletd=='si'){

                $sql="DELETE FROM tblVisor WHERE id ='".$id."'";
                $query_update = $con->query($sql);
            }

            if ($query_update){
                $response['status'] = "<div class='alert alert-success' role='alert'><strong>Bien!</strong> Se ha actualizado el registro satisfactoriamente.</div>";
            } else{
                $response['status'] = "<div class='alert alert-danger' role='alert'><strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.</div>".mysqli_error($con);
            }
            

        } else {
            $response['status']= "Error desconocido.";
        }
        
        if (isset($errors)){
            
            ?>
            <div class="alert alert-danger" role="alert">
               
             

                    <strong>Error!</strong> 
                    <?php

                        foreach ($response['status'] as $error) {
                               // echo $error;

                            }
                        ?>
            </div>

            
            <?php
            }
            if (isset($messages)){
                
                ?>
                <div class="alert alert-success" role="alert">
                
                    <strong>Bien!</strong> 
                    <?php



                        foreach ($response['status'] as $message) {
                                //echo $message;
                            }
                        ?>
            </div>

                
                <?php
            }

            echo json_encode($response);

?>  