<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    /*COLOR BACKGROUND EVENT HOVER*/
    table#sample_1.display tbody tr.marca:nth-child(even):hover td{
        background-color: #a5f4fb !important;
    }

    table#sample_1.display tbody tr.marca:nth-child(odd):hover td{
        background-color: #a5f4fb !important;
    }
    /*COLOR BACKGROUND EVENT HOVER*/

    /*COLOR BACKGROUND SET*/
    table#sample_1.display tbody tr.marca2:nth-child(even) td{
        background-color: #ffa !important;
    }

    table#sample_1.display tbody tr.marca2:nth-child(odd) td{
        background-color: #ffa !important;
    }
    /*COLOR BACKGROUND SET*/

</style>

<?php
function convertDMSToDecimal($latlng) {
    $valid = false;
    $decimal_degrees = 0;
    $degrees = 0; $minutes = 0; $seconds = 0; $direction = 1;

    // Determine if there are extra periods in the input string
    $num_periods = substr_count($latlng, '.');
    if ($num_periods > 1) {
        $temp = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // replace all but last period with delimiter
        $temp = trim(preg_replace('/[a-zA-Z]/','',$temp)); // when counting chunks we only want numbers
        $chunk_count = count(explode(" ",$temp));
        if ($chunk_count > 2) {
            $latlng = preg_replace('/\./', ' ', $latlng, $num_periods - 1); // remove last period
        } else {
            $latlng = str_replace("."," ",$latlng); // remove all periods, not enough chunks left by keeping last one
        }
    }
    
    // Remove unneeded characters
    $latlng = trim($latlng);
    $latlng = str_replace("º"," ",$latlng);
    $latlng = str_replace("°"," ",$latlng);
    $latlng = str_replace("'"," ",$latlng);
    $latlng = str_replace("\""," ",$latlng);
    $latlng = str_replace("  "," ",$latlng);
    $latlng = substr($latlng,0,1) . str_replace('-', ' ', substr($latlng,1)); // remove all but first dash

    if ($latlng != "") {
        // DMS with the direction at the start of the string
        if (preg_match("/^([nsewoNSEWO]?)\s*(\d{1,3})\s+(\d{1,3})\s*(\d*\.?\d*)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[2]);
            $minutes = intval($matches[3]);
            $seconds = floatval($matches[4]);
            if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                $direction = -1;
        }
        // DMS with the direction at the end of the string
        elseif (preg_match("/^(-?\d{1,3})\s+(\d{1,3})\s*(\d*(?:\.\d*)?)\s*([nsewoNSEWO]?)$/",$latlng,$matches)) {
            $valid = true;
            $degrees = intval($matches[1]);
            $minutes = intval($matches[2]);
            $seconds = floatval($matches[3]);
            if (strtoupper($matches[4]) == "S" || strtoupper($matches[4]) == "W" || $degrees < 0) {
                $direction = -1;
                $degrees = abs($degrees);
            }
        }
        if ($valid) {
            // A match was found, do the calculation
            $decimal_degrees = ($degrees + ($minutes / 60) + ($seconds / 3600)) * $direction;
        } else {
            // Decimal degrees with a direction at the start of the string
            if (preg_match("/^([nsewNSEW]?)\s*(\d+(?:\.\d+)?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[1]) == "S" || strtoupper($matches[1]) == "W")
                    $direction = -1;
                $decimal_degrees = $matches[2] * $direction;
            }
            // Decimal degrees with a direction at the end of the string
            elseif (preg_match("/^(-?\d+(?:\.\d+)?)\s*([nsewNSEW]?)$/",$latlng,$matches)) {
                $valid = true;
                if (strtoupper($matches[2]) == "S" || strtoupper($matches[2]) == "W" || $degrees < 0) {
                    $direction = -1;
                    $degrees = abs($degrees);
                }
                $decimal_degrees = $matches[1] * $direction;
            }
        }
    }
    if ($valid) {
        return $decimal_degrees;
    } else {
        return false;
    }
}
require_once("../conn.php");
 $idVis=$_POST['ids'];
 $idIns=$_POST['idIns'];

    $response='';
    $response.='
    <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i>Nueva planilla Expedientes'.$idVis.' </div>
                                        <div class="tools"> </div>
                                    </div>
                                    
                                    <div class="portlet-body table-both-scroll">
    <table class="table table-striped table-bordered table-hover table-fixed text-nowrap order-column display" id="sample_1">
                                            <thead>
                                                <tr>
                                                <th>Institución</th>
                                                <th>Ubicación</th>
                                                <th>Nro. Exp.</th>
                                                <th>Iniciado</th>
                                                <th>Consul. Amb.</th>
                                                <th>Título</th>
                                                <th>Descripción</th>
                                                <th>Dpto.</th>
                                                <th>Localid.</th>
                                                <th>Área protegida</th>
                                                <th>Name_ap</th>
                                                <th>Catastro</th>
                                                <th>Lote</th>
                                                <th>Estado</th>
                                                <th>Uso suelo</th>
                                                <th>Latitud</th>
                                                <th>Longitud</th>
                                                <th>Altura m.s.</th>
                                                <th>Geometría</th>
                                                <th>Observador</th>
                                                <th>Contacto</th>
                                                <th>Fec. Observac.</th>
                                                <th>Fuente</th>
                                                <th>URL</th>
                                                <th>Contacto</th>
                                                <th># Taxones</th>
                                                <th>Cnjt. Esp.</th>
                                                <th>Grupo</th>
                                                <th>Familia</th>
                                                <th>Género</th>
                                                <th>Especie</th>
                                                <th>Subespecie</th>
                                                <th>Nom. Vulgar</th>
                                                <th>Tipo Reg.</th>
                                                <th>Sexo</th>
                                                <th>Clasificación</th>
                                                <th>Mes</th>
                                                <th>Año</th>
                                                <th>Rastros</th>
                                                <th>Observaciones</th>
                                                <th>Excel origen</th>
                                                <th>Acciones</th>
                                        </tr>
                                            </thead>
                                            <tbody>';
    //$response = array(); 

//    $sqlExc="SELECT expExc FROM tblExpediente WHERE idExp='$idVis'";
    $sqlExc="SELECT 't1.id', `idExp`, `expIdIns`, 't5.insNom' AS insNom, `expUbi`, `expNro`, `expIni`, `expTor`, `expTit`, `expDes`, 
    `expDpto`, 't12.Nombre' AS Dpto, `expLoc`, 't14.Nombre' AS Loca, `expApr`, `expNap`, `expEco`, 't15.nomEco' AS nomEco, `expCat`, `expLot`,
    `expEst`, `expIdUsoSue`, 
    `colLatG`, `colLatM`, `colLatS`, `colLatX`, `colLonG`, `colLonM`, `colLonS`, `colLonX`, 
    `expAlt`, `expIdGeo`, 't6.geoNom' AS Geometria, `expVador`, `expCon`, `expFob`,
     `expFue`, 't7.nomFue' AS nomFue, `expUrl`, `expTax`, `expIdGru`, 't2.nomGru' AS nomGru, `expCju`, 't3.nomCju' AS nomCju, 
     `expIdFlia`, 't4.nomFlia' AS nomFlia, `expIdGen`, 't8.nomGen' AS nomGen, `expIdEsp`, 't9.nomEsp' AS nomEsp, `expIdSubEsp`, 't10.nomSubEsp' AS nomSubEsp, 
     `expVul`, `expTipo`, `expSex`, `expIdCla`, 't11.claNom' AS Clasific, `expMes`, `expAno`, `expRas`, `expObs`, 
     `expExc`, `expOri`, `expUlt`, `expExp`, `expExpor`, `expDup`, `expEmail`
     FROM tblExpediente t1 
JOIN tblInstitucion t5 ON t1.expIdIns=t5.idIns 
JOIN tblGeometria t6 ON t1.expIdGeo=t6.idGeo 
JOIN tblFuente t7 ON t1.expFue=t7.idFue
JOIN departamento t13 ON t1.expDpto=t13.ID 
JOIN localidad t14 ON t1.expLoc=t14.ID 
JOIN tblEcoregion t15 ON t1.expEco=t15.idEco 
JOIN tblGrupo t2 ON t1.expIdGru=t2.idGru
JOIN tblConjunto t3 ON t1.expCju=t3.idCju
JOIN tblFamilia t4 ON t1.expIdFlia=t4.idFlia
JOIN tblGenero t8 ON t1.expIdGen=t8.IdGen
JOIN tblespecie t9 ON t1.expIdEsp=t9.idEsp
JOIN tblsubespecie t10 ON t1.expIdSubEsp=t10.idSubEsp
JOIN tblclasificacion t11 ON t1.expIdCla=t11.idCla
WHERE (expUlt='S' OR expUlt='Y') AND expExpor='N' AND (expOri=4 OR expOri=5) AND id='$idVis'
ORDER BY insNom ASC";
    $resultExc=mysqli_query($con, $sqlExc);
//    $rowExc = mysqli_fetch_array($resultExc);
         
    //$sqlSelectVis="SELECT * FROM tblvisor WHERE idVisor='$idVis'";
    //$sqlSelectVis="SELECT * FROM tblExpediente t1 JOIN tblInstitucion t5 ON t1.expIdIns=t5.idIns JOIN tblGeometria t6 ON t1.expIdGeo=t6.idGeo JOIN tblFuente t7 ON t1.expFue=t7.idFue WHERE (expUlt='S' OR expUlt='Y') AND expExpor='N' AND (expOri=1 OR expOri=4) ORDER BY insNom ASC";
    $counter=0;
    //$count=0;
        while ($rowVis = mysqli_fetch_array($resultExc)) {

        $latitud_data=''.$rowVis['colLatG'].'° '.$rowVis['colLatM'].'\' '.$rowVis['colLatS'].'" '.$rowVis['colLatX'].'';
        $latitud = convertDMSToDecimal($latitud_data);
        $longitud_data=''.$rowVis['colLonG'].'° '.$rowVis['colLonM'].'\' '.$rowVis['colLonS'].'" '.$rowVis['colLonX'].'';
        $longitud = convertDMSToDecimal($longitud_data);

        if($rowVis['expDup']=='S'){
            $duplicado="marca2";
        }else{
            $duplicado="";
        }
            $response.='
            <tr class="'.$duplicado.'">
            <td>'.$rowVis['id'].'</td>
            <td>'.$rowInsti['insNom'].'</td>
            <td>'.$rowVis['expUbi'].'</td>
            <td>'.$rowVis['expNro'].'</td>
            <td>'.$rowVis['expIni'].'</td>
            <td>'.$rowVis['expTor'].'</td>
            <td>'.$rowVis['expTit'].'</td>
            <td>'.$rowVis['expDes'].'</td>
            <td>'.$rowVis['Dpto'].'</td>
            <td>'.$rowVis['Loca'].'</td>
            <td>'.$rowVis['expApr'].'</td>
            <td>'.$rowVis['expNap'].'</td>
            <td>'.$roweco['nomEco'].'</td>
            <td>'.$rowVis['expCat'].'</td>
            <td>'.$rowVis['expLot'].'</td>
            <td>'.$rowVis['expEst'].'</td>
            <td>'.$rowVis['expIdUsoSue'].'</td>
            <td>'.$latitud_data.'</td>
            <td>'.$longitud_data.'</td>
            <td>'.$rowVis['expAlt'].'</td>
            <td>'.$rowGeo['geoNom'].'</td>
            <td>'.$rowVis['expVador'].'</td>
            <td>'.$rowVis['expCon'].'</td>
            <td>'.$rowVis['expFob'].'</td>
            <td>'.$rowFuen['nomFue'].'</td>
            <td>'.$rowVis['expUrl'].'</td>
            <td>'.$rowVis['expTax'].'</td>
            
            <td>'.$rowGru['nomGru'].'</td>
            <td>'.$rowCju['nomCju'].'</td>
            <td>'.$rowFlia['nomFlia'].'</td>
            <td>'.$rowGen['nomGen'].'</td>
            <td>'.$rowEsp['nomEsp'].'</td>
            <td>'.$rowsEsp['nomSubEsp'].'</td>
            <td>'.$rowVis['expVul'].'</td>
            <td>'.$rowVis['expTipoReg'].'</td>
            <td>'.$rowVis['expSex'].'</td>
            <td>'.$rowCla['Clasific'].'</td>
            
            <td>'.$rowVis['expMes'].'</td>
            <td>'.$rowVis['expAno'].'</td>
            <td>'.$rowVis['expRas'].'</td>
            <td>'.$rowVis['expObs'].'</td>
            <td>'.$rowVis['expExc'].'</td>
            <td>
            <div align="center">  <a class="btn btn-sm blue btn-outline filter-cancel" data-toggle="modal" data-target="#myModal101" onclick="editar_new_alert('.$rowVis['idExp'].','.$idVis.')"><i class="fa fa-edit fa-lg"></i> Editar</a>
            <a class="btn btn-sm red btn-outline filter-cancel" data-toggle="modal" data-target="#myModal100" onclick="eliminar_new_alert('.$rowVis['idExp'].')"><i class="fa fa-trash-o fa-lg"></i> Eliminar</a>
            </td>
            </tr>';
            //$response[$count]['value']=$rowVis['idSubDisc'];
            //$response[$count]['name']=$rowViss['nomSubDisc'];
            //$count=$count+1;
        }
        $response.=' </tbody>
                                        </table>
                                        </div>
                                     
                                </div>';
    echo $response;		

?>	

<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="assets/pages/scripts/table-datatables-buttons2.js" type="text/javascript"></script>
