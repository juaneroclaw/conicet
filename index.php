<?php
ob_start();
session_start();
require_once("conn.php");

if ( isset($_SESSION['name'])!="" ) {
        header("Location: menu.php");
        exit;
    }

if(isset($_POST["login-submit"])){
    if(!empty($_POST['username']) && !empty($_POST['password'])) {
    $usuarioEmpleado=$_POST['username'];
    $claveEmpleado=$_POST['password'];
    $passHash = strtoupper(hash('sha256', $claveEmpleado));
    
    

    $consulta =mysqli_query($con, "SELECT * FROM login WHERE email='$usuarioEmpleado' AND password='$passHash'");
    $row = mysqli_fetch_array($consulta);
    $count = mysqli_num_rows($consulta);
    
    if( $count == 1 && $row['password']==$passHash ) {

     $_SESSION['loggedin'] = true;
     $_SESSION['name'] = $row['nick'];
     $_SESSION['email'] = $row['email'];
     $_SESSION['start'] = time();
     $_SESSION['expire'] = $_SESSION['start'] + (20 * 60) ;

     header("Location: menu.php");

        } else {
            $mensaje = "Credenciales Incorrectas, Intente de nuevo...";
                
        }
   

    
} else {
    $mensaje = "Todos los campos son requeridos!";
}
}

if(isset($_POST["register-submit-btn"])){

    $nombreCliente=$_POST['fullname'];
    $instCliente=$_POST['inst'];
    $direccionCliente=$_POST['email'];
    $usuarioCliente=$_POST['username'];
    $claveCliente=$_POST['password'];
    $passHash = strtoupper(hash('sha256', $claveCliente));


    $consulta=mysqli_query($con, "SELECT * FROM login WHERE nick='$usuarioCliente'");
    $numeroFilas=mysqli_num_rows($consulta);
    

    if($numeroFilas==0)
    {

    // The password_hash() function convert the password in a hash before send it to the database
    $sql="INSERT INTO login(`id`,`idInst`,`name`,`email`,`nick`,`password`) VALUES(null, '$instCliente', '$nombreCliente', '$direccionCliente', '$usuarioCliente', '$passHash')";
    $resultado=mysqli_query($con, $sql);
    print($resultado);

    if($resultado){
     $mensaje = "Cuenta creada correctamente";
    } else {
     $mensaje = "Error al ingresar datos de la información.";
    }
    } else {
     $mensaje = "El nombre de usuario ya existe! Por favor, intenta con otro.";
    }

}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>INECOA | Iniciar Sesion</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="author" content="Desarrollado por Ana Lorenzo *-* INECOA-CONICET" />
        <meta name="description" content="Contactame anaconicet@gmail.com"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/fonts-googleapis.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/login-4.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.php">
                <img src="assets/pages/img/logo-big.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="" method="post">
                <h3 class="form-title">Iniciar Sesion</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Todos los campos son requeridos. </span>
                </div>
                <div class="alert alert-success display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Cuenta registrada correctamente! </span>
                </div>
                <div class="form-group">

                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="username" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Recuerdame
                        <span></span>
                    </label>
                    <button type="submit" name="login-submit" class="btn green pull-right"> Entrar </button>
                </div>
                <!--<div class="login-options">
                    <h4>Or login with</h4>
                    <ul class="social-icons">
                        <li>
                            <a class="facebook" data-original-title="facebook" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="twitter" data-original-title="Twitter" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="linkedin" data-original-title="Linkedin" href="javascript:;"> </a>
                        </li>
                    </ul>
                </div>-->
                <!--<div class="forget-password">
                    <h4>Olvido su contraseña ?</h4>
                    <p> Haga click
                        <a href="javascript:;" id="forget-password"> aqui </a> para recuperar su contraseña. </p>
                </div>-->
                <div class="create-account">
                    <p> No estas registrado ?&nbsp;
                        <a href="javascript:;" id="register-btn"> Registrate aquí </a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <!--<form class="forget-form" action="index.html" method="post">
                <h3>Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">Back </button>
                    <button type="submit" class="btn green pull-right"> Submit </button>
                </div>
            </form>-->
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form class="register-form" action="" method="post">
                <h3>Registrarse</h3>
                
                <!--<p> Enter your personal details below: </p>-->
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nombre y Apellido</label>
                    <div class="input-icon">
                        <i class="fa fa-font"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Nombre y Apellido" name="fullname" tabindex="1" /> </div>
                </div>


                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Institución</label>
                    
                    <select name="inst" id="inst_list" class="select2 form-control select2-hidden-accessible" tabindex="2" aria-hidden="true">
                        <option value=""></option>
                    <?php
                    $sqlSelectDisc = "SELECT * FROM tblInstitucion ORDER BY insAcro ASC";
                    $resultDisc = mysqli_query($con, $sqlSelectDisc);
                            while ($rowDis = mysqli_fetch_array($resultDisc)) {
                            echo '<option name="inst" id="inst" value="'.$rowDis['idIns'].'">'.$rowDis['insAcro'].'-'.$rowDis['insNom'].'</option>';
                         }
                    ?>
                    </select>
                   
               
                    <div id="inst_error"> </div>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" tabindex="3"/> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nombre de Usuario</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Nombre de Usuario" name="username" tabindex="4"/> </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Contraseña" name="password" tabindex="5"/> </div>
                </div>
                
                <!--<div class="form-group">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="tnc" /> Yo estoy de acuerdo con los
                        <a href="javascript:;">Terminos </a> y
                        <a href="javascript:;">Condiciones </a>
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>-->
                <div class="form-actions">
                    <button id="register-back-btn" type="button" class="btn red btn-outline"> Volver </button>
                    <button type="submit" name="register-submit-btn" id="register-submit-btn" class="btn green pull-right"> Registrar </button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> San Salvador de Jujuy 2020 &copy; Cosecha de datos para VISOR AMBIENTAL </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/login-4.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>