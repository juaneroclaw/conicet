<?php 
//header('Content-Type: application/json');
$uploadDir = 'subidas/'; 
$response = array( 
    'status' => 0, 
    'uri' => '$targetFilePath',
    'message' => 'Falló el envío, por favor intenta de nuevo.' 
); 
            $uploadStatus = 1; 
             
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
          
                $fileName = basename($_FILES['file']["name"]);  
                //$temp = explode(".", $_FILES["file"]["name"]);
                //$newfilename = round(microtime(true)) . '.' . end($temp);
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
                 
                // Allow certain file formats 
                $allowTypes = array('xls', 'xlsx'); 
                if(in_array($fileType, $allowTypes)){ 
                    // test file exists
                    if(file_exists($targetFilePath)){
                        $uploadedFile = $fileName; 
                        $response['status'] = 2; 
                        $response['uri'] = $targetFilePath; 
                        $response['message'] = 'El archivo <strong>'.$targetFilePath.'</strong> ya existe.';
                    }else{
                        // Upload file to the server 
                        if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                            $uploadedFile = $fileName; 
                            $response['status'] = 1; 
                            $response['uri'] = $targetFilePath; 
                            $response['message'] = 'El archivo se cargó satisfactoriamente!.'; 
                        }else{ 
                            $uploadStatus = 0; 
                            $response['message'] = 'Lo siento, ocurrió un error al cargar tu archivo.'; 
                        } 
                    }
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'Lo siento, sólo se permiten archivos XLS & XLSX para cargar.'; 
                } 
            } 
// Return response 
echo json_encode($response);